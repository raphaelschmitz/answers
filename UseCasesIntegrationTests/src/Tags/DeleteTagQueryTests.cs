using System;
using System.Threading;
using Answers.Entities.Models;
using Answers.UseCases.Common.Authorization;
using Answers.UseCases.Tags.DeleteTag;
using Microsoft.AspNetCore.Mvc;
using NHibernate;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Tags
{
	public class DeleteTagUseCaseTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void DeniesUnauthorized()
		{
			// Arrange
			IAuthorizer authorizer = TestAuthorizer.Create(null);
			DeleteTagUseCase useCase = new DeleteTagUseCase(Session, authorizer);
			DeleteTagInput input = new DeleteTagInput { TagId = Guid.NewGuid() };


			// Act
			ActionResult<DeleteTagOutput> actionResult =
				await useCase.Execute(input, CancellationToken.None);


			// Assert
			Assert.IsType<UnauthorizedResult>(actionResult.Result);
		}


		[Fact]
		public async void NonExistingReturns404()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			IAuthorizer authorizer = TestAuthorizer.Create(account);
			DeleteTagUseCase useCase = new DeleteTagUseCase(Session, authorizer);
			DeleteTagInput input = new DeleteTagInput { TagId = Guid.NewGuid() };


			// Act
			ActionResult<DeleteTagOutput> actionResult =
				await useCase.Execute(input, CancellationToken.None);


			// Assert
			Assert.IsType<NotFoundResult>(actionResult.Result);
		}


		[Fact]
		public async void EditsArePersisted()
		{
			// Arrange
			Tag tag = Tag.Create("title", "description");
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(tag);
				await transaction.CommitAsync();
			}

			Account account = await TestAccount.Create(Session);
			IAuthorizer authorizer = TestAuthorizer.Create(account);
			DeleteTagUseCase useCase = new DeleteTagUseCase(Session, authorizer);


			DeleteTagInput input = new DeleteTagInput { TagId = tag.Id };


			// Act
			ActionResult<DeleteTagOutput> actionResult =
				await useCase.Execute(input, CancellationToken.None);


			// Assert
			Assert.True(actionResult.Value.HasSucceeded);

			Tag tagFromDb = await Session.GetAsync<Tag>(tag.Id);
			Assert.Null(tagFromDb);
		}
	}
}
