using System;
using System.Threading;
using Answers.Entities.Models;
using Answers.UseCases.Common;
using Answers.UseCases.Tags.GetTag;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NHibernate;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Tags
{
	public class GetTagUseCaseTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void NonExistingReturns404()
		{
			// Arrange
			IMapper<Tag, GetTagOutput> mapper = CreateMapper(null);
			GetTagUseCase useCase = new GetTagUseCase(Session, mapper);
			GetTagInput input = new GetTagInput { Id = Guid.NewGuid() };


			// Act
			ActionResult<GetTagOutput> actionResult =
				await useCase.Execute(input, CancellationToken.None);


			// Assert
			Assert.IsType<NotFoundResult>(actionResult.Result);
		}


		[Fact]
		public async void ReturnsMapperResult()
		{
			// Arrange
			string title = "orpisgjmeorv";
			string description = "owrg;jios;reiohg";
			Tag tag = Tag.Create(title, description);
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(tag);
				await transaction.CommitAsync();
			}

			GetTagOutput getTagOutput = new GetTagOutput();
			IMapper<Tag, GetTagOutput> mapper = CreateMapper(getTagOutput);

			GetTagUseCase useCase = new GetTagUseCase(Session, mapper);
			GetTagInput input = new GetTagInput { Id = tag.Id };


			// Act
			ActionResult<GetTagOutput> actionResult =
				await useCase.Execute(input, CancellationToken.None);


			// Assert
			GetTagOutput result = actionResult.Value;
			Assert.Equal(getTagOutput, result);
		}


		private static IMapper<Tag, GetTagOutput> CreateMapper(GetTagOutput result)
		{
			Mock<IMapper<Tag, GetTagOutput>> mapperMock = new Mock<IMapper<Tag, GetTagOutput>>();
			mapperMock
				.Setup(x => x.Map(It.IsAny<Tag>()))
				.Returns(result);
			return mapperMock.Object;
		}
	}
}
