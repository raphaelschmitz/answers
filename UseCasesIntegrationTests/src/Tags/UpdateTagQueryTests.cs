using System;
using System.Threading;
using Answers.Entities.Models;
using Answers.UseCases.Common.Authorization;
using Answers.UseCases.Tags.UpdateTag;
using Microsoft.AspNetCore.Mvc;
using NHibernate;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Tags
{
	public class UpdateTagUseCaseTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void DeniesUnauthorized()
		{
			// Arrange
			IAuthorizer authorizer = TestAuthorizer.Create(null);
			UpdateTagUseCase query = new UpdateTagUseCase(Session, authorizer);
			UpdateTagInput input = new UpdateTagInput
			{
				Id = Guid.NewGuid(),
				Title = "",
				Description = ""
			};


			// Act
			ActionResult<UpdateTagOutput> actionResult =
				await query.Execute(input, CancellationToken.None);


			// Assert
			Assert.IsType<UnauthorizedResult>(actionResult.Result);
		}

		[Fact]
		public async void NonExistingReturns404()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			IAuthorizer authorizer = TestAuthorizer.Create(account);
			UpdateTagUseCase query = new UpdateTagUseCase(Session, authorizer);
			UpdateTagInput input = new UpdateTagInput
			{
				Id = Guid.NewGuid(),
				Title = "",
				Description = ""
			};


			// Act
			ActionResult<UpdateTagOutput> actionResult =
				await query.Execute(input, CancellationToken.None);


			// Assert
			Assert.IsType<NotFoundResult>(actionResult.Result);
		}


		[Fact]
		public async void EditsArePersisted()
		{
			// Arrange
			Tag tag = Tag.Create("title", "description");
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(tag);
				await transaction.CommitAsync();
			}

			Account account = await TestAccount.Create(Session);
			IAuthorizer authorizer = TestAuthorizer.Create(account);
			UpdateTagUseCase query = new UpdateTagUseCase(Session, authorizer);

			Guid id = tag.Id;
			string newTitle = "poergvj;on";
			string newDescription = "aoeiwhcgoeh409";
			UpdateTagInput input = new UpdateTagInput
			{
				Id = id,
				Title = newTitle,
				Description = newDescription
			};


			// Act
			ActionResult<UpdateTagOutput> actionResult =
				await query.Execute(input, CancellationToken.None);


			// Assert
			Assert.True(actionResult.Value.HasSucceeded);

			Tag tagFromDb = await Session.GetAsync<Tag>(id);
			Assert.Equal(newTitle, tagFromDb.Title);
			Assert.Equal(newDescription, tagFromDb.Description);
		}
	}
}
