using System;
using System.Linq;
using Answers.Entities.Models;
using Answers.UseCases.Tags.GetTag;
using Xunit;


namespace UseCasesIntegrationTests.Tags
{
	public class GetTagOutputMapperTests
	{
		[Fact]
		public async void CanGetTag()
		{
			// Arrange
			string title = "orpisgjmeorv";
			string description = "owrg;jios;reiohg";
			Tag tag = Tag.Create(title, description);
			GetTagOutputMapper mapper = new GetTagOutputMapper();


			// Act
			GetTagOutput result = mapper.Map(tag);


			// Assert
			Assert.Equal(title, result.Title);
			Assert.Equal(description, result.Description);
		}

		[Fact]
		public void OutputContainsQuestions()
		{
			// Arrange
			Account account = Account.Create("a@b.com", "", "");
			Question question = Question.Create(account, DateTime.Now, "", "");
			question.Id = Guid.NewGuid();

			string title = "orpisgjmeorv";
			string description = "owrg;jios;reiohg";
			Tag tag = Tag.Create(title, description);
			tag.Questions.Add(question);


			// Act
			GetTagOutputMapper mapper = new GetTagOutputMapper();


			// Assert
			GetTagOutput result = mapper.Map(tag);
			GetTagOutput.QuestionDto questionInResult = result.Questions.FirstOrDefault();
			Assert.Equal(question.Id, questionInResult.Id);
		}



	}
}
