using System;
using System.Threading;
using Answers.Entities.Models;
using Answers.UseCases.Accounts.SignUp;
using Answers.UseCases.Common;
using Answers.UseCases.Common.Clocks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NHibernate;
using NHibernate.Linq;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Accounts
{
	public class SignUpTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void DeniesAlreadyExistingEmail()
		{
			// Arrange
			string hashedPassword = "iiuomirbvwe";
			IEmailAddressValidator emailAddressValidator = CreateEmailAddressValidator(true);
			IPasswordHasher passwordHasher = CreatePasswordHasher(hashedPassword);
			IClock clock = TestClock.Create();
			Mock<IRegistrationMailSender> mailSenderMock = new Mock<IRegistrationMailSender>();
			IRegistrationMailSender mailSender = mailSenderMock.Object;
			SignUpUseCase query = new SignUpUseCase(Session, emailAddressValidator, passwordHasher, clock, mailSender);

			string emailAddress = "emailAddress";
			string password = "password";
			string userName = "userName";
			Account account = Account.Create(emailAddress, password, userName);
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(account);
				await transaction.CommitAsync();
			}
			SignUpInput input = new SignUpInput { EmailAddress = emailAddress, Password = password, UserName = userName };


			// Act
			ActionResult<SignUpOutput> actionResult = await query.Execute(input, CancellationToken.None);


			// Assert
			Assert.IsType<ConflictResult>(actionResult.Result);
		}


		[Fact]
		public async void DataIsPersisted()
		{
			// Arrange
			WebsiteConfiguration websiteConfiguration = WebsiteConfiguration.Create("email@smtp.com", 1234, false,
				"noreply answers", "password", "mailer@answers.com", "Hello. Yu are regsiter.");
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(websiteConfiguration);
				await transaction.CommitAsync();
			}

			IEmailAddressValidator emailAddressValidator = CreateEmailAddressValidator(true);

			string hashedPassword = "iiuomirbvwe";
			IPasswordHasher passwordHasher = CreatePasswordHasher(hashedPassword);

			DateTime creationDate = DateTime.Now;
			IClock clock = TestClock.Create(creationDate);

			Mock<IRegistrationMailSender> mailSenderMock = new Mock<IRegistrationMailSender>();
			IRegistrationMailSender mailSender = mailSenderMock.Object;

			SignUpUseCase query = new SignUpUseCase(Session, emailAddressValidator, passwordHasher, clock, mailSender);

			string emailAddress = "emailAddress";
			string password = "password";
			string userName = "userName";
			SignUpInput input = new SignUpInput { EmailAddress = emailAddress, Password = password, UserName = userName };


			// Act
			ActionResult<SignUpOutput> output = await query.Execute(input, CancellationToken.None);
			SignUpOutput result = output.Value;


			// Assert
			Account account = await Session.GetAsync<Account>(result.Id);
			Assert.NotNull(account);
			Assert.Equal(emailAddress, account.EmailAddress);
			Assert.Equal(hashedPassword, account.PasswordHash);
			Assert.Equal(userName, account.UserName);

			EmailConfirmProcess emailConfirmProcess = await Session.Query<EmailConfirmProcess>()
				.FirstOrDefaultAsync(x => x.Account == account);
			Assert.NotNull(emailConfirmProcess);
			Assert.Equal(creationDate, emailConfirmProcess.CreationDate);

			mailSenderMock.Verify(x => x.Send(It.IsAny<WebsiteConfiguration>(), It.IsAny<EmailConfirmProcess>()), Times.Once);
		}


		// =====================================================================================
		// Helpers
		// =====================================================================================


		private static IEmailAddressValidator CreateEmailAddressValidator(bool emailValidationResult)
		{
			Mock<IEmailAddressValidator> emailValidatorMock = new Mock<IEmailAddressValidator>();
			emailValidatorMock.Setup(x => x.Validate(It.IsAny<string>()))
				.Returns(emailValidationResult);
			IEmailAddressValidator emailAddressValidator = emailValidatorMock.Object;
			return emailAddressValidator;
		}


		private static IPasswordHasher CreatePasswordHasher(string passwordHashResult)
		{
			Mock<IPasswordHasher> passwordHasherMock = new Mock<IPasswordHasher>();
			passwordHasherMock.Setup(x => x.CreateSaltedHash(It.IsAny<string>()))
				.Returns(passwordHashResult);
			IPasswordHasher passwordHasher = passwordHasherMock.Object;
			return passwordHasher;
		}
	}
}
