using System;
using System.Linq;
using Answers.Entities.Models;
using Answers.UseCases.Accounts.GetAccount;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Accounts
{
	public class GetAccountOutputMapperTests
	{
		[Fact]
		public void CountsVotesCorrectly()
		{
			// Arrange
			Account account = Account.Create("", "", "");

			Question question = Question.Create(account, DateTime.Now, "", "");
			Account voter1 = Account.Create("", "", "");
			Vote.Create(question.Post, voter1, Vote.Up);
			Account voter2 = Account.Create("", "", "");
			Vote.Create(question.Post, voter2, Vote.Up);
			Account voter3 = Account.Create("", "", "");
			Vote.Create(question.Post, voter3, Vote.Up);
			Account voter4 = Account.Create("", "", "");
			Vote.Create(question.Post, voter4, Vote.Down);

			GetAccountOutputMapper mapper = new GetAccountOutputMapper();


			// Act
			GetAccountOutput result = mapper.Map(account);


			// Assert
			Assert.Equal(2, result.Points);
		}


		[Fact]
		public void GetsQuestionsCorrectly()
		{
			// Arrange
			Account account = Account.Create("", "", "");

			string title = "kosjf8ocisfjvos";
			Question question = Question.Create(account, DateTime.Now, title, "");
			Question.Create(account, DateTime.Now, "", "");
			Question.Create(account, DateTime.Now, "", "");

			question.AddAnswer(account, DateTime.Now, "");
			question.AddAnswer(account, DateTime.Now, "");

			Vote.Create(question.Post, account, Vote.Up);

			GetAccountOutputMapper mapper = new GetAccountOutputMapper();


			// Act
			GetAccountOutput result = mapper.Map(account);


			// Assert
			Assert.Equal(3, result.Questions.Count());
			var resultQuestion = result.Questions.FirstOrDefault(x => x.Title == title);
			Assert.NotNull(resultQuestion);
			Assert.Equal(question.Post.Votes.Sum(x=>x.Value), resultQuestion.Points);
		}


		[Fact]
		public void GetsAnswersCorrectly()
		{
			// Arrange
			Account account = Account.Create("", "", "");

			Question question = Question.Create(account, DateTime.Now, "", "");
			Question.Create(account, DateTime.Now, "", "");
			Question.Create(account, DateTime.Now, "", "");
			question.AddAnswer(account, DateTime.Now, "");
			question.AddAnswer(account, DateTime.Now, "");
			question.AddAnswer(account, DateTime.Now, "");
			question.AddAnswer(account, DateTime.Now, "");


			GetAccountOutputMapper mapper = new GetAccountOutputMapper();


			// Act
			GetAccountOutput result = mapper.Map(account);


			// Assert
			Assert.Equal(4, result.Answers.Count());
		}
	}
}
