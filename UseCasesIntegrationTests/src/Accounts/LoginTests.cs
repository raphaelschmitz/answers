using System.Threading;
using Answers.Entities.Models;
using Answers.UseCases.Accounts.Login;
using Answers.UseCases.Common;
using Answers.UseCases.Common.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Accounts
{
	public class LoginTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void DeniesNonExistingUser()
		{
			// Arrange
			Mock<IPasswordHasher> passwordHasherMock = new Mock<IPasswordHasher>();
			IPasswordHasher passwordHasher = passwordHasherMock.Object;

			Account account = await TestAccount.Create(Session);
			IAuthorizer authorizer = TestAuthorizer.Create(account);

			LoginUseCase query = new LoginUseCase(Session, passwordHasher, authorizer);

			LoginInput input = new LoginInput { EmailAddress = "doesnotexist@email.com" };


			// Act
			ActionResult<LoginOutput> actionResult =
				await query.Execute(input, CancellationToken.None);


			// Assert
			Assert.IsType<BadRequestResult>(actionResult.Result);
		}


		[Fact]
		public async void DeniesWrongPassword()
		{
			// Arrange
			Mock<IPasswordHasher> passwordHasherMock = new Mock<IPasswordHasher>();
			IPasswordHasher passwordHasher = passwordHasherMock.Object;

			Account account = await TestAccount.Create(Session);
			IAuthorizer authorizer = TestAuthorizer.Create(null);

			LoginUseCase query = new LoginUseCase(Session, passwordHasher, authorizer);

			LoginInput input = new LoginInput
			{
				EmailAddress = account.EmailAddress,
                Password = ""
			};


			// Act
			ActionResult<LoginOutput> actionResult =
				await query.Execute(input, CancellationToken.None);


			// Assert
			Assert.IsType<BadRequestResult>(actionResult.Result);
		}


		[Fact]
		public async void ReturnsTokenForCorrectCredentials()
		{
			// Arrange
			Mock<IPasswordHasher> passwordHasherMock = new Mock<IPasswordHasher>();
			passwordHasherMock
				.Setup(x => x.CheckIfPasswordMatches(It.IsAny<string>(), It.IsAny<string>()))
				.Returns(true);
			IPasswordHasher passwordHasher = passwordHasherMock.Object;

			Account account = await TestAccount.Create(Session);

			Mock<IAuthorizer> mock = new Mock<IAuthorizer>();
			AuthorizationResult authorizationResult = new AuthorizationResult(account);
			mock
				.Setup(x => x.Check(It.IsAny<string>(), It.IsAny<CancellationToken>()))
				.ReturnsAsync(authorizationResult);
			string token = "iefjcmsrflj";
			mock
				.Setup(x => x.CreateToken(It.IsAny<Account>()))
				.Returns(token);
			IAuthorizer authorizer = mock.Object;

			LoginUseCase query = new LoginUseCase(Session, passwordHasher, authorizer);

			LoginInput input = new LoginInput { EmailAddress = account.EmailAddress };


			// Act
			ActionResult<LoginOutput> actionResult =
				await query.Execute(input, CancellationToken.None);


			// Assert
			Assert.Null(actionResult.Result);

			LoginOutput loginOutput = actionResult.Value;
			Assert.NotNull(loginOutput);
			Assert.Equal(token, loginOutput.Token);
		}
	}
}
