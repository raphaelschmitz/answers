using System;
using System.Linq;
using System.Threading;
using Answers.Entities.Models;
using Answers.UseCases.Accounts.GetAccount;
using Answers.UseCases.Common;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NHibernate;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Accounts
{
	public class GetAccountTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void NonExistingReturns404()
		{
			// Arrange
			IMapper<Account, GetAccountOutput> mapper = new GetAccountOutputMapper();
			GetAccountUseCase query = new GetAccountUseCase(Session, mapper);

			GetAccountInput input = new GetAccountInput { Id = Guid.NewGuid() };


			// Act
			ActionResult<GetAccountOutput> actionResult =
				await query.Execute(input, CancellationToken.None);


			// Assert
			Assert.IsType<NotFoundResult>(actionResult.Result);
		}


		[Fact]
		public async void ConvertsAccountWithMapper()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);

			var mapperMock = new Mock<IMapper<Account, GetAccountOutput>>();
			IMapper<Account, GetAccountOutput> mapper = mapperMock.Object;

			GetAccountUseCase query = new GetAccountUseCase(Session, mapper);
			GetAccountInput input = new GetAccountInput { Id = account.Id };


			// Act
			await query.Execute(input, CancellationToken.None);


			// Assert
			mapperMock.Verify(x => x.Map(account), Times.Once);
		}


		[Fact]
		public async void CanGetAccount()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			string title = "iawjofi rj";
			string content = "p[efj onroursinocs";
			Question question = await TestQuestion.Create(Session, account, title, content);
			await TestQuestion.Create(Session, account);

			question.AddAnswer(account, DateTime.Now, "");
			question.AddAnswer(account, DateTime.Now, "");
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(question);
				await transaction.CommitAsync();
			}

			GetAccountOutputMapper getAccountOutputMapper = new GetAccountOutputMapper();
			GetAccountUseCase query = new GetAccountUseCase(Session, getAccountOutputMapper);

			GetAccountInput input = new GetAccountInput { Id = account.Id };


			// Act
			ActionResult<GetAccountOutput> actionResult =
				await query.Execute(input, CancellationToken.None);


			// Assert
			GetAccountOutput result = actionResult.Value;
			Assert.Equal(account.UserName, result.UserName);

			Assert.Equal(2, result.Questions.Count());
			var resultQuestion = result.Questions.FirstOrDefault(x => x.Title == title);
			Assert.NotNull(resultQuestion);
			Assert.Equal(question.Id, resultQuestion.Id);
			Assert.Equal(question.Post.Votes.Sum(x => x.Value), resultQuestion.Points);

			Assert.Equal(2, result.Answers.Count());
			var resultAnswer = result.Answers.FirstOrDefault(x => x.Title == title);
			Assert.NotNull(resultAnswer);
			Assert.Equal(question.Id, resultAnswer.Id);
		}


		[Fact]
		public async void DoesCountPoints()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);
			Account voter = await TestAccount.Create(Session, "voter@vote.com");
			TestVote.Create(Session, question.Post, voter, Vote.Up);
			Account voter2 = await TestAccount.Create(Session, "voter2@vote.com");
			TestVote.Create(Session, question.Post, voter2, Vote.Up);

			GetAccountOutputMapper mapper = new GetAccountOutputMapper();
			GetAccountUseCase query = new GetAccountUseCase(Session, mapper);

			GetAccountInput input = new GetAccountInput { Id = account.Id };


			// Act
			ActionResult<GetAccountOutput> actionResult =
				await query.Execute(input, CancellationToken.None);


			// Assert
			GetAccountOutput result = actionResult.Value;
			Assert.Equal(2, result.Points);
		}
	}
}
