using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.UseCases.Admin.UpdateSettings;
using Answers.UseCases.Common.Authorization;
using Microsoft.AspNetCore.Mvc;
using NHibernate;
using NHibernate.Linq;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Admin
{
	public class UpdateSettingsUseCaseTests : InMemoryDatabaseTest
	{
		[Fact]
		public async Task DeniesUnauthorized()
		{
			// Arrange
			IAuthorizer authorizer = TestAuthorizer.Create(null);
			UpdateSettingsUseCase query = new UpdateSettingsUseCase(Session, authorizer);
			UpdateSettingsInput input = new UpdateSettingsInput();


			// Act
			ActionResult<UpdateSettingsOutput> actionResult = await query.Execute(input, CancellationToken.None);


			// Assert
			Assert.IsType<UnauthorizedResult>(actionResult.Result);
		}


		[Fact]
		public async Task SetsValuesInDatabase()
		{
			// Arrange
			WebsiteConfiguration websiteConfiguration = WebsiteConfiguration.Create("email@smtp.com", 1234, false,
				"noreply answers", "password", "mailer@answers.com", "Hello. Yu are regsiter.");
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(websiteConfiguration);
				await transaction.CommitAsync();
			}

			Account account = await TestAccount.Create(Session);
			IAuthorizer authorizer = TestAuthorizer.Create(account);
			UpdateSettingsUseCase query = new UpdateSettingsUseCase(Session, authorizer);

			string emailFromAddress = "from@email.com";
			string registrationEmailTemplate = "Hello {{ 0 }} whatup";
			UpdateSettingsInput input = new UpdateSettingsInput
			{
				RegistrationFromAddress = emailFromAddress,
				RegistrationEmailTemplate = registrationEmailTemplate
			};


			// Act
			await query.Execute(input, CancellationToken.None);


			// Assert
			WebsiteConfiguration fromDb = await Session.Query<WebsiteConfiguration>().FirstAsync();
			Assert.Equal(emailFromAddress, fromDb.EmailFromAddress);
			Assert.Equal(registrationEmailTemplate, fromDb.RegistrationEmailTemplate);
		}
	}
}
