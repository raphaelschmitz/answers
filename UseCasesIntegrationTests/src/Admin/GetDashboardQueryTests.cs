using System.Threading;
using Answers.Entities.Models;
using Answers.UseCases.Admin.GetDashboard;
using Answers.UseCases.Common.Authorization;
using Microsoft.AspNetCore.Mvc;
using NHibernate;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Admin
{
	public class GetDashboardUseCaseTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void DeniesUnauthorized()
		{
			// Arrange
			IAuthorizer authorizer = TestAuthorizer.Create(null);
			GetDashboardUseCase getDashboardUseCase = new GetDashboardUseCase(Session, authorizer);
			GetDashboardInput input = new GetDashboardInput();


			// Act
			ActionResult<GetDashboardOutput> actionResult =
				await getDashboardUseCase.Execute(input, CancellationToken.None);


			// Assert
			Assert.IsType<UnauthorizedResult>(actionResult.Result);
		}


		[Fact]
		public async void GetsValues()
		{
			// Arrange
			string emailSmtpAddress = "from@email.com";
			int emailPort = 1234;
			bool emailUseSsl = true;
			string emailUserName = "from";
			string emailPassword = "ialecnalcn";
			string emailFromAddress = "from@email.com";
			string registrationEmailTemplate = "ioha uwcrlhguseguirhmuvic rh {{ name }} ivorsrcj";

			WebsiteConfiguration websiteConfiguration = WebsiteConfiguration.Create(emailSmtpAddress, emailPort,
				emailUseSsl, emailUserName, emailPassword, emailFromAddress, registrationEmailTemplate);
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(websiteConfiguration);
				await transaction.CommitAsync();
			}

			Account account = await TestAccount.Create(Session);
			IAuthorizer authorizer = TestAuthorizer.Create(account);
			GetDashboardUseCase getDashboardUseCase = new GetDashboardUseCase(Session, authorizer);
			GetDashboardInput input = new GetDashboardInput();


			// Act
			ActionResult<GetDashboardOutput> actionResult =
				await getDashboardUseCase.Execute(input, CancellationToken.None);


			// Assert
			GetDashboardOutput getDashboardOutput = actionResult.Value;
			Assert.NotNull(getDashboardOutput);
			Assert.Equal(emailFromAddress, getDashboardOutput.RegistrationFromAddress);
			Assert.Equal(registrationEmailTemplate, getDashboardOutput.RegistrationEmailTemplate);

			GetDashboardOutput.EmailSettingsDto emailSettingsDto = getDashboardOutput.EmailSettings;
			Assert.NotNull(emailSettingsDto);
			Assert.Equal(emailSmtpAddress, emailSettingsDto.SmtpAddress);
			Assert.Equal(emailPort, emailSettingsDto.Port);
			Assert.Equal(emailUseSsl, emailSettingsDto.UseSsl);
			Assert.Equal(emailUserName, emailSettingsDto.UserName);
			Assert.Equal(emailPassword, emailSettingsDto.Password);
		}
	}
}
