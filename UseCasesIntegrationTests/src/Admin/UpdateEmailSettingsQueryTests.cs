using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.UseCases.Admin.UpdateEmailSettings;
using Answers.UseCases.Common.Authorization;
using Microsoft.AspNetCore.Mvc;
using NHibernate;
using NHibernate.Linq;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Admin
{
	public class UpdateEmailSettingsUseCaseTests : InMemoryDatabaseTest
	{
		[Fact]
		public async Task DeniesUnauthorized()
		{
			// Arrange
			IAuthorizer authorizer = TestAuthorizer.Create(null);
			UpdateEmailSettingsUseCase query = new UpdateEmailSettingsUseCase(Session, authorizer);
			UpdateEmailSettingsInput input = new UpdateEmailSettingsInput();


			// Act
			ActionResult<UpdateEmailSettingsOutput> actionResult = await query.Execute(input, CancellationToken.None);


			// Assert
			Assert.IsType<UnauthorizedResult>(actionResult.Result);
		}


		[Fact]
		public async Task SetsValuesInDatabase()
		{
			// Arrange
			WebsiteConfiguration websiteConfiguration = WebsiteConfiguration.Create("email@smtp.com", 1234, false,
				"noreply answers", "password", "mailer@answers.com", "Hello. Yu are regsiter.");
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(websiteConfiguration);
				await transaction.CommitAsync();
			}

			Account account = await TestAccount.Create(Session);
			IAuthorizer authorizer = TestAuthorizer.Create(account);
			UpdateEmailSettingsUseCase query = new UpdateEmailSettingsUseCase(Session, authorizer);

			string emailSmtpAddress = "linacsreunc";
			int emailPort = 4321;
			bool emailUseSsl = true;
			string emailUserName = "jnlriusnlrjkns";
			string emailPassword = "inrciusncr";
			UpdateEmailSettingsInput input = new UpdateEmailSettingsInput
			{
				SmtpAddress = emailSmtpAddress,
				Port = emailPort,
				UseSsl = emailUseSsl,
				UserName = emailUserName,
				Password = emailPassword
			};


			// Act
			await query.Execute(input, CancellationToken.None);


			// Assert
			WebsiteConfiguration fromDb = await Session.Query<WebsiteConfiguration>().FirstAsync();
			Assert.Equal(emailSmtpAddress, fromDb.EmailSmtpAddress);
			Assert.Equal(emailPort, fromDb.EmailPort);
			Assert.Equal(emailUseSsl, fromDb.EmailUseSsl);
			Assert.Equal(emailUserName, fromDb.EmailUserName);
			Assert.Equal(emailPassword, fromDb.EmailPassword);
		}
	}
}
