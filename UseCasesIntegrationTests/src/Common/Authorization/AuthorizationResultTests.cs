using System.Linq;
using Answers.Entities.Models;
using Answers.Entities.Values;
using Answers.UseCases.Common.Authorization;
using Xunit;


namespace UseCasesIntegrationTests.Common.Authorization
{
	public class AuthorizationResultTests
	{
		[Fact]
		public void AuthorizesAddedAllowable()
		{
			// Arrange
			Account account = Account.Create("", "", "");
			AuthorizationResult authorizationResult = new AuthorizationResult(account);
			Allowable allowable = account.GetPermissions().First();


			// Act
			bool isAuthorized = authorizationResult.IsAuthorizedTo(allowable);


			// Assert
			Assert.True(isAuthorized);
		}


		[Fact]
		public void DoesNotAuthorizeMissingAllowable()
		{
			// Arrange
			Account account = Account.Create("", "", "");
			AuthorizationResult authorizationResult = new AuthorizationResult(account);
			Allowable allowable = Allowable.DeleteQuestions;


			// Act
			bool isAuthorized = authorizationResult.IsAuthorizedTo(allowable);


			// Assert
			Assert.False(isAuthorized);
		}
	}

}
