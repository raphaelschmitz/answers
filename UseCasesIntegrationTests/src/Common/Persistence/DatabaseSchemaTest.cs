using Answers.UseCases.Common.Persistence;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Common.Persistence
{
	public class DatabaseSchemaTest
	{
		[Fact]
		public void SchemaIsValid()
		{
			Configuration configuration = new Configuration();

			configuration.ConfigureInMemorySqLiteConnection();

			new SchemaValidator(configuration).Validate();
		}


		[Fact]
		public void CanCreateInMemoryDb()
		{
			Configuration configuration = new Configuration();

			configuration.ConfigureInMemorySqLiteConnection();

			SchemaExport schemaExport = new SchemaExport(configuration);
			schemaExport.Execute(true, true, false);
		}


		[Fact]
		public void CanGenerateDatabaseSchema()
		{
			Configuration configuration = new Configuration();

			configuration.ConfigureInMemorySqLiteConnection();
			configuration.AddAnswersMappings();

			SchemaExport schemaExport = new SchemaExport(configuration);
			schemaExport.Execute(true, true, false);
		}
	}
}
