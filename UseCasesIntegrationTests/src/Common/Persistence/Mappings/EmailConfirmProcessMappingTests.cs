using System;
using Answers.Entities.Models;
using NHibernate;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Common.Persistence.Mappings
{
	public class EmailConfirmProcessMappingTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void DataIsPersisted()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			string confirmKey = "efesserbysegbtrh";
			EmailConfirmProcess emailConfirmProcess = EmailConfirmProcess.Create(account, confirmKey, DateTime.Now);


			// Act
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(emailConfirmProcess);
				await transaction.CommitAsync();
			}


			// Assert
			EmailConfirmProcess result = await Session.GetAsync<EmailConfirmProcess>(emailConfirmProcess.Id);
			Assert.NotNull(result);

			Assert.Equal(account, result.Account);
			Assert.Equal(confirmKey, result.ConfirmKey);
		}
	}
}
