using Answers.Entities.Models;
using NHibernate;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Common.Persistence.Mappings
{
	public class VoteMappingTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void DataIsPersisted()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);
			Post post = question.Post;
			Account voter = await TestAccount.Create(Session, "voter@vote.com");
			int value = Vote.Down;
			Vote vote = Vote.Create(post, voter, value);

			
			// Act
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(vote);
				await transaction.CommitAsync();
			}


			// Assert
			Vote result = await Session.GetAsync<Vote>(vote.Id);
			Assert.NotNull(result);

			Assert.Equal(post, result.Post);
			Assert.Equal(voter, result.Voter);
			Assert.Equal(account, result.Beneficiary);
			Assert.Equal(value, result.Value);
		}
	}
}
