using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Answers.Entities.Models;
using Answers.Entities.Values;
using NHibernate;
using NHibernate.Linq;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Common.Persistence.Mappings
{
	public class QuestionMappingTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void CanBeSaved()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			DateTime creationDate = DateTime.Now;
			string title = "egdtdmyssd";
			string content = "uoytbdrdrfse";
			Question question = Question.Create(account, creationDate, title, content);

			// Act
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(question);
				await transaction.CommitAsync();
			}

			// Assert
			Question result = await Session.Query<Question>()
				.FirstOrDefaultAsync(x => x.CurrentTitle == title);
			Assert.NotNull(result);
			Assert.NotNull(result.Creator);

			Post post = result.Post;
			Assert.NotNull(post);

			PostRevision postRevision = post.CurrentRevision;
			Assert.NotNull(postRevision);
			Assert.Equal(creationDate, postRevision.CreationDate);
			Assert.Equal(title, postRevision.Title);
			Assert.Equal(content, postRevision.Content);
		}


		[Fact]
		public async void AddedAnswersAreSaved()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = Question.Create(account, DateTime.Now, "", "");
			Post post = question.AddAnswer(account, DateTime.Now, "");


			// Act
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(question);
				await transaction.CommitAsync();
			}

			// Assert
			List<Post> posts = await Session.Query<Post>().ToListAsync();
			Assert.Equal(2, posts.Count);

			Assert.Single(posts.Where(x => x.PostType == PostType.Question));
			Assert.Single(posts.Where(x => x.PostType == PostType.Answer));
		}


		[Fact]
		public async void PostsAreSaved()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = Question.Create(account, DateTime.Now, "", "");
			Post post = question.AddAnswer(account, DateTime.Now, "");


			// Act
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(question);
				await transaction.CommitAsync();
			}

			// Assert
			List<Post> posts = await Session.Query<Post>().ToListAsync();
			Assert.Equal(2, posts.Count);

			Assert.Single(posts.Where(x => x.PostType == PostType.Question));
			Assert.Single(posts.Where(x => x.PostType == PostType.Answer));
		}


		[Fact]
		public async void RemovedAnswersAreDeleted()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = Question.Create(account, DateTime.Now, "", "q");
			Post answer = question.AddAnswer(account, DateTime.Now, "a");

			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(question);
				await transaction.CommitAsync();
			}


			// Act
			using (ITransaction transaction = Session.BeginTransaction())
			{
				question.Posts.Remove(answer);
				await Session.SaveAsync(question);
				await transaction.CommitAsync();
			}


			// Assert
			Question questionFromDatabase = await Session.GetAsync<Question>(question.Id);
			Assert.Null(questionFromDatabase.Posts.FirstOrDefault(x => x.PostType == PostType.Answer));
		}


		[Fact]
		public async void AnswersAreDeletedWithTheirQuestion()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = Question.Create(account, DateTime.Now, "", "");
			Post answer = question.AddAnswer(account, DateTime.Now, "");

			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(question, CancellationToken.None);
				await transaction.CommitAsync(CancellationToken.None);
			}


			// Act
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.DeleteAsync(question);
				await transaction.CommitAsync();
			}


			// Assert
			Post postFromDatabase = Session.Get<Post>(answer.Id);
			Assert.Null(postFromDatabase);
		}


		[Fact]
		public async void PostsAreDeletedWithTheirQuestion()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			DateTime dateTime = DateTime.Now;
			string title = "sgsthb";
			string content = "louahhlefnucgir";
			Question question = Question.Create(account, dateTime, title, content);

			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(question, CancellationToken.None);
				await transaction.CommitAsync(CancellationToken.None);
			}


			// Act
			await Session.DeleteAsync(question);
			await Session.FlushAsync();


			// Assert
			Assert.Empty(Session.Query<Post>());
		}
	}
}
