using Answers.Entities.Models;
using NHibernate;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Common.Persistence.Mappings
{
	public class TagMappingTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void DataIsPersisted()
		{
			// Arrange
			string title = "rbrtnmarvenfutytb";
			string description = "rbrtnmnfutytb";
			Tag tag = Tag.Create(title, description);


			// Act
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(tag);
				await transaction.CommitAsync();
			}


			// Assert
			Tag result = await Session.GetAsync<Tag>(tag.Id);
			Assert.NotNull(result);

			Assert.Equal(title, result.Title);
			Assert.Equal(description, result.Description);
		}
	}
}
