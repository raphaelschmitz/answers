using Answers.Entities.Models;
using NHibernate;
using NHibernate.Linq;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Common.Persistence.Mappings
{
	public class AccountMappingTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void DataIsPersisted()
		{
			// Arrange
			string emailAddress = "john@email.com";
			string passwordHash = "acxr";
			string userName = "Johnster123";
			Account account = Account.Create(emailAddress, passwordHash, userName);


			// Act
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(account);
				await transaction.CommitAsync();
			}


			// Assert
			account = await Session.Query<Account>()
				.FirstOrDefaultAsync(x => x.EmailAddress == emailAddress);
			Assert.NotNull(account);
			account = await Session.Query<Account>()
				.FirstOrDefaultAsync(x => x.PasswordHash == passwordHash);
			Assert.NotNull(account);
			account = await Session.Query<Account>()
				.FirstOrDefaultAsync(x => x.UserName == userName);
			Assert.NotNull(account);
		}
	}
}
