using System;
using Answers.Entities.Models;
using NHibernate;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Common.Persistence.Mappings
{
	public class PostRevisionMappingTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void DataIsPersisted()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);
			Post post = question.Post;
			DateTime creationDate = DateTime.Now;
			string title = "p049mutsgeci";
			string content = "rbrtnmnfutytb";
			PostRevision postRevision = PostRevision.Create(post, account, creationDate, title, content);

			// Act
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(postRevision);
				await transaction.CommitAsync();
			}


			// Assert
			PostRevision result = await Session.GetAsync<PostRevision>(postRevision.Id);
			Assert.NotNull(result);

			Assert.Equal(post, result.Post);
			Assert.Equal(title, result.Title);
			Assert.Equal(content, result.Content);
			Assert.Equal(account, result.Account);
			Assert.Equal(creationDate, result.CreationDate);
		}
	}
}
