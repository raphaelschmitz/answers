using System;
using System.Linq;
using System.Threading;
using Answers.Entities.Models;
using NHibernate;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Common.Persistence.Mappings
{
	public class PostMappingTests : InMemoryDatabaseTest
	{
	[Fact]
		public async void RevisionCascadeWorks()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			DateTime dateTime = DateTime.Now;
			string title = "sgsthb";
			string content = "louahhlefnucgir";
			Question question = Question.Create(account, dateTime, title, content);
			Post post = question.AddAnswer(account, dateTime, content);

			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(question, CancellationToken.None);
				await transaction.CommitAsync(CancellationToken.None);
			}


			// Act
			question.Posts.Remove(post);
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(question, CancellationToken.None);
				await transaction.CommitAsync(CancellationToken.None);
			}


			// Assert
			Assert.Equal(1, Session.Query<PostRevision>().Count());
		}


		[Fact]
		public async void CommentCascadeWorks()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			DateTime dateTime = DateTime.Now;
			string title = "sgsthb";
			string content = "louahhlefnucgir";
			Question question = Question.Create(account, dateTime, title, content);
			Post post = question.AddAnswer(account, dateTime, content);
			Comment.Create(post, account, dateTime, content);


			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(question, CancellationToken.None);
				await transaction.CommitAsync(CancellationToken.None);
			}


			// Act
			question.Posts.Remove(post);
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(question, CancellationToken.None);
				await transaction.CommitAsync(CancellationToken.None);
			}


			// Assert
			Assert.False(Session.Query<Comment>().Any());
		}
	}
}
