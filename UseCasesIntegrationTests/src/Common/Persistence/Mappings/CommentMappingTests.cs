using System;
using Answers.Entities.Models;
using NHibernate;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Common.Persistence.Mappings
{
	public class CommentMappingTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void DataIsPersisted()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);
			Post post = question.Post;
			DateTime creationDate = DateTime.Now;
			string content = "rbrtnmnfutytb";
			Comment comment = Comment.Create(post, account, creationDate, content);


			// Act
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(comment);
				await transaction.CommitAsync();
			}


			// Assert
			Comment result = await Session.GetAsync<Comment>(comment.Id);
			Assert.NotNull(result);

			Assert.Equal(account, result.Account);
			Assert.Equal(post, result.Post);
			Assert.Equal(creationDate, result.CreationDate);
			Assert.Equal(content, result.Content);
		}
	}
}
