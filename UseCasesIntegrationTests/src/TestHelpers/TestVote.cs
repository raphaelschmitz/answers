using Answers.Entities.Models;
using NHibernate;


namespace UseCasesIntegrationTests.TestHelpers
{
	public static class TestVote
	{
		public static Vote Create(ISession session, Post post, Account voter, int value)
		{
			Vote vote = Vote.Create(post, voter, value);
			using (ITransaction transaction = session.BeginTransaction())
			{
				session.Save(vote);
				transaction.Commit();
			}
			return vote;
		}
	}
}
