using System.Threading.Tasks;
using Answers.Entities.Models;
using NHibernate;


namespace UseCasesIntegrationTests.TestHelpers
{
	public class TestAccount
	{
		public static async Task<Account> Create(ISession session, string emailAddress = "john@email.com")
		{
			Account account = Account.Create(emailAddress, "acxr", "Johnster123");
			using (ITransaction transaction = session.BeginTransaction())
			{
				await session.SaveAsync(account);
				await transaction.CommitAsync();
			}
			return account;
		}
	}
}
