using System;
using System.Threading.Tasks;
using Answers.Entities.Models;
using NHibernate;


namespace UseCasesIntegrationTests.TestHelpers
{
	public static class TestQuestion
	{
		public static async Task<Question> Create(
			ISession session,
			Account account,
			string title = "Test Title",
			string content = "*Possunt oraque* contigerat cum Sminthea. Non undae de spectat vacuum longisque" +
							 "modum tendere [currat puellae et](http://www.suas.com/)."
			)
		{
			DateTime creationDate = DateTime.Now;
			Question question = Question.Create(account, creationDate, title, content);
			await session.SaveAsync(question);

			return question;
		}
	}
}
