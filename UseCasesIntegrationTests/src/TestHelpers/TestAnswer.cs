using System;
using System.Threading.Tasks;
using Answers.Entities.Models;
using NHibernate;


namespace UseCasesIntegrationTests.TestHelpers
{
	public static class TestAnswer
	{
		public static async Task<Post> Create(ISession session, Question question, Account account)
		{
			DateTime creationDate = DateTime.Now;
			Post post = question.AddAnswer(account, creationDate, "Content");
			await session.SaveAsync(post);

			return post;
		}
	}
}
