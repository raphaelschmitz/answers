using System.Threading;
using Answers.Entities.Models;
using Answers.UseCases.Common.Authorization;
using Moq;


namespace UseCasesIntegrationTests.TestHelpers
{
	public static class TestAuthorizer
	{
		public static IAuthorizer Create(Account account)
		{
			Mock<IAuthorizer> mock = new Mock<IAuthorizer>();
			AuthorizationResult authorizationResult = new AuthorizationResult(account);
			mock
				.Setup(x => x.Check(It.IsAny<string>(), It.IsAny<CancellationToken>()))
				.ReturnsAsync(authorizationResult);
			return mock.Object;
		}
	}
}
