using System;
using Answers.UseCases.Common.Clocks;
using Moq;


namespace UseCasesIntegrationTests.TestHelpers
{
    public static class TestClock
    {
        public static IClock Create(DateTime creationDate)
        {
            Mock<IClock> clockMock = new Mock<IClock>();
            clockMock.Setup(x => x.GetNow()).Returns(creationDate);
            return clockMock.Object;
        }


        public static IClock Create()
        {
            return Create(DateTime.Now);
        }
    }
}
