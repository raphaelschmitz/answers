using Answers.UseCases.Common.Validators;
using Moq;


namespace UseCasesIntegrationTests.TestHelpers
{
	public static class TestInputValidator
	{
		public static IValidator<T> Create<T>(params string[] returnErrors)
		{
			Mock<IValidator<T>> mock = new Mock<IValidator<T>>();
			mock
				.Setup(x => x.Validate(It.IsAny<T>()))
				.Returns(new ValidationResult(returnErrors));
			return mock.Object;
		}
	}
}
