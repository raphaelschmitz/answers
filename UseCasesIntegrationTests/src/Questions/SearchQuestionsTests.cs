using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Answers.Entities.Models;
using Answers.UseCases.Questions.SearchQuestions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NHibernate;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Questions
{
	public class SearchQuestionsTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void CanFindQuestion()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			DateTime creationDate = DateTime.Now;
			Question question = Question.Create(account, creationDate, "AAAAAABAAAAA", "");
			Question dummy = Question.Create(account, creationDate, "no", "");
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(question);
				await Session.SaveAsync(dummy);
				await transaction.CommitAsync();
			}

			SearchQuestionsInput searchQuestionsInput = new SearchQuestionsInput {SearchString = "ABA"};
			SearchQuestionOutputMapper mapper = new SearchQuestionOutputMapper();
			ILogger<string> logger = new Mock<ILogger<string>>().Object;
			SearchQuestionsUseCase searchQuestionsUseCase = new SearchQuestionsUseCase(Session, mapper, logger);


			// Act
			ActionResult<SearchQuestionsOutput> actionResult =
				await searchQuestionsUseCase.Execute(searchQuestionsInput, CancellationToken.None);


			// Assert
			SearchQuestionsOutput getQuestionOutput = actionResult.Value;
			IEnumerable<SearchQuestionsOutput.QuestionDto> questionEntries = getQuestionOutput.Questions.ToList();
			Assert.Single(questionEntries);

			SearchQuestionsOutput.QuestionDto questionDto = questionEntries.First();
			Assert.Equal(question.Id, questionDto.Id);
			Assert.Equal(question.CurrentTitle, questionDto.Title);

			PostRevision currentRevision = question.Post.CurrentRevision;
			Assert.Equal(currentRevision.Content, questionDto.Gist);
			Assert.Equal(currentRevision.CreationDate, questionDto.CreationDate);

			SearchQuestionsOutput.UserDto userDto = questionDto.User;
			Assert.Equal(account.Id, userDto.Id);
			Assert.Equal(account.EmailAddress, userDto.Email);
			Assert.Equal(account.UserName, userDto.UserName);
		}
	}
}
