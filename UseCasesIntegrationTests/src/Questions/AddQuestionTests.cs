using System;
using System.Threading;
using Answers.Entities.Models;
using Answers.UseCases.Common.Authorization;
using Answers.UseCases.Common.Clocks;
using Answers.UseCases.Questions.AddQuestion;
using Microsoft.AspNetCore.Mvc;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Questions
{
	public class AddQuestionTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void DeniesIfNotAuthorized()
		{
			// Arrange
			IClock clock = TestClock.Create();
			var inputValidator = TestInputValidator.Create<AddQuestionInput>();
			IAuthorizer authorizer = TestAuthorizer.Create(null);
			AddQuestionUseCase addQuestionUseCase = new AddQuestionUseCase(Session, inputValidator, authorizer, clock);
			AddQuestionInput addQuestionInput = new AddQuestionInput();


			// Act
			ActionResult<AddQuestionOutput> actionResult =
				await addQuestionUseCase.Execute(addQuestionInput, CancellationToken.None);


			// Assert
			Assert.IsType<UnauthorizedResult>(actionResult.Result);
		}


		[Fact]
		public async void DeniesIfNotValidated()
		{
			// Arrange
			IClock clock = TestClock.Create();
			var inputValidator = TestInputValidator.Create<AddQuestionInput>("error");
			IAuthorizer authorizer = TestAuthorizer.Create(null);
			AddQuestionUseCase addQuestionUseCase = new AddQuestionUseCase(Session, inputValidator, authorizer, clock);

			AddQuestionInput addQuestionInput = new AddQuestionInput
			{
				JsonWebToken = "",
				Title = "too short",
				Content = "too short"
			};


			// Act
			ActionResult<AddQuestionOutput> actionResult =
				await addQuestionUseCase.Execute(addQuestionInput, CancellationToken.None);


			// Assert
			Assert.IsType<BadRequestObjectResult>(actionResult.Result);
		}


		[Fact]
		public async void DataIsPersisted()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);

			IClock clock = TestClock.Create();
			IAuthorizer authorizer = TestAuthorizer.Create(account);
			var inputValidator = TestInputValidator.Create<AddQuestionInput>();
			AddQuestionUseCase addQuestionUseCase = new AddQuestionUseCase(Session, inputValidator, authorizer, clock);

			string title = "sgsthb";
			string content = "louahhlefnucgir";
			AddQuestionInput addQuestionInput = new AddQuestionInput
			{
				JsonWebToken = "",
				Title = title,
				Content = content
			};


			// Act
			ActionResult<AddQuestionOutput> actionResult =
				await addQuestionUseCase.Execute(addQuestionInput, CancellationToken.None);


			// Assert
			AddQuestionOutput addQuestionOutput = actionResult.Value;
			Guid questionId = addQuestionOutput.Id;

			Question question = await Session.GetAsync<Question>(questionId);
			Assert.NotNull(question);
			Assert.Equal(account, question.Post.CurrentRevision.Account);
			Assert.Equal(content, question.Post.CurrentRevision.Content);
		}
	}
}
