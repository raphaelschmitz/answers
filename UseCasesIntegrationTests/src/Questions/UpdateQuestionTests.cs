using System;
using System.Threading;
using Answers.Entities.Models;
using Answers.UseCases.Common.Authorization;
using Answers.UseCases.Common.Clocks;
using Answers.UseCases.Questions.UpdateQuestion;
using Microsoft.AspNetCore.Mvc;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Questions
{
	public class UpdateQuestionTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void DeniesUnauthorized()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);

			DateTime creationDate = DateTime.Now;
			IClock clock = TestClock.Create(creationDate);
			IAuthorizer authorizer = TestAuthorizer.Create(null);
			UpdateQuestionUseCase updateQuestionUseCase = new UpdateQuestionUseCase(Session, authorizer, clock);

			string title = "asdgbtsbersentms";
			string content = "afsegsntxrv";
			UpdateQuestionInput updateQuestionInput = new UpdateQuestionInput
			{
				QuestionId = question.Id,
				Title = title,
				Content = content
			};


			// Act
			ActionResult<UpdateQuestionOutput> actionResult =
				await updateQuestionUseCase.Execute(updateQuestionInput, CancellationToken.None);


			// Assert
			Assert.IsType<UnauthorizedResult>(actionResult.Result);
		}


		[Fact]
		public async void DataIsPersisted()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);

			DateTime creationDate = DateTime.Now;
			IClock clock = TestClock.Create(creationDate);
			IAuthorizer authorizer = TestAuthorizer.Create(account);
			UpdateQuestionUseCase updateQuestionUseCase = new UpdateQuestionUseCase(Session, authorizer, clock);

			string title = "asdgbtsbersentms";
			string content = "afsegsntxrv";
			UpdateQuestionInput updateQuestionInput = new UpdateQuestionInput
			{
				QuestionId = question.Id,
				Title = title,
				Content = content
			};


			// Act
			ActionResult<UpdateQuestionOutput> actionResult =
				await updateQuestionUseCase.Execute(updateQuestionInput, CancellationToken.None);


			// Assert
			UpdateQuestionOutput updateQuestionOutput = actionResult.Value;
			Guid commentId = updateQuestionOutput.Id;

			PostRevision questionRevision = await Session.GetAsync<PostRevision>(commentId);
			Assert.NotNull(questionRevision);
			Assert.Equal(creationDate, questionRevision.CreationDate);
			Assert.Equal(content, questionRevision.Content);
			Assert.Equal(question.Post, questionRevision.Post);
			Assert.Equal(account, questionRevision.Account);

			Assert.Contains(questionRevision, question.Post.Revisions);
			Assert.Equal(questionRevision, question.Post.CurrentRevision);
		}
	}
}
