using System.Threading;
using Answers.Entities.Models;
using Answers.UseCases.Common.Authorization;
using Answers.UseCases.Common.Validators;
using Answers.UseCases.Questions.GetQuestion;
using Microsoft.AspNetCore.Mvc;
using Moq;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Questions
{
	public class GetQuestionTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void NotExistingQuestionReturns404()
		{
			// Arrange
			GetQuestionInput getQuestionInput = new GetQuestionInput();
			GetQuestionOutputMapper mapper = new GetQuestionOutputMapper();
			IValidator<GetQuestionInput> inputValidator = TestInputValidator.Create<GetQuestionInput>();
			Mock<IGistCreator> gistCreatorMock = new Mock<IGistCreator>();
			IGistCreator gistCreator = gistCreatorMock.Object;
			IAuthorizer authorizer = TestAuthorizer.Create(null);

			GetQuestionUseCase getQuestionUseCase =
				new GetQuestionUseCase(Session, mapper, inputValidator, gistCreator, authorizer);


			// Act
			ActionResult<GetQuestionOutput> actionResult =
				await getQuestionUseCase.Execute(getQuestionInput, CancellationToken.None);


			// Assert
			Assert.IsType<NotFoundResult>(actionResult.Result);
		}


		[Fact]
		public async void InvalidInputReturns400()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);

			GetQuestionOutputMapper mapper = new GetQuestionOutputMapper();
			var inputValidator = TestInputValidator.Create<GetQuestionInput>("error");
			Mock<IGistCreator> gistCreatorMock = new Mock<IGistCreator>();
			IAuthorizer authorizer = TestAuthorizer.Create(account);
			GetQuestionUseCase getQuestionUseCase =
				new GetQuestionUseCase(Session, mapper, inputValidator, gistCreatorMock.Object, authorizer);

			GetQuestionInput getQuestionInput = new GetQuestionInput {Id = question.Id};


			// Act
			ActionResult<GetQuestionOutput> actionResult =
				await getQuestionUseCase.Execute(getQuestionInput, CancellationToken.None);


			// Assert
			Assert.IsType<BadRequestObjectResult>(actionResult.Result);
		}


		[Fact]
		public async void CanGetQuestion()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);

			GetQuestionOutputMapper mapper = new GetQuestionOutputMapper();
			string gist = "Shrek 2";
			var inputValidator = TestInputValidator.Create<GetQuestionInput>();
			Mock<IGistCreator> gistCreatorMock = new Mock<IGistCreator>();
			gistCreatorMock
				.Setup(x => x.CreateGist(It.IsAny<string>(), It.IsAny<int>()))
				.Returns(gist);
			IAuthorizer authorizer = TestAuthorizer.Create(account);

			GetQuestionUseCase getQuestionUseCase =
				new GetQuestionUseCase(Session, mapper, inputValidator, gistCreatorMock.Object, authorizer);

			GetQuestionInput getQuestionInput = new GetQuestionInput
			{
				Id = question.Id,
				GistLength = 10
			};


			// Act
			ActionResult<GetQuestionOutput> actionResult =
				await getQuestionUseCase.Execute(getQuestionInput, CancellationToken.None);


			// Assert
			GetQuestionOutput result = actionResult.Value;
			Assert.Equal(question.Id, result.Id);
			Assert.Equal(question.CurrentTitle, result.Title);

			PostRevision currentRevision = question.Post.CurrentRevision;
			GetQuestionOutput.PostDto postDto = result.Post;
			Assert.Equal(gist, postDto.Content);
			Assert.Equal(currentRevision.CreationDate, postDto.CreationDate);

			Account currentRevisionAuthor = currentRevision.Account;
			GetQuestionOutput.UserDto lastChangeAuthor = postDto.LastChangeAuthor;
			Assert.Equal(currentRevisionAuthor.Id, lastChangeAuthor.Id);
			Assert.Equal(currentRevisionAuthor.UserName, lastChangeAuthor.UserName);

			Assert.Equal(account.Id, postDto.Creator.Id);
			Assert.Equal(account.UserName, postDto.Creator.UserName);

			Assert.Empty(postDto.Comments);
			Assert.Empty(result.Answers);
		}


		[Fact]
		public async void CanGetQuestionAuthorizedVersion()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);
			Vote vote = TestVote.Create(Session, question.Post, account, Vote.Up);

			GetQuestionOutputMapper mapper = new GetQuestionOutputMapper();
			string gist = "Shrek 2";
			var inputValidator = TestInputValidator.Create<GetQuestionInput>();
			Mock<IGistCreator> gistCreatorMock = new Mock<IGistCreator>();
			gistCreatorMock
				.Setup(x => x.CreateGist(It.IsAny<string>(), It.IsAny<int>()))
				.Returns(gist);
			IAuthorizer authorizer = TestAuthorizer.Create(account);

			GetQuestionUseCase getQuestionUseCase =
				new GetQuestionUseCase(Session, mapper, inputValidator, gistCreatorMock.Object, authorizer);

			GetQuestionInput getQuestionInput = new GetQuestionInput
			{
				Id = question.Id,
				GistLength = 10
			};


			// Act
			ActionResult<GetQuestionOutput> actionResult =
				await getQuestionUseCase.Execute(getQuestionInput, CancellationToken.None);


			// Assert
			GetQuestionOutput result = actionResult.Value;
			Assert.Equal(question.Id, result.Id);
			Assert.Equal(question.CurrentTitle, result.Title);

			PostRevision currentRevision = question.Post.CurrentRevision;
			GetQuestionOutput.PostDto postDto = result.Post;
			Assert.Equal(vote.Value, postDto.OwnVote);
			Assert.Equal(gist, postDto.Content);
			Assert.Equal(currentRevision.CreationDate, postDto.CreationDate);

			Account currentRevisionAuthor = currentRevision.Account;
			GetQuestionOutput.UserDto lastChangeAuthor = postDto.LastChangeAuthor;
			Assert.Equal(currentRevisionAuthor.Id, lastChangeAuthor.Id);
			Assert.Equal(currentRevisionAuthor.UserName, lastChangeAuthor.UserName);

			Assert.Equal(account.Id, postDto.Creator.Id);
			Assert.Equal(account.UserName, postDto.Creator.UserName);

			Assert.Empty(postDto.Comments);
			Assert.Empty(result.Answers);
		}
	}
}
