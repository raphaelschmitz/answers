using System.Threading;
using Answers.Entities.Models;
using Answers.UseCases.Common.Authorization;
using Answers.UseCases.Common.Clocks;
using Answers.UseCases.Questions.AddAnswer;
using Microsoft.AspNetCore.Mvc;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Questions
{
	public class AddAnswerTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void DeniesIfNotAuthorized()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);
			IAuthorizer authorizer = TestAuthorizer.Create(null);
			IClock clock = TestClock.Create();
			AddAnswerUseCase query = new AddAnswerUseCase(Session, authorizer, clock);

			AddAnswerInput input = new AddAnswerInput
			{
				QuestionId = question.Id,
				Content = ""
			};


			// Act
			ActionResult<AddAnswerOutput> output = await query.Execute(input, CancellationToken.None);
			AddAnswerOutput result = output.Value;


			// Assert
			Assert.IsType<UnauthorizedResult>(output.Result);
		}


		[Fact]
		public async void DataIsPersisted()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);
			IAuthorizer authorizer = TestAuthorizer.Create(account);
			IClock clock = TestClock.Create();
			AddAnswerUseCase query = new AddAnswerUseCase(Session, authorizer, clock);

			AddAnswerInput input = new AddAnswerInput
			{
				QuestionId = question.Id,
				Content = ""
			};


			// Act
			ActionResult<AddAnswerOutput> output = await query.Execute(input, CancellationToken.None);
			AddAnswerOutput result = output.Value;


			// Assert
			Post post = await Session.GetAsync<Post>(result.Id);
			Assert.NotNull(post);

			Assert.NotNull(post.CurrentRevision);
			Assert.Equal(1, post.Revisions.Count);

			Assert.Empty(post.Comments);
		}
	}
}
