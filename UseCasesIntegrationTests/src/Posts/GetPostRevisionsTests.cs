using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Answers.Entities.Models;
using Answers.UseCases.Posts.GetPostRevisions;
using Microsoft.AspNetCore.Mvc;
using NHibernate;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Posts
{
	public class GetPostRevisionsTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void CanGetQuestionRevisions()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			DateTime creationDate = DateTime.Now;
			Question question = Question.Create(account, creationDate, "AAAAAABAAAAA", "");
			using (ITransaction transaction = Session.BeginTransaction())
			{
				await Session.SaveAsync(question);
				await transaction.CommitAsync();
			}

			GetPostRevisionsInput input = new GetPostRevisionsInput {PostId = question.Post.Id};
			GetQuestionRevisionsOutputMapper mapper = new GetQuestionRevisionsOutputMapper();
			GetPostRevisionsUseCase query = new GetPostRevisionsUseCase(Session, mapper);


			// Act
			ActionResult<GetPostRevisionsOutput> actionResult = await query.Execute(input, CancellationToken.None);


			// Assert
			GetPostRevisionsOutput output = actionResult.Value;
			IList<GetPostRevisionsOutput.RevisionDto> revisions = output.QuestionRevisions.ToList();
			Assert.Single(revisions);

			PostRevision currentRevision = question.Post.CurrentRevision;
			GetPostRevisionsOutput.RevisionDto revisionDto = revisions.First();
			Assert.Equal(currentRevision.Id, revisionDto.Id);
			Assert.Equal(currentRevision.CreationDate, revisionDto.CreationDate);
			Assert.Equal(currentRevision.Content, revisionDto.Content);

			GetPostRevisionsOutput.AccountDto accountDto = revisionDto.Account;
			Assert.Equal(account.Id, accountDto.Id);
			Assert.Equal(account.UserName, accountDto.UserName);
		}
	}
}
