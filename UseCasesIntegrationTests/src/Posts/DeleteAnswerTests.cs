using System.Collections.Generic;
using System.Threading;
using Answers.Entities.Models;
using Answers.UseCases.Common.Authorization;
using Answers.UseCases.Posts.DeletePost;
using Microsoft.AspNetCore.Mvc;
using NHibernate.Linq;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Posts
{
	public class DeleteAnswerTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void DeniesUnauthorized()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);
			Post post = await TestAnswer.Create(Session, question, account);

			IAuthorizer authorizer = TestAuthorizer.Create(null);
			DeletePostUseCase query = new DeletePostUseCase(Session, authorizer);

			DeletePostInput input = new DeletePostInput { AnswerId = post.Id };


			// Act
			ActionResult<DeletePostOutput> output = await query.Execute(input, CancellationToken.None);


			// Assert
			Assert.IsType<UnauthorizedResult>(output.Result);
		}


		[Fact]
		public async void DataIsPersisted()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);
			Post post = await TestAnswer.Create(Session, question, account);

			IAuthorizer authorizer = TestAuthorizer.Create(account);
			DeletePostUseCase query = new DeletePostUseCase(Session, authorizer);

			DeletePostInput input = new DeletePostInput { AnswerId = post.Id };


			// Act
			ActionResult<DeletePostOutput> output = await query.Execute(input, CancellationToken.None);
			DeletePostOutput result = output.Value;


			// Assert
			Assert.True(result.Success);

			List<Post> answers = await Session.Query<Post>().ToListAsync();
			Assert.Single(answers);
		}
	}
}
