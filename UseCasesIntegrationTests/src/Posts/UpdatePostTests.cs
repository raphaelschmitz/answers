using System.Threading;
using Answers.Entities.Models;
using Answers.UseCases.Common.Authorization;
using Answers.UseCases.Common.Clocks;
using Answers.UseCases.Posts.UpdatePost;
using Microsoft.AspNetCore.Mvc;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Posts
{
	public class UpdatePostTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void DeniesUnauthorized()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);
			Post post = await TestAnswer.Create(Session, question, account);

			IClock clock = TestClock.Create();
			IAuthorizer authorizer = TestAuthorizer.Create(null);
			UpdatePostUseCase query = new UpdatePostUseCase(Session, authorizer, clock);

			string content = "dcecssddc";
			UpdatePostInput input = new UpdatePostInput
			{
				AnswerId = post.Id,
				Content = content
			};


			// Act
			ActionResult<UpdatePostOutput> output = await query.Execute(input, CancellationToken.None);


			// Assert
			Assert.IsType<UnauthorizedResult>(output.Result);
		}


		[Fact]
		public async void DataIsPersisted()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);
			Post post = await TestAnswer.Create(Session, question, account);

			IClock clock = TestClock.Create();
			IAuthorizer authorizer = TestAuthorizer.Create(account);
			UpdatePostUseCase query = new UpdatePostUseCase(Session, authorizer, clock);

			string content = "dcecssddc";
			UpdatePostInput input = new UpdatePostInput
			{
				AnswerId = post.Id,
				Content = content
			};


			// Act
			ActionResult<UpdatePostOutput> output = await query.Execute(input, CancellationToken.None);
			UpdatePostOutput result = output.Value;


			// Assert
			Assert.True(result.Success);

			Post updatedPost = await Session.GetAsync<Post>(post.Id);
			Assert.Equal(2, updatedPost.Revisions.Count);
			Assert.Equal(content, updatedPost.CurrentRevision.Content);
		}
	}
}
