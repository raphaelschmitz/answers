using System;
using System.Threading;
using Answers.Entities.Models;
using Answers.UseCases.Common.Authorization;
using Answers.UseCases.Posts.PlaceVote;
using Microsoft.AspNetCore.Mvc;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Posts
{
	public class PlaceVoteTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void DeniesUnauthorized()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);
			Post post = await TestAnswer.Create(Session, question, account);

			IAuthorizer authorizer = TestAuthorizer.Create(null);
			PlaceVoteUseCase query = new PlaceVoteUseCase(Session, authorizer);

			PlaceVoteInput input = new PlaceVoteInput
			{
				PostId = post.Id,
				Value = Vote.Up
			};


			// Act
			ActionResult<PlaceVoteOutput> output = await query.Execute(input, CancellationToken.None);


			// Assert
			Assert.IsType<UnauthorizedResult>(output.Result);
		}


		[Fact]
		public async void DataIsPersisted()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);
			Post post = await TestAnswer.Create(Session, question, account);

			Account voter = await TestAccount.Create(Session, "voter@vote.com");
			IAuthorizer authorizer = TestAuthorizer.Create(voter);
			PlaceVoteUseCase query = new PlaceVoteUseCase(Session, authorizer);

			int value = Vote.Up;
			PlaceVoteInput input = new PlaceVoteInput
			{
				PostId = post.Id,
				Value = value
			};


			// Act
			ActionResult<PlaceVoteOutput> output = await query.Execute(input, CancellationToken.None);
			PlaceVoteOutput result = output.Value;


			// Assert
			Assert.NotNull(result);
			Assert.NotEqual(Guid.Empty, result.Id);

			Vote placedVote = await Session.GetAsync<Vote>(result.Id);
			Assert.Equal(post, placedVote.Post);
			Assert.Equal(voter, placedVote.Voter);
			Assert.Equal(account, placedVote.Beneficiary);
			Assert.Equal(value, placedVote.Value);
		}
	}
}
