using System;
using System.Security.Claims;
using System.Threading;
using Answers.Entities.Models;
using Answers.Entities.Values;
using Answers.UseCases.Common.Authorization;
using Answers.UseCases.Common.Clocks;
using Answers.UseCases.Posts.AddComment;
using Microsoft.AspNetCore.Mvc;
using UseCasesIntegrationTests.TestHelpers;
using Xunit;


namespace UseCasesIntegrationTests.Posts
{
	public class AddAnswerCommentTests : InMemoryDatabaseTest
	{
		[Fact]
		public async void DeniesIfUnauthorized()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);
			Post post = await TestAnswer.Create(Session, question, account);

			DateTime creationDate = DateTime.Now;
			IClock clock = TestClock.Create(creationDate);
			IAuthorizer authorizer = TestAuthorizer.Create(null);
			AddCommentUseCase query = new AddCommentUseCase(Session, authorizer, clock);

			Claim claim = new Claim(Allowable.AddAnswerComments.ClaimId, "");
			string content = "dcecssddc";
			AddCommentInput input = new AddCommentInput { AnswerId = post.Id, Content = content };


			// Act
			ActionResult<AddCommentOutput> output = await query.Execute(input, CancellationToken.None);


			// Assert
			Assert.IsType<UnauthorizedResult>(output.Result);
		}


		[Fact]
		public async void DataIsPersisted()
		{
			// Arrange
			Account account = await TestAccount.Create(Session);
			Question question = await TestQuestion.Create(Session, account);
			Post post = await TestAnswer.Create(Session, question, account);

			DateTime creationDate = DateTime.Now;
			IClock clock = TestClock.Create(creationDate);
			IAuthorizer authorizer = TestAuthorizer.Create(account);
			AddCommentUseCase query = new AddCommentUseCase(Session, authorizer, clock);

			Claim claim = new Claim(Allowable.AddAnswerComments.ClaimId, "");
			string content = "dcecssddc";
			AddCommentInput input = new AddCommentInput { AnswerId = post.Id, Content = content };


			// Act
			ActionResult<AddCommentOutput> output = await query.Execute(input, CancellationToken.None);
			AddCommentOutput result = output.Value;


			// Assert
			Comment comment = await Session.GetAsync<Comment>(result.Id);
			Assert.NotNull(comment);
			Assert.Equal(creationDate, comment.CreationDate);
			Assert.Equal(content, comment.Content);
			Assert.Equal(post, comment.Post);
			Assert.Equal(account, comment.Account);
		}
	}
}
