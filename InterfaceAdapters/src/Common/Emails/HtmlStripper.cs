using System.Text.RegularExpressions;


namespace Answers.InterfaceAdapters.Common.Emails
{
	public	class HtmlStripper : IHtmlStripper
	{
		public string Strip(string html)
		{
			return Regex.Replace(html, "<[a-zA-Z/].*?>", "");
		}
	}
}
