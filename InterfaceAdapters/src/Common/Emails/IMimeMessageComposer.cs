using System.Collections.Generic;
using Answers.Entities.Models;
using MimeKit;


namespace Answers.InterfaceAdapters.Common.Emails
{
	public interface IMimeMessageComposer
	{
		MimeMessage CreateMimeMessage(string from, IEnumerable<Account> to, string subject, string htmlBody);
	}
}
