using System;
using System.Collections.Generic;
using Answers.Entities.Models;
using MimeKit;


namespace Answers.InterfaceAdapters.Common.Emails
{
	public class MimeMessageComposer : IMimeMessageComposer
	{
		private readonly IHtmlStripper _htmlStripper;


		public MimeMessageComposer(IHtmlStripper htmlStripper)
		{
			_htmlStripper = htmlStripper;
		}


		MimeMessage IMimeMessageComposer.CreateMimeMessage(string from, IEnumerable<Account> to, string subject, string htmlBody)
		{
			if (from == null) throw new ArgumentNullException(nameof(from));
			if (to == null) throw new ArgumentNullException(nameof(to));
			if (subject == null) throw new ArgumentNullException(nameof(subject));
			if (htmlBody == null) throw new ArgumentNullException(nameof(htmlBody));

			MimeMessage message = new MimeMessage();

			MailboxAddress fromMailboxAddress = new MailboxAddress(from);
			message.From.Add(fromMailboxAddress);
			foreach (Account account in to)
			{
				MailboxAddress mailboxAddress = CreateMailboxAddress(account);
				message.To.Add(mailboxAddress);
			}

			message.Subject = subject;


			BodyBuilder bodyBuilder = new BodyBuilder();
			bodyBuilder.HtmlBody = htmlBody;
			bodyBuilder.TextBody = _htmlStripper.Strip(htmlBody);

			message.Body = bodyBuilder.ToMessageBody();
			return message;
		}


		private static MailboxAddress CreateMailboxAddress(Account account)
		{
			string userName = account.UserName;
			string emailAddress = account.EmailAddress;
			return new MailboxAddress(userName, emailAddress);
		}
	}
}
