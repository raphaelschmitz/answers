using Answers.UseCases.Common;


namespace Answers.InterfaceAdapters.Common.Emails
{
	public class EmailAddressValidator : IEmailAddressValidator
	{
		public bool Validate(string emailAddress)
		{
			if (!emailAddress.Contains('@')) return false;
			if (!emailAddress.Contains('.')) return false;
			return true;
		}
	}
}
