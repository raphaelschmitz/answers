using Answers.Entities.Models;
using MailKit.Net.Smtp;
using MimeKit;


namespace Answers.InterfaceAdapters.Common.Emails
{
	public class MimeMessageSender : IMimeMessageSender
	{
		void IMimeMessageSender.SendMimeMessage(WebsiteConfiguration websiteConfiguration, MimeMessage message)
		{
			string smtpAddress = websiteConfiguration.EmailSmtpAddress;
			int port = websiteConfiguration.EmailPort;
			bool useSsl = websiteConfiguration.EmailUseSsl;
			string userName = websiteConfiguration.EmailUserName;
			string password = websiteConfiguration.EmailPassword;


			using (SmtpClient client = new SmtpClient())
			{
				client.Connect(smtpAddress, port, useSsl);
				client.Authenticate(userName, password);
				client.Send(message);
				client.Disconnect(true);
			}
		}
	}
}
