namespace Answers.InterfaceAdapters.Common.Emails
{
	public interface IHtmlStripper
	{
		string Strip(string html);
	}
}
