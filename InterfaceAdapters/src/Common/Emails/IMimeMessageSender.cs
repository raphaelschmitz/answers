using Answers.Entities.Models;
using MimeKit;


namespace Answers.InterfaceAdapters.Common.Emails
{
	public interface IMimeMessageSender
	{
		void SendMimeMessage(WebsiteConfiguration websiteConfiguration, MimeMessage message);
	}
}
