using System;
using Answers.UseCases.Common.Clocks;


namespace Answers.InterfaceAdapters.Common
{
	public class Clock : IClock
	{
		public DateTime GetNow()
		{
			return DateTime.UtcNow;
		}
	}
}
