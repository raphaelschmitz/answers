using System;
using Answers.Entities.Models;
using Answers.InterfaceAdapters.Common.Emails;
using Answers.UseCases.Accounts.SignUp;
using MimeKit;


namespace Answers.InterfaceAdapters.Accounts.Emails
{
	public class RegistrationMailSender : IRegistrationMailSender
	{
		private readonly IRegistrationTemplateFormatter _registrationTemplateFormatter;
		private readonly IMimeMessageComposer _mimeMessageComposer;
		private readonly IMimeMessageSender _mimeMessageSender;


		public RegistrationMailSender(
			IRegistrationTemplateFormatter registrationTemplateFormatter,
			IMimeMessageComposer mimeMessageComposer,
			IMimeMessageSender mimeMessageSender)
		{
			_registrationTemplateFormatter = registrationTemplateFormatter;
			_mimeMessageComposer = mimeMessageComposer;
			_mimeMessageSender = mimeMessageSender;
		}


		public void Send(WebsiteConfiguration websiteConfiguration, EmailConfirmProcess emailConfirmProcess)
		{
			string from = websiteConfiguration.EmailFromAddress;

			Account account = emailConfirmProcess.Account;
			Account[] to = { account };

			string subject = "Registration";

			string template = websiteConfiguration.RegistrationEmailTemplate;
			string body = _registrationTemplateFormatter.Format(template, account, emailConfirmProcess);

			MimeMessage message = _mimeMessageComposer.CreateMimeMessage(from, to, subject, body);


			_mimeMessageSender.SendMimeMessage(websiteConfiguration, message);
		}
	}
}
