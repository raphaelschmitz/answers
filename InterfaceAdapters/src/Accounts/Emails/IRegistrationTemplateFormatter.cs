using Answers.Entities.Models;


namespace Answers.InterfaceAdapters.Accounts.Emails
{
	public interface IRegistrationTemplateFormatter
	{
		string Format(string template, Account account, EmailConfirmProcess emailConfirmProcess);
	}
}
