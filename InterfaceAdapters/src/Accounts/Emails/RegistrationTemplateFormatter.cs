using Answers.Entities.Models;


namespace Answers.InterfaceAdapters.Accounts.Emails
{
	public class RegistrationTemplateFormatter : IRegistrationTemplateFormatter
	{
		public string Format(string template, Account account, EmailConfirmProcess emailConfirmProcess)
		{
			return template;
		}
	}
}
