namespace Answers.InterfaceAdapters.Accounts.JsonWebTokens
{
	public class JwtConfiguration
	{
		public static readonly string JsonElementName = "Jwt";

		public string Secret { get; set; }
		public string Issuer { get; set; }
		public string Audience { get; set; }
		public string ExpirationTime { get; set; }
	}
}
