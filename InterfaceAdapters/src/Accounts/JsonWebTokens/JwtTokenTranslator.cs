using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Answers.InterfaceAdapters.Accounts.Authorizers;
using Answers.UseCases.Common.Clocks;
using Microsoft.IdentityModel.Tokens;


namespace Answers.InterfaceAdapters.Accounts.JsonWebTokens
{
	public class JwtTokenTranslator : ITokenTranslator
	{
		private readonly JwtConfiguration _jwtConfiguration;
		private readonly IClock _clock;


		public JwtTokenTranslator(JwtConfiguration jwtConfiguration, IClock clock)
		{
			_jwtConfiguration = jwtConfiguration;
			_clock = clock;
		}


		public string CreateToken(IEnumerable<Claim> claims)
		{
			string issuer = _jwtConfiguration.Issuer;
			string audience = _jwtConfiguration.Audience;
			List<Claim> claimsIncludingUserId = claims.ToList();
			DateTime notBefore = _clock.GetNow();
			TimeSpan expirationTimeSpan = TimeSpan.Parse(_jwtConfiguration.ExpirationTime);
			DateTime expires = notBefore + expirationTimeSpan;

			string secretKey = _jwtConfiguration.Secret;
			byte[] keyAsBytes = Encoding.UTF8.GetBytes(secretKey);
			SymmetricSecurityKey securityKey = new SymmetricSecurityKey(keyAsBytes);
			SigningCredentials signingCredentials =
				new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);

			JwtSecurityToken jwtSecurityToken =
				new JwtSecurityToken(issuer, audience, claimsIncludingUserId, notBefore, expires, signingCredentials);

			JwtSecurityTokenHandler jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
			return jwtSecurityTokenHandler.WriteToken(jwtSecurityToken);
		}


		public ClaimsPrincipal ReadToken(string tokenString)
		{
			JwtSecurityTokenHandler jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
			if (!jwtSecurityTokenHandler.CanReadToken(tokenString)) return null;

			string secretString = _jwtConfiguration.Secret;
			var secretKey = Encoding.UTF8.GetBytes(secretString);
			var validationParameters = new TokenValidationParameters()
			{
				RequireExpirationTime = true,
				ValidateIssuer = false,
				ValidateAudience = false,
				IssuerSigningKey = new SymmetricSecurityKey(secretKey)
			};


			try
			{
				return jwtSecurityTokenHandler.ValidateToken(tokenString, validationParameters, out SecurityToken _);
			}
			catch (SecurityTokenException)
			{
				return null;
			}

		}
	}
}
