using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using Answers.UseCases.Common;


namespace Answers.InterfaceAdapters.Accounts.PasswordHashing
{
	public class PasswordHasher : IPasswordHasher
	{
		private readonly int _saltLength = 16;
		private readonly int _hashLength = 64;
		private readonly int _iterations = 10000;
		private readonly Random _random = new Random();


		string IPasswordHasher.CreateSaltedHash(string password)
		{
			byte[] salt = CreateRandomSalt();
			SaltedHash saltedHash = CreateSaltedHash(password, salt);
			return saltedHash.AsString();
		}


		bool IPasswordHasher.CheckIfPasswordMatches(string input, string saltedHash)
		{
			saltedHash = ReplaceWithFakeIfNecessary(saltedHash);
			SaltedHash saltedHashToUse = SaltedHash.FromString(saltedHash, _saltLength);
			byte[] salt = saltedHashToUse.Salt;
			SaltedHash hashGeneratedFromInput = CreateSaltedHash(input, salt);
			return saltedHashToUse.CheckIfItMatchesInConstantTime(hashGeneratedFromInput);
		}


		private byte[] CreateRandomSalt()
		{
			byte[] salt;
			using (RNGCryptoServiceProvider rngCryptoServiceProvider = new RNGCryptoServiceProvider())
			{
				rngCryptoServiceProvider.GetBytes(salt = new byte[_saltLength]);
			}
			return salt;
		}


		private SaltedHash CreateSaltedHash(string password, byte[] salt)
		{
			using (var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, salt, _iterations, HashAlgorithmName.SHA512))
			{
				var hash = rfc2898DeriveBytes.GetBytes(_hashLength);
				return new SaltedHash(salt, hash);
			}
		}

		[MethodImpl(MethodImplOptions.NoOptimization)]
		private string ReplaceWithFakeIfNecessary(string saltedHash)
		{
			var saltedHashString = CreateFakeSaltedHash();
			if (saltedHash != null)
			{
				saltedHashString = saltedHash;
			}
			return saltedHashString;
		}


		private string CreateFakeSaltedHash()
		{
			const string charsToChooseFrom = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			var chars = Enumerable.Repeat(charsToChooseFrom, _saltLength + _hashLength)
				.Select(s => s[_random.Next(s.Length)])
				.ToArray();
			return new string(chars);
		}
	}
}
