using System;
using System.Runtime.CompilerServices;


namespace Answers.InterfaceAdapters.Accounts.PasswordHashing
{
	public class SaltedHash
	{
		public SaltedHash(byte[] salt, byte[] hash)
		{
			Salt = salt;
			Hash = hash;
		}


		public byte[] Salt { get; }
		public byte[] Hash { get; }


		public static SaltedHash FromString(string saltedHashString, int saltLength)
		{
			byte[] saltedHashBytes = Convert.FromBase64String(saltedHashString);
			int hashLength = saltedHashBytes.Length - saltLength;
			byte[] salt = new byte[saltLength];
			byte[] hash = new byte[hashLength];
			Array.Copy(saltedHashBytes, 0, salt, 0, saltLength);
			Array.Copy(saltedHashBytes, saltLength, hash, 0, hashLength);
			return new SaltedHash(salt, hash);
		}


		public string AsString()
		{
			byte[] hashBytes = new byte[Salt.Length + Hash.Length];
			Array.Copy(Salt, 0, hashBytes, 0, Salt.Length);
			Array.Copy(Hash, 0, hashBytes, Salt.Length, Hash.Length);
			return Convert.ToBase64String(hashBytes);
		}


		[MethodImpl(MethodImplOptions.NoOptimization)]
		public bool CheckIfItMatchesInConstantTime(SaltedHash other)
		{
			bool matchesPassword = true;
			for (int i = 0; i < Hash.Length; i++)
			{
				if (Hash[i] == other.Hash[i]) continue;
				matchesPassword = false;
			}

			return matchesPassword;
		}
	}
}
