using System.Collections.Generic;
using System.Security.Claims;


namespace Answers.InterfaceAdapters.Accounts.Authorizers
{
	public interface ITokenTranslator
	{
		string CreateToken(IEnumerable<Claim> claims);
		ClaimsPrincipal ReadToken(string tokenString);
	}
}
