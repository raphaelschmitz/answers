using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;


namespace Answers.InterfaceAdapters.Accounts.Authorizers
{
	public class UserIdClaim
	{
		private static string ClaimId = "UserId";


		public Claim Create(Guid guid)
		{
			string userId = guid.ToString();
			return new Claim(ClaimId, userId);
		}


		public Guid Find(IEnumerable<Claim> claims)
		{
			Claim userIdClaim = claims.FirstOrDefault(x => x.Type == ClaimId);
			if (userIdClaim == null) return Guid.Empty;

			return Guid.Parse(userIdClaim.Value);
		}
	}
}
