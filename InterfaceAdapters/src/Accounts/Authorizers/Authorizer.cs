using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.Entities.Values;
using Answers.UseCases.Common.Authorization;
using NHibernate;


namespace Answers.InterfaceAdapters.Accounts.Authorizers
{
	public class Authorizer : IAuthorizer
	{
		private static string UserNameClaimId = "UserName";

		private readonly ISession _session;
		private readonly ITokenTranslator _tokenTranslator;
		private readonly UserIdClaim _userIdClaim;


		public Authorizer(ISession session, ITokenTranslator tokenTranslator, UserIdClaim userIdClaim)
		{
			_session = session;
			_tokenTranslator = tokenTranslator;
			_userIdClaim = userIdClaim;
		}


		string IAuthorizer.CreateToken(Account account)
		{
			IList<Claim> claims = account.GetPermissions()
				.Select(x => x.ToClaim())
				.ToList();

			Guid guid = account.Id;
			Claim userIdClaim = _userIdClaim.Create(guid);
			claims.Add(userIdClaim);

			string userName = account.UserName;
			claims.Add(new Claim(UserNameClaimId, userName));

			return _tokenTranslator.CreateToken(claims);
		}


		async Task<AuthorizationResult> IAuthorizer.Check(string token, CancellationToken cancellationToken)
		{
			ClaimsPrincipal claimsPrincipal = _tokenTranslator.ReadToken(token);
			if (claimsPrincipal == null) return new AuthorizationResult(null);
			
			IEnumerable<Claim> claims = claimsPrincipal.Claims;
			Guid guid = _userIdClaim.Find(claims);
			Account account = await _session.GetAsync<Account>(guid, cancellationToken);
			return new AuthorizationResult(account);
		}
	}
}
