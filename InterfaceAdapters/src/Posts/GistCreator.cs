using System;
using Answers.UseCases.Questions.GetQuestion;


namespace Answers.InterfaceAdapters.Posts
{
	public class GistCreator : IGistCreator
	{
		private static readonly string Ellipsis = "...";


		public string CreateGist(string fullText, int maxLength)
		{
			if (fullText == null) throw new ArgumentNullException(nameof(fullText));

			int inputLength = fullText.Length;
			bool needsToBeTrimmed = inputLength > maxLength;
			if (!needsToBeTrimmed) return fullText;

			int lengthOfEllipsis = Ellipsis.Length;
			int maxCutoff = maxLength - lengthOfEllipsis;
			for (int cutoff = maxCutoff; cutoff > lengthOfEllipsis; cutoff--)
			{
				bool isWhiteSpace = char.IsWhiteSpace(fullText, cutoff);
				if (isWhiteSpace) return ReturnSubStringWithEllipsis(fullText, cutoff);
			}

			return ReturnSubStringWithEllipsis(fullText, maxCutoff);
		}


		private string ReturnSubStringWithEllipsis(string fullText, int cutoff)
		{
			string gistTexto = fullText.Substring(0, cutoff);
			return gistTexto + Ellipsis;
		}
	}
}
