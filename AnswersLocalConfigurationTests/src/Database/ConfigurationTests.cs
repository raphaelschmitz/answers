using System.IO;
using Answers.Details.Installers.Database;
using Microsoft.Extensions.Configuration.UserSecrets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;
using Xunit.Abstractions;


namespace AnswersLocalConfigurationTests.Database
{
	public class ConfigurationTests
	{
		private readonly ITestOutputHelper _testOutputHelper;


		public ConfigurationTests(ITestOutputHelper testOutputHelper)
		{
			_testOutputHelper = testOutputHelper;
		}


		[Fact]
		public void NecessarySecretsAreAvailable()
		{
			_testOutputHelper.WriteLine(
				"If this test fails, you probably just need to add the usersecrets, as described at " +
				"https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets");

			string pathToSecretsFile =
				PathHelper.GetSecretsPathFromSecretsId(AnswersProjectConfiguration.UserSecretsId);
			using (StreamReader streamReader = File.OpenText(pathToSecretsFile))
			{
				JsonReader jsonReader = new JsonTextReader(streamReader);
				JObject root = JObject.Load(jsonReader);
				JToken jToken = root.GetValue(DbConfiguration.JsonElementName);
				DbConfiguration dbConfiguration = jToken.ToObject<DbConfiguration>();

				Assert.False(string.IsNullOrEmpty(dbConfiguration.ConnectionString));
			}
		}
	}
}
