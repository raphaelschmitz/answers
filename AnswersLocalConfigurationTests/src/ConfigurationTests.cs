using System.IO;
using Microsoft.Extensions.Configuration.UserSecrets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;


namespace AnswersLocalConfigurationTests
{
	public class ConfigurationTests
	{
		[Fact]
		public void ConfigurationIsValidJson()
		{
			string answerProjectFolder = AnswersProjectConfiguration.GetProjectFolder();
			string pathToConfigFile = Path.Combine(answerProjectFolder, "appsettings.json");

			using (StreamReader streamReader = File.OpenText(pathToConfigFile))
			{
				JsonReader jsonReader = new JsonTextReader(streamReader);
				JObject.Load(jsonReader);
			}
		}


		[Fact]
		public void SecretsAreValidJson()
		{
			string pathToSecretsFile =
				PathHelper.GetSecretsPathFromSecretsId(AnswersProjectConfiguration.UserSecretsId);
			using (StreamReader streamReader = File.OpenText(pathToSecretsFile))
			{
				JsonReader jsonReader = new JsonTextReader(streamReader);
				JObject.Load(jsonReader);
			}
		}
	}
}
