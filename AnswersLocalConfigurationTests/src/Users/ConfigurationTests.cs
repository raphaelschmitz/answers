using System.IO;
using Answers.InterfaceAdapters.Accounts.JsonWebTokens;
using Microsoft.Extensions.Configuration.UserSecrets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;
using Xunit.Abstractions;


namespace AnswersLocalConfigurationTests.Users
{
	public class ConfigurationTests
	{
		private readonly ITestOutputHelper _testOutputHelper;


		public ConfigurationTests(ITestOutputHelper testOutputHelper)
		{
			_testOutputHelper = testOutputHelper;
		}


		[Fact]
		public void NecessarySecretsAreAvailable()
		{
			_testOutputHelper.WriteLine(
				"If this test fails, you probably just need to add the usersecrets, as described at " +
				"https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets");

			string userSecretsId = AnswersProjectConfiguration.UserSecretsId;
			string pathToSecretsFile = PathHelper.GetSecretsPathFromSecretsId(userSecretsId);
			using (StreamReader streamReader = File.OpenText(pathToSecretsFile))
			{
				JsonReader jsonReader = new JsonTextReader(streamReader);
				JObject root = JObject.Load(jsonReader);
				JToken jToken = root.GetValue(JwtConfiguration.JsonElementName);
				JwtConfiguration jwtConfiguration = jToken.ToObject<JwtConfiguration>();

				Assert.False(string.IsNullOrEmpty(jwtConfiguration.Secret));
			}
		}
	}
}
