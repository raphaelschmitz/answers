using System;
using System.IO;
using System.Reflection;


namespace AnswersLocalConfigurationTests
{
	public static class AnswersProjectConfiguration
	{
		public static readonly string UserSecretsId = "AnswersApi";

		private static readonly string ProjectFolderName = "Answers";


		public static string GetProjectFolder()
		{
			string codeBase = Assembly.GetExecutingAssembly().CodeBase;
			UriBuilder uri = new UriBuilder(codeBase);
			string path = Uri.UnescapeDataString(uri.Path);
			DirectoryInfo netcoreappFolder = Directory.GetParent(path);
			DirectoryInfo solutionDirectory = netcoreappFolder.Parent.Parent.Parent.Parent;

			string answerProjectFolder = Path.Combine(solutionDirectory.FullName, ProjectFolderName);
			return answerProjectFolder;
		}
	}
}
