using System;


namespace Answers.UseCases.Posts.AddComment
{
	public class AddCommentInput
	{
		public string JsonWebToken { get; set; }
		public Guid AnswerId { get; set; }
		public string Content { get; set; }


		public override string ToString()
		{
			return $"{GetType().Name}\n" +
			       $"{nameof(JsonWebToken)}: {JsonWebToken}\n" +
			       $"{nameof(AnswerId)}: {AnswerId}\n" +
			       $"{nameof(Content)}: {Content}\n";
		}
	}
}
