using System;


namespace Answers.UseCases.Posts.AddComment
{
	public class AddCommentOutput
	{
		public Guid Id { get; set; }
	}
}
