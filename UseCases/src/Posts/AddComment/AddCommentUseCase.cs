using System;
using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.Entities.Values;
using Answers.UseCases.Common.Authorization;
using Answers.UseCases.Common.Clocks;
using Microsoft.AspNetCore.Mvc;
using NHibernate;


namespace Answers.UseCases.Posts.AddComment
{
	public class AddCommentUseCase
	{
		private readonly ISession _session;
		private readonly IAuthorizer _authorizer;
		private readonly IClock _clock;


		public AddCommentUseCase(
			ISession session,
			IAuthorizer authorizer,
			IClock clock)
		{
			_session = session;
			_authorizer = authorizer;
			_clock = clock;
		}


		public async Task<ActionResult<AddCommentOutput>> Execute(
			AddCommentInput input,
			CancellationToken cancellationToken)
		{
			using (ITransaction transaction = _session.BeginTransaction())
			{
				string token = input.JsonWebToken;
				AuthorizationResult authorizationResult = await _authorizer.Check(token, cancellationToken);
				if (!authorizationResult.IsAuthorizedTo(Allowable.AddAnswerComments)) return new UnauthorizedResult();

				Guid answerId = input.AnswerId;
				Post post = await _session.GetAsync<Post>(answerId, cancellationToken);
				if (post == null) return new BadRequestResult();

				Account account = authorizationResult.Account;
				DateTime creationDate = _clock.GetNow();
				string content = input.Content;
				Comment comment = Comment.Create(post, account, creationDate, content);

				await _session.SaveAsync(comment, cancellationToken);
				await transaction.CommitAsync(cancellationToken);
				return new AddCommentOutput {Id = comment.Id};
			}
		}
	}
}
