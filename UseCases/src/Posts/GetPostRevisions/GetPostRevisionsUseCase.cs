using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.UseCases.Common;
using Microsoft.AspNetCore.Mvc;
using NHibernate;


namespace Answers.UseCases.Posts.GetPostRevisions
{
	public class GetPostRevisionsUseCase
	{
		private readonly ISession _session;
		private readonly IMapper<Post, GetPostRevisionsOutput> _mapper;


		public GetPostRevisionsUseCase(ISession session, IMapper<Post, GetPostRevisionsOutput> mapper)
		{
			_session = session;
			_mapper = mapper;
		}


		public async Task<ActionResult<GetPostRevisionsOutput>> Execute(
			GetPostRevisionsInput input,
			CancellationToken cancellationToken)
		{
			Post question = await _session.GetAsync<Post>(input.PostId, cancellationToken);
			if (question == null) return new NotFoundResult();

			return _mapper.Map(question);
		}
	}
}
