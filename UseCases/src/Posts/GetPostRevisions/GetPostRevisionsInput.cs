using System;


namespace Answers.UseCases.Posts.GetPostRevisions
{
	public class GetPostRevisionsInput
	{
		public Guid PostId { get; set; }


		public override string ToString()
		{
			return $"{GetType().Name}\n" +
			       $"{nameof(PostId)}: {PostId}\n";
		}
	}
}
