using System;
using System.Collections.Generic;


namespace Answers.UseCases.Posts.GetPostRevisions
{
	public class GetPostRevisionsOutput
	{
		public string Title { get; set; }

		public IEnumerable<RevisionDto> QuestionRevisions { get; set; }



		public class RevisionDto
		{
			public Guid Id { get; set; }

			public DateTime CreationDate { get; set; }
			public string Content { get; set; }
			public AccountDto Account { get; set; }
		}



		public class AccountDto
		{
			public Guid Id { get; set; }

			public string UserName { get; set; }
			public int Points { get; set; }
		}
	}
}
