using System.Linq;
using Answers.Entities.Models;
using Answers.UseCases.Common;


namespace Answers.UseCases.Posts.GetPostRevisions
{
	public class GetQuestionRevisionsOutputMapper : IMapper<Post, GetPostRevisionsOutput>
	{
		public GetPostRevisionsOutput Map(Post post)
		{
			return new GetPostRevisionsOutput
			{
				Title = post.Question.CurrentTitle,
				QuestionRevisions = post.Revisions.Select(Map)
			};
		}


		private GetPostRevisionsOutput.RevisionDto Map(PostRevision postRevision)
		{
			return new GetPostRevisionsOutput.RevisionDto
			{
				Id = postRevision.Id,
				CreationDate = postRevision.CreationDate,
				Content = postRevision.Content,
				Account = Map(postRevision.Account)
			};
		}


		private GetPostRevisionsOutput.AccountDto Map(Account account)
		{
			return new GetPostRevisionsOutput.AccountDto
			{
				Id = account.Id,
				UserName = account.UserName,
				Points = account.ReceivedVotes.Sum(x => x.Value)
			};
		}
	}
}
