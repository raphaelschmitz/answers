using System;


namespace Answers.UseCases.Posts.PlaceVote
{
	public class PlaceVoteOutput
	{
		public Guid Id { get; set; }
	}
}
