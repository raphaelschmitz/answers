using System;


namespace Answers.UseCases.Posts.PlaceVote
{
	public class PlaceVoteInput
	{
		public string JsonWebToken { get; set; }
		public Guid PostId { get; set; }
		public int Value { get; set; }
	}
}
