using System;
using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.Entities.Values;
using Answers.UseCases.Common.Authorization;
using Microsoft.AspNetCore.Mvc;
using NHibernate;
using NHibernate.Linq;


namespace Answers.UseCases.Posts.PlaceVote
{
	public class PlaceVoteUseCase
	{
		private readonly ISession _session;
		private readonly IAuthorizer _authorizer;


		public PlaceVoteUseCase(
			ISession session,
			IAuthorizer authorizer
		)
		{
			_session = session;
			_authorizer = authorizer;
		}


		public async Task<ActionResult<PlaceVoteOutput>> Execute(
			PlaceVoteInput input,
			CancellationToken cancellationToken)
		{
			using (ITransaction transaction = _session.BeginTransaction())
			{
				string jsonWebToken = input.JsonWebToken;
				AuthorizationResult authorizationResult = await _authorizer.Check(jsonWebToken, cancellationToken);
				if (!authorizationResult.IsAuthorizedTo(Allowable.Vote)) return new UnauthorizedResult();

				Guid postId = input.PostId;
				Post post = await _session.GetAsync<Post>(postId, cancellationToken);
				if (post == null) return new BadRequestResult();

				Account account = authorizationResult.Account;
				Vote vote = await _session.Query<Vote>()
					.FirstOrDefaultAsync(x => x.Post == post && x.Voter == account, cancellationToken);
				if (vote == null) vote = Vote.Create(post, account, input.Value);
				vote.Value = input.Value;

				await _session.SaveAsync(vote, cancellationToken);
				await transaction.CommitAsync(cancellationToken);

				return new PlaceVoteOutput { Id = vote.Id };
			}
		}
	}
}
