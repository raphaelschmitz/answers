namespace Answers.UseCases.Posts.DeletePost
{
	public class DeletePostOutput
	{
        public bool Success { get; set; }
    }
}
