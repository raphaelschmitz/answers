using System;


namespace Answers.UseCases.Posts.DeletePost
{
	public class DeletePostInput
	{
		public string JsonWebToken { get; set; }
		public Guid AnswerId { get; set; }


		public override string ToString()
		{
			return $"{GetType().Name}\n" +
			       $"{nameof(JsonWebToken)}: {JsonWebToken}\n" +
			       $"{nameof(AnswerId)}: {AnswerId}\n";
		}
	}
}
