using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.Entities.Values;
using Answers.UseCases.Common.Authorization;
using Microsoft.AspNetCore.Mvc;
using NHibernate;


namespace Answers.UseCases.Posts.DeletePost
{
	public class DeletePostUseCase
	{
		private readonly ISession _session;
		private readonly IAuthorizer _authorizer;


		public DeletePostUseCase(ISession session, IAuthorizer authorizer)
		{
			_session = session;
			_authorizer = authorizer;
		}


		public async Task<ActionResult<DeletePostOutput>> Execute(
			DeletePostInput input,
			CancellationToken cancellationToken)
		{
			string jsonWebToken = input.JsonWebToken;
			AuthorizationResult authorizationResult = await _authorizer.Check(jsonWebToken, cancellationToken);
			if (!authorizationResult.IsAuthorizedTo(Allowable.DeleteAnswers)) return new UnauthorizedResult();

			Post post = await _session.GetAsync<Post>(input.AnswerId, cancellationToken);
			if (post == null) return new BadRequestResult();

			Question question = post.Question;
			question.Posts.Remove(post);
			using (ITransaction transaction = _session.BeginTransaction())
			{
				await _session.DeleteAsync(post, cancellationToken);
				await transaction.CommitAsync(cancellationToken);
			}

			return new DeletePostOutput { Success = true };
		}
	}
}
