namespace Answers.UseCases.Posts.UpdatePost
{
	public class UpdatePostOutput
	{
		public bool Success { get; set; }
	}
}
