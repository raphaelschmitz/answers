using System;


namespace Answers.UseCases.Posts.UpdatePost
{
	public class UpdatePostInput
	{
		public string JsonWebToken { get; set; }
		public Guid AnswerId { get; set; }
		public string Title { get; set; }
		public string Content { get; set; }


		public override string ToString()
		{
			return $"{GetType().Name}\n" +
			       $"{nameof(JsonWebToken)}: {JsonWebToken}\n" +
			       $"{nameof(AnswerId)}: {AnswerId}\n" +
			       $"{nameof(Title)}: {Title}\n" +
			       $"{nameof(Content)}: {Content}\n";
		}
	}
}
