using System;
using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.Entities.Values;
using Answers.UseCases.Common.Authorization;
using Answers.UseCases.Common.Clocks;
using Microsoft.AspNetCore.Mvc;
using NHibernate;


namespace Answers.UseCases.Posts.UpdatePost
{
	public class UpdatePostUseCase
	{
		private readonly ISession _session;
		private readonly IAuthorizer _authorizer;
		private readonly IClock _clock;


		public UpdatePostUseCase(
			ISession session,
			IAuthorizer authorizer,
			IClock clock)
		{
			_session = session;
			_authorizer = authorizer;
			_clock = clock;
		}


		public async Task<ActionResult<UpdatePostOutput>> Execute(
			UpdatePostInput input,
			CancellationToken cancellationToken)
		{
			using (ITransaction transaction = _session.BeginTransaction())
			{
				string jsonWebToken = input.JsonWebToken;
				AuthorizationResult authorizationResult = await _authorizer.Check(jsonWebToken, cancellationToken);
				if (!authorizationResult.IsAuthorizedTo(Allowable.UpdateAnswers)) return new UnauthorizedResult();

				Post post = await _session.GetAsync<Post>(input.AnswerId, cancellationToken);
				if (post == null) return new BadRequestResult();

				Account account = authorizationResult.Account;
				DateTime creationDate = _clock.GetNow();
				string title = input.Title ?? "";
				string content = input.Content;
				PostRevision.Create(post, account, creationDate, title, content);
				await _session.SaveAsync(post, cancellationToken);
				await transaction.CommitAsync(cancellationToken);
				return new UpdatePostOutput {Success = true};
			}
		}
	}
}
