using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.Entities.Values;
using Answers.UseCases.Common.Authorization;
using Microsoft.AspNetCore.Mvc;
using NHibernate;
using NHibernate.Linq;


namespace Answers.UseCases.Admin.GetDashboard
{
	public class GetDashboardUseCase
	{
		private readonly ISession _session;
		private readonly IAuthorizer _authorizer;


		public GetDashboardUseCase(ISession session, IAuthorizer authorizer)
		{
			_session = session;
			_authorizer = authorizer;
		}


		public async Task<ActionResult<GetDashboardOutput>> Execute(
			GetDashboardInput input,
			CancellationToken cancellationToken)
		{
			string jsonWebToken = input.JsonWebToken;
			AuthorizationResult authorizationResult = await _authorizer.Check(jsonWebToken, cancellationToken);
			if (!authorizationResult.IsAuthorizedTo(Allowable.AccessAdminArea)) return new UnauthorizedResult();

			WebsiteConfiguration websiteConfiguration = await _session.Query<WebsiteConfiguration>()
				.FirstAsync(cancellationToken);

			return new GetDashboardOutput
			{
				EmailSettings = new GetDashboardOutput.EmailSettingsDto
				{
					SmtpAddress = websiteConfiguration.EmailSmtpAddress,
					UserName = websiteConfiguration.EmailUserName,
					Password = websiteConfiguration.EmailPassword,
					Port = websiteConfiguration.EmailPort,
					UseSsl = websiteConfiguration.EmailUseSsl
				},
				RegistrationFromAddress = websiteConfiguration.EmailFromAddress,
				RegistrationEmailTemplate = websiteConfiguration.RegistrationEmailTemplate
			};
		}
	}
}
