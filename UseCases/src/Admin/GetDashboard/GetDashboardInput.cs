namespace Answers.UseCases.Admin.GetDashboard
{
	public class GetDashboardInput
	{
		public string JsonWebToken { get; set; }
	}
}
