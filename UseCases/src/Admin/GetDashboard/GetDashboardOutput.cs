namespace Answers.UseCases.Admin.GetDashboard
{
	public class GetDashboardOutput
	{
		public EmailSettingsDto EmailSettings { get; set; }

		public string RegistrationFromAddress { get; set; }
		public string RegistrationEmailTemplate { get; set; }


		public class EmailSettingsDto
		{
			public string SmtpAddress { get; set; }
			public int Port { get; set; }
			public bool UseSsl { get; set; }
			public string UserName { get; set; }
			public string Password { get; set; }
		}
	}
}
