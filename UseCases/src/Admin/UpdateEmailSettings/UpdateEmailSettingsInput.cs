namespace Answers.UseCases.Admin.UpdateEmailSettings
{
	public class UpdateEmailSettingsInput
	{
		public string JsonWebToken { get; set; }

		public string SmtpAddress { get; set; }
		public int Port { get; set; }
		public bool UseSsl { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
	}
}
