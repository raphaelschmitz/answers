using System;
using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.UseCases.Common.Authorization;
using Microsoft.AspNetCore.Mvc;
using NHibernate;
using NHibernate.Linq;


namespace Answers.UseCases.Admin.UpdateSettings
{
	public class UpdateSettingsUseCase
	{
		private readonly ISession _session;
		private readonly IAuthorizer _authorizer;


		public UpdateSettingsUseCase(ISession session, IAuthorizer authorizer)
		{
			_session = session;
			_authorizer = authorizer;
		}


		public async Task<ActionResult<UpdateSettingsOutput>> Execute(UpdateSettingsInput input, CancellationToken cancellationToken)
		{
			string jsonWebToken = input.JsonWebToken;
			AuthorizationResult authorizationResult = await _authorizer.Check(jsonWebToken, cancellationToken);
			if (!authorizationResult.IsAuthenticated) return new UnauthorizedResult();

			WebsiteConfiguration websiteConfiguration = await _session
				.Query<WebsiteConfiguration>()
				.FirstAsync(cancellationToken);

			websiteConfiguration.EmailFromAddress = input.RegistrationFromAddress;
			websiteConfiguration.RegistrationEmailTemplate = input.RegistrationEmailTemplate;
			using (ITransaction transaction = _session.BeginTransaction())
			{
				await _session.UpdateAsync(websiteConfiguration, cancellationToken);
				await transaction.CommitAsync(cancellationToken);
			}

			return new OkResult();
		}
	}
}
