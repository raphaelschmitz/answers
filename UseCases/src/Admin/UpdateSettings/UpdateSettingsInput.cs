namespace Answers.UseCases.Admin.UpdateSettings
{
	public class UpdateSettingsInput
	{
		public string JsonWebToken { get; set; }

		public string RegistrationFromAddress { get; set; }
		public string RegistrationEmailTemplate { get; set; }
	}
}
