using System;
using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.UseCases.Common.Authorization;
using Microsoft.AspNetCore.Mvc;
using NHibernate;


namespace Answers.UseCases.Tags.DeleteTag
{
	public class DeleteTagUseCase
	{
		private readonly ISession _session;
		private readonly IAuthorizer _authorizer;


		public DeleteTagUseCase(ISession session, IAuthorizer authorizer)
		{
			_session = session;
			_authorizer = authorizer;
		}


		public async Task<ActionResult<DeleteTagOutput>> Execute(
			DeleteTagInput input, CancellationToken cancellationToken)
		{
			string token = input.JsonWebToken;
			AuthorizationResult authorizationResult = await _authorizer.Check(token, cancellationToken);
			if (!authorizationResult.IsAuthenticated) return new UnauthorizedResult();

			Guid id = input.TagId;
			Tag tag = await _session.GetAsync<Tag>(id, cancellationToken);
			if (tag == null) return new NotFoundResult();

			using (ITransaction transaction = _session.BeginTransaction())
			{
				await _session.DeleteAsync(tag, cancellationToken);
				await transaction.CommitAsync(cancellationToken);
			}

			return new DeleteTagOutput { HasSucceeded = true };
		}
	}
}
