namespace Answers.UseCases.Tags.DeleteTag
{
	public class DeleteTagOutput
	{
		public bool HasSucceeded { get; set; }
	}
}
