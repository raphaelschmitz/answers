using System;


namespace Answers.UseCases.Tags.DeleteTag
{
	public class DeleteTagInput
	{
		public string JsonWebToken { get; set; }
		public Guid TagId { get; set; }
	}
}
