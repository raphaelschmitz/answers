using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.UseCases.Common;
using Microsoft.AspNetCore.Mvc;
using NHibernate;


namespace Answers.UseCases.Tags.GetTag
{
	public class GetTagUseCase
	{
		private readonly ISession _session;
		private readonly IMapper<Tag, GetTagOutput> _mapper;


		public GetTagUseCase(ISession session, IMapper<Tag, GetTagOutput> mapper)
		{
			_session = session;
			_mapper = mapper;
		}


		public async Task<ActionResult<GetTagOutput>> Execute(GetTagInput input, CancellationToken cancellationToken)
		{
			Guid id = input.Id;
			Tag tag = await _session.GetAsync<Tag>(id, cancellationToken);
			if (tag == null) return new NotFoundResult();

			return _mapper.Map(tag);
		}
	}
}
