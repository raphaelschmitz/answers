using System;


namespace Answers.UseCases.Tags.GetTag
{
	public class GetTagInput
	{
		public Guid Id { get; set; }
	}
}
