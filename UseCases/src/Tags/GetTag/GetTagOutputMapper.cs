using System.Linq;
using Answers.Entities.Models;
using Answers.UseCases.Common;


namespace Answers.UseCases.Tags.GetTag
{
	public class GetTagOutputMapper : IMapper<Tag, GetTagOutput>
	{
		public GetTagOutput Map(Tag tag)
		{
			return new GetTagOutput
			{
				Title = tag.Title,
				Description = tag.Description,
				Questions = tag.Questions.Select(Map)
			};
		}


		private GetTagOutput.QuestionDto Map(Question question)
		{
			return new GetTagOutput.QuestionDto
			{
				Id = question.Id,
				Points = question.Post.Votes.Sum(x => x.Value),
				Title = question.CurrentTitle,
				Gist = question.Post.CurrentRevision.Content,
				CreationDate = question.Post.CurrentRevision.CreationDate,
				User = Map(question.Post.Revisions.OrderBy(x => x.CreationDate).First().Account)
			};
		}


		private GetTagOutput.UserDto Map(Account account)
		{
			return new GetTagOutput.UserDto
			{
				Id = account.Id,
				Email = account.EmailAddress,
				UserName = account.UserName,
			};
		}
	}
}
