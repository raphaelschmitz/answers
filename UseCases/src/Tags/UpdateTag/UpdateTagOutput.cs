namespace Answers.UseCases.Tags.UpdateTag
{
	public class UpdateTagOutput
	{
		public bool HasSucceeded { get; set; }
	}
}
