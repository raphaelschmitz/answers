using System;
using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.UseCases.Common.Authorization;
using Microsoft.AspNetCore.Mvc;
using NHibernate;


namespace Answers.UseCases.Tags.UpdateTag
{
	public class UpdateTagUseCase
	{
		private readonly ISession _session;
		private readonly IAuthorizer _authorizer;


		public UpdateTagUseCase(ISession session, IAuthorizer authorizer)
		{
			_session = session;
			_authorizer = authorizer;
		}


		public async Task<ActionResult<UpdateTagOutput>> Execute(UpdateTagInput input, CancellationToken cancellationToken)
		{
			string token = input.JsonWebToken;
			AuthorizationResult authorizationResult = await _authorizer.Check(token, cancellationToken);
			if (!authorizationResult.IsAuthenticated) return new UnauthorizedResult();

			Guid id = input.Id;
			Tag tag = await _session.GetAsync<Tag>(id, cancellationToken);
			if (tag == null) return new NotFoundResult();

			tag.Title = input.Title;
			tag.Description = input.Description;
			using (ITransaction transaction = _session.BeginTransaction())
			{
				await _session.UpdateAsync(tag, cancellationToken);
				await transaction.CommitAsync(cancellationToken);
			}

			return new UpdateTagOutput { HasSucceeded = true };
		}
	}
}
