using System;


namespace Answers.UseCases.Tags.UpdateTag
{
	public class UpdateTagInput
	{
		public string JsonWebToken { get; set; }
		public Guid Id { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
	}
}
