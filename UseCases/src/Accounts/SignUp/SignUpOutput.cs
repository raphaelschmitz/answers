using System;


namespace Answers.UseCases.Accounts.SignUp
{
	public class SignUpOutput
	{
		public bool Success { get; set; }
		public Guid Id { get; set; }
	}
}
