using Answers.Entities.Models;


namespace Answers.UseCases.Accounts.SignUp
{
	public interface IRegistrationMailSender
	{
		void Send(WebsiteConfiguration websiteConfiguration, EmailConfirmProcess emailConfirmProcess);
	}
}