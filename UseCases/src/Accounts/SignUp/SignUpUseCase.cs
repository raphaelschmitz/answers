using System;
using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.UseCases.Common;
using Answers.UseCases.Common.Clocks;
using Microsoft.AspNetCore.Mvc;
using NHibernate;
using NHibernate.Linq;


namespace Answers.UseCases.Accounts.SignUp
{
	public class SignUpUseCase
	{
		private readonly ISession _session;
		private readonly IEmailAddressValidator _emailAddressValidator;
		private readonly IPasswordHasher _passwordHasher;
		private readonly IClock _clock;
		private readonly IRegistrationMailSender _registrationMailSender;


		public SignUpUseCase(ISession session,
			IEmailAddressValidator emailAddressValidator,
			IPasswordHasher passwordHasher,
			IClock clock,
			IRegistrationMailSender registrationMailSender)
		{
			_session = session;
			_emailAddressValidator = emailAddressValidator;
			_passwordHasher = passwordHasher;
			_clock = clock;
			_registrationMailSender = registrationMailSender;
		}


		public async Task<ActionResult<SignUpOutput>> Execute(
			SignUpInput input,
			CancellationToken cancellationToken)
		{
			string emailAddress = input.EmailAddress;
			bool emailIsValid = _emailAddressValidator.Validate(emailAddress);
			if (!emailIsValid) return new BadRequestResult();

			Account alreadyExistingAccount = await _session.Query<Account>()
				.FirstOrDefaultAsync(x => x.EmailAddress == emailAddress, cancellationToken);
			if (alreadyExistingAccount != null) return new ConflictResult();

			string password = input.Password;
			string saltedHash = _passwordHasher.CreateSaltedHash(password);
			Account account = Account.Create(emailAddress, saltedHash, input.UserName);
			string confirmKey = Guid.NewGuid().ToString();
			DateTime creationDate = _clock.GetNow();
			EmailConfirmProcess emailConfirmProcess = EmailConfirmProcess.Create(account, confirmKey, creationDate);

			using (ITransaction transaction = _session.BeginTransaction())
			{
				await _session.SaveAsync(account, cancellationToken);
				await _session.SaveAsync(emailConfirmProcess, cancellationToken);

				WebsiteConfiguration websiteConfiguration = await _session.Query<WebsiteConfiguration>()
					.FirstAsync(cancellationToken);
				_registrationMailSender.Send(websiteConfiguration, emailConfirmProcess);

				await transaction.CommitAsync(cancellationToken);
			}

			return new SignUpOutput
			{
				Success = true,
				Id = account.Id
			};
		}
	}
}
