namespace Answers.UseCases.Accounts.SignUp
{
	public class SignUpInput
	{
		public string EmailAddress { get; set; }
		public string Password { get; set; }
		public string UserName { get; set; }


		public override string ToString()
		{
			return $"{GetType().Name}\n" +
			       $"{nameof(EmailAddress)}: {EmailAddress}\n" +
			       $"{nameof(UserName)}: {UserName}\n";
		}
	}
}
