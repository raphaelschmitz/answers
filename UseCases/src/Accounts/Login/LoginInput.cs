namespace Answers.UseCases.Accounts.Login
{
	public class LoginInput
	{
		public string EmailAddress { get; set; }
		public string Password { get; set; }


		public override string ToString()
		{
			return $"{GetType().Name}\n" +
			       $"{nameof(EmailAddress)}: {EmailAddress}\n" +
			       $"{nameof(Password)}: {Password}\n";
		}
	}
}
