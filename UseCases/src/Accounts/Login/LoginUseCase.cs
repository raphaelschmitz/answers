using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.UseCases.Common;
using Answers.UseCases.Common.Authorization;
using Microsoft.AspNetCore.Mvc;
using NHibernate;
using NHibernate.Linq;


namespace Answers.UseCases.Accounts.Login
{
	public class LoginUseCase
	{
		private readonly ISession _session;
		private readonly IPasswordHasher _passwordHasher;
		private readonly IAuthorizer _authorizer;


		public LoginUseCase(ISession session, IPasswordHasher passwordHasher, IAuthorizer authorizer)
		{
			_session = session;
			_passwordHasher = passwordHasher;
			_authorizer = authorizer;
		}


		public async Task<ActionResult<LoginOutput>> Execute(
			LoginInput input,
			CancellationToken cancellationToken)
		{
			Account account = await _session.Query<Account>()
				.FirstOrDefaultAsync(x => x.EmailAddress == input.EmailAddress, cancellationToken);
			bool userExists = account != null;
			string hashedPassword = userExists ? account.PasswordHash : null;
			bool passwordIsValid = _passwordHasher.CheckIfPasswordMatches(input.Password, hashedPassword);
			if (!userExists || !passwordIsValid) return new BadRequestResult();

			string token = _authorizer.CreateToken(account);

			return new LoginOutput { Token = token };
		}
	}
}
