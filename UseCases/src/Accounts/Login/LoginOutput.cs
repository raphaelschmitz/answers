namespace Answers.UseCases.Accounts.Login
{
	public class LoginOutput
	{
		public string Token { get; set; }
	}
}
