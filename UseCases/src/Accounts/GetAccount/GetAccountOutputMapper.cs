using System.Linq;
using Answers.Entities.Models;
using Answers.Entities.Values;
using Answers.UseCases.Common;


namespace Answers.UseCases.Accounts.GetAccount
{
	public class GetAccountOutputMapper : IMapper<Account, GetAccountOutput>
	{
		public GetAccountOutput Map(Account post)
		{
			return new GetAccountOutput
			{
				UserName = post.UserName,
				Points = post.ReceivedVotes.Sum(x => x.Value),
				Questions = post.Posts
					.Where(x => x.PostType == PostType.Question)
					.Select(MapQuestion),
				Answers = post.Posts
					.Where(x => x.PostType == PostType.Answer)
					.Select(MapAnswer)
			};
		}


		private GetAccountOutput.AskedQuestionDto MapQuestion(Post post)
		{
			return new GetAccountOutput.AskedQuestionDto
			{
				Id = post.Question.Id,
				Title = post.Question.CurrentTitle,
				Points = post.Votes.Sum(x => x.Value)
			};
		}


		private GetAccountOutput.AnsweredQuestionDto MapAnswer(Post post)
		{
			return new GetAccountOutput.AnsweredQuestionDto
			{
				Id = post.Question.Id,
				Title = post.Question.CurrentTitle
			};
		}
	}
}
