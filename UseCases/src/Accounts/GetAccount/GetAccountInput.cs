﻿using System;


namespace Answers.UseCases.Accounts.GetAccount
{
	public class GetAccountInput
	{
		public Guid Id { get; set; }
	}
}
