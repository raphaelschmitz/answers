using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.UseCases.Common;
using Microsoft.AspNetCore.Mvc;
using NHibernate;
using NHibernate.Linq;


namespace Answers.UseCases.Accounts.GetAccount
{
	public class GetAccountUseCase
	{
		private readonly ISession _session;
		private readonly IMapper<Account, GetAccountOutput> _mapper;


		public GetAccountUseCase(ISession session, IMapper<Account, GetAccountOutput> mapper)
		{
			_session = session;
			_mapper = mapper;
		}


		public async Task<ActionResult<GetAccountOutput>> Execute(
			GetAccountInput getAccountInput,
			CancellationToken cancellationToken)
		{
			Guid userId = getAccountInput.Id;

			// Fetches seem to not work here, they cause only one item to be returned instead of the whole list.
			// However, this was not reproducible in a unit test (only happens when actually running the app).
			Account account = await _session.Query<Account>()
				.Where(x => x.Id == userId)
				.FirstOrDefaultAsync(cancellationToken);
			if (account == null) return new NotFoundResult();

			return _mapper.Map(account);
		}
	}
}
