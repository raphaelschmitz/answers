using System;
using System.Collections.Generic;


namespace Answers.UseCases.Accounts.GetAccount
{
	public class GetAccountOutput
	{
		public string UserName { get; set; }
		public int Points { get; set; }

		public virtual IEnumerable<AskedQuestionDto> Questions { get; set; }
		public virtual IEnumerable<AnsweredQuestionDto> Answers { get; set; }



		public class AskedQuestionDto
		{
			public Guid Id { get; set; }
			public string Title { get; set; }
			public int Points { get; set; }
		}



		public class AnsweredQuestionDto
		{
			public Guid Id { get; set; }
			public string Title { get; set; }
		}
	}
}
