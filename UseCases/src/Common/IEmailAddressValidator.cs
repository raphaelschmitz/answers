namespace Answers.UseCases.Common
{
    public interface IEmailAddressValidator
    {
        bool Validate(string emailAddress);
    }
}
