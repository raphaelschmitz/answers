using Answers.UseCases.Common.Persistence.Mappings;
using NHibernate.Cfg;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Mapping.ByCode;


namespace Answers.UseCases.Common.Persistence
{
	public static class NHibernateMappingsInstaller
	{
		public static void AddAnswersMappings(this Configuration configuration)
		{
			ModelMapper modelMapper = new ModelMapper();
			modelMapper.AddAnswersMappings();
			HbmMapping hbmMapping = modelMapper.CompileMappingForAllExplicitlyAddedEntities();
			configuration.AddMapping(hbmMapping);
		}


		public static void AddAnswersMappings(this ModelMapper modelMapper)
		{
			modelMapper.AddMapping<WebsiteConfigurationMapping>();

			modelMapper.AddMapping<AccountMapping>();
			modelMapper.AddMapping<EmailConfirmProcessMapping>();

			modelMapper.AddMapping<QuestionMapping>();

			modelMapper.AddMapping<PostMapping>();
			modelMapper.AddMapping<VoteMapping>();
			modelMapper.AddMapping<PostRevisionMapping>();
			modelMapper.AddMapping<CommentMapping>();
			modelMapper.AddMapping<TagMapping>();
		}
	}
}
