using Answers.Entities.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;


namespace Answers.UseCases.Common.Persistence.Mappings
{
	public class AccountMapping : ClassMapping<Account>
	{
		public static readonly string TableName = "Accounts";

		public static readonly string ColumnId = "Id";
		public static readonly string ColumnEmailAddress = "EmailAddress";
		public static readonly string ColumnPasswordHash = "PasswordHash";
		public static readonly string ColumnUserName = "UserName";


		public AccountMapping()
		{
			Table(TableName);


			Id(x => x.Id, m =>
			{
				m.Column(ColumnId);
				m.Generator(Generators.Guid);
			});


			Property(x => x.EmailAddress, m =>
			{
				m.Column(ColumnEmailAddress);
				m.NotNullable(true);
				m.Length(50);
				m.Unique(true);
			});
			Property(x => x.PasswordHash, m =>
			{
				m.Column(ColumnPasswordHash);
				m.NotNullable(true);
				m.Length(108);
			});
			Property(x => x.UserName, m =>
			{
				m.Column(ColumnUserName);
				m.NotNullable(true);
				m.Length(50);
			});


			Set(x => x.Questions, m =>
				{
					m.Key(km => km.Column(QuestionMapping.ColumnCreator));
					m.Inverse(true);
				},
				action => action.OneToMany());

			Set(x => x.Posts, m =>
				{
					m.Key(km => km.Column(PostMapping.ColumnCreator));
					m.Inverse(true);
				},
				action => action.OneToMany());

			Set(x => x.Comments, m =>
				{
					m.Key(km => km.Column(CommentMapping.ColumnUser));
				},
				action => action.OneToMany());

			Set(x => x.ReceivedVotes, m =>
				{
					m.Key(km => km.Column(VoteMapping.ColumnBeneficiary));
					m.Inverse(true);
				},
				action => action.OneToMany());
		}
	}
}
