using Answers.Entities.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;


namespace Answers.UseCases.Common.Persistence.Mappings
{
	public class PostMapping : ClassMapping<Post>
	{
		public static readonly string TableName = "Posts";

		public static readonly string ColumnId = "Id";
		public static readonly string ColumnQuestion = "Question";
		public static readonly string ColumnPostType = "PostType ";
		public static readonly string ColumnCreator = "Account";
		public static readonly string ColumnFirstRevision = "FirstRevision";
		public static readonly string ColumnCurrentRevision = "CurrentRevision";


		public PostMapping()
		{
			Table(TableName);


			Id(x => x.Id, m =>
			{
				m.Column(ColumnId);
				m.Generator(Generators.Guid);
			});


			ManyToOne(x => x.Question, m =>
			{
				m.Column(ColumnQuestion);
			});

			Property(x => x.PostType, m =>
			{
				m.Column(ColumnPostType);
				m.NotNullable(true);
			});

			ManyToOne(x => x.Creator, m =>
			{
				m.Column(ColumnCreator);
			});

			ManyToOne(x => x.FirstRevision, m =>
			{
				m.Column(ColumnFirstRevision);
				m.Unique(true);
			});

			ManyToOne(x => x.CurrentRevision, m =>
			{
				m.Column(ColumnCurrentRevision);
				m.Unique(true);
			});


			Set(x => x.Votes, m =>
				{
					m.Key(km => km.Column(VoteMapping.ColumnPost));
					m.Cascade(Cascade.All | Cascade.DeleteOrphans);
					m.Inverse(true);
				},
				action => action.OneToMany());

			Set(x => x.Revisions, m =>
				{
					m.Key(km => km.Column(PostRevisionMapping.ColumnPost));
					m.Cascade(Cascade.All | Cascade.DeleteOrphans);
					m.Inverse(true);
				},
				action => action.OneToMany());

			Set(x => x.Comments, m =>
				{
					m.Key(km => km.Column(CommentMapping.ColumnPost));
					m.Cascade(Cascade.All | Cascade.DeleteOrphans);
					m.Inverse(true);
				},
				action => action.OneToMany());
		}
	}
}
