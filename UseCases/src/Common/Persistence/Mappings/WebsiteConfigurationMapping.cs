using Answers.Entities.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;


namespace Answers.UseCases.Common.Persistence.Mappings
{
	public class WebsiteConfigurationMapping : ClassMapping<WebsiteConfiguration>
	{
		public static readonly string TableName = "WebsiteConfiguration";

		public static readonly string ColumnId = "Id";
		public static readonly string ColumnEmailSmtpAddress = "EmailSmtpAddress";
		public static readonly string ColumnEmailPort = "EmailPort";
		public static readonly string ColumnEmailUseSsl = "EmailUseSsl";
		public static readonly string ColumnEmailUserName = "EmailUserName";
		public static readonly string ColumnEmailPassword = "EmailPassword";
		public static readonly string ColumnEmailFromAddress = "EmailFromAddress";
		public static readonly string ColumnRegistrationEmailTemplate = "RegistrationEmailTemplate";


		public WebsiteConfigurationMapping()
		{
			Table(TableName);


			Id(x => x.Id, m =>
			{
				m.Column(ColumnId);
				m.Generator(Generators.Guid);
			});


			Property(x => x.EmailSmtpAddress, m =>
			{
				m.Column(ColumnEmailSmtpAddress);
				m.Length(256);
				m.NotNullable(true);
			});

			Property(x => x.EmailPort, m =>
			{
				m.Column(ColumnEmailPort);
				m.NotNullable(true);
			});

			Property(x => x.EmailUseSsl, m =>
			{
				m.Column(ColumnEmailUseSsl);
				m.NotNullable(true);
			});

			Property(x => x.EmailUserName, m =>
			{
				m.Column(ColumnEmailUserName);
				m.Length(256);
				m.NotNullable(true);
			});

			Property(x => x.EmailPassword, m =>
			{
				m.Column(ColumnEmailPassword);
				m.Length(256);
				m.NotNullable(true);
			});

			Property(x => x.EmailFromAddress, m =>
			{
				m.Column(ColumnEmailFromAddress);
				m.Length(256);
				m.NotNullable(true);
			});

			Property(x => x.RegistrationEmailTemplate, m =>
			{
				m.Column(ColumnRegistrationEmailTemplate);
				m.Length(3000);
				m.NotNullable(true);
			});
		}
	}
}
