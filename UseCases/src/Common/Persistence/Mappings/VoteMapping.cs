using Answers.Entities.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;


namespace Answers.UseCases.Common.Persistence.Mappings
{
	public class VoteMapping : ClassMapping<Vote>
	{
		public static readonly string TableName = "Votes";

		public static readonly string ColumnId = "Id";
		public static readonly string ColumnValue = "Value";
		public static readonly string ColumnPost = "Post";
		public static readonly string ColumnVoter = "Account";
		public static readonly string ColumnBeneficiary = "Beneficiary";


		public VoteMapping()
		{
			Table(TableName);


			Id(x => x.Id, m =>
			{
				m.Column(ColumnId);
				m.Generator(Generators.Guid);
			});


			Property(x => x.Value, m =>
			{
				m.Column(ColumnValue);
				m.NotNullable(true);
			});


			ManyToOne(x => x.Post, m =>
			{
				m.Column(ColumnPost);
				m.NotNullable(true);
			});

			ManyToOne(x => x.Voter, m =>
			{
				m.Column(ColumnVoter);
				m.NotNullable(true);
			});

			ManyToOne(x => x.Beneficiary, m =>
			{
				m.Column(ColumnBeneficiary);
				m.NotNullable(true);
			});
		}
	}
}
