using Answers.Entities.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;


namespace Answers.UseCases.Common.Persistence.Mappings
{
	public class QuestionMapping : ClassMapping<Question>
	{
		public static readonly string TableName = "Questions";

		public static readonly string ColumnId = "Id";
		public static readonly string ColumnCreator = "Creator";
		public static readonly string ColumnCurrentTitle = "CurrentTitle";
		public static readonly string ColumnPost = "QuestionPost";

		public static readonly string ManyToManyKey = "Question";
		public static readonly string TableManyToManyTags = "QuestionPosts";


		public QuestionMapping()
		{
			Table(TableName);


			Id(x => x.Id, m =>
			{
				m.Column(ColumnId);
				m.Generator(Generators.Guid);
			});


			ManyToOne(x => x.Creator, m =>
			{
				m.Column(ColumnCreator);
				m.NotNullable(true);
			});

			Property(x => x.CurrentTitle, m =>
			{
				m.Column(ColumnCurrentTitle);
				m.NotNullable(true);
				m.Length(150);
			});

			ManyToOne(x => x.Post, m =>
			{
				m.Column(ColumnPost);
				m.Unique(true);
				m.Cascade(Cascade.All | Cascade.DeleteOrphans);
			});


			Set(x => x.Tags, m =>
				{
					m.Table(TableManyToManyTags);
					m.Key(k => k.Column(ManyToManyKey));
					m.Cascade(Cascade.All);
				},
				map => map.ManyToMany(p => p.Column(TagMapping.ManyToManyKey))
			);

			Set(x => x.Posts, m =>
				{
					m.Key(km => km.Column(PostMapping.ColumnQuestion));
					m.Cascade(Cascade.All | Cascade.DeleteOrphans);
					//m.Inverse(true);
				},
				action => action.OneToMany());
		}
	}
}
