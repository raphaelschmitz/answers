using Answers.Entities.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;


namespace Answers.UseCases.Common.Persistence.Mappings
{
	public class TagMapping : ClassMapping<Tag>
	{
		public static readonly string TableName = "Tags";

		public static readonly string ColumnId = "Id";
		public static readonly string ColumnTitle = "Title";
		public static readonly string ColumnDescription = "Description";

		public static readonly string ManyToManyKey = "Tag";


		public TagMapping()
		{
			Table(TableName);


			Id(x => x.Id, m =>
			{
				m.Column(ColumnId);
				m.Generator(Generators.Guid);
			});


			Property(x => x.Title, m =>
			{
				m.Column(ColumnTitle);
				m.NotNullable(true);
				m.Length(15);
			});
			Property(x => x.Description, m =>
			{
				m.Column(ColumnDescription);
				m.NotNullable(true);
				m.Length(200);
			});


			Set(x => x.Questions, collectionMap =>
				{
					collectionMap.Table(QuestionMapping.TableManyToManyTags);
					collectionMap.Key(k => k.Column(ManyToManyKey));
				},
				map => map.ManyToMany(p => p.Column(QuestionMapping.ManyToManyKey))
			);
		}
	}
}
