using Answers.Entities.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;


namespace Answers.UseCases.Common.Persistence.Mappings
{
	public class PostRevisionMapping : ClassMapping<PostRevision>
	{
		public static readonly string TableName = "PostRevisions";

		public static readonly string ColumnId = "Id";
		public static readonly string ColumnCreationDate = "CreationDate";
		public static readonly string ColumnTitle = "Title";
		public static readonly string ColumnContent = "Content";
		public static readonly string ColumnPost = "Post";
		public static readonly string ColumnUser = "Account";


		public PostRevisionMapping()
		{
			Table(TableName);


			Id(x => x.Id, m =>
			{
				m.Column(ColumnId);
				m.Generator(Generators.Guid);
			});


			Property(x => x.CreationDate, m =>
			{
				m.Column(ColumnCreationDate);
				m.NotNullable(true);
			});

			Property(x => x.Title, m =>
			{
				m.Column(ColumnTitle);
				m.NotNullable(true);
				m.Length(30000);
			});

			Property(x => x.Content, m =>
			{
				m.Column(ColumnContent);
				m.NotNullable(true);
				m.Length(30000);
			});


			ManyToOne(x => x.Post, m =>
			{
				m.Column(ColumnPost);
				m.NotNullable(true);
			});

			ManyToOne(x => x.Account, m =>
			{
				m.Column(ColumnUser);
				m.NotNullable(true);
			});
		}
	}
}
