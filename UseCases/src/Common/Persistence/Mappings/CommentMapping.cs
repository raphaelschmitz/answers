using Answers.Entities.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;


namespace Answers.UseCases.Common.Persistence.Mappings
{
	public class CommentMapping : ClassMapping<Comment>
	{
		public static readonly string TableName = "Comments";

		public static readonly string ColumnId = "Id";
		public static readonly string ColumnCreationDate = "CreationDate";
		public static readonly string ColumnContent = "Content";
		public static readonly string ColumnPost = "Answer";
		public static readonly string ColumnUser = "Account";


		public CommentMapping()
		{
			Table(TableName);


			Id(x => x.Id, m =>
			{
				m.Column(ColumnId);
				m.Generator(Generators.Guid);
			});


			Property(x => x.CreationDate, m =>
			{
				m.Column(ColumnCreationDate);
				m.NotNullable(true);
			});
			Property(x => x.Content, m =>
			{
				m.Column(ColumnContent);
				m.NotNullable(true);
				m.Length(300);
			});


			ManyToOne(x => x.Post, m =>
			{
				m.Column(ColumnPost);
				m.NotNullable(true);
			});

			ManyToOne(x => x.Account, m =>
			{
				m.Column(ColumnUser);
				m.NotNullable(true);
			});
		}
	}
}
