using Answers.Entities.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;


namespace Answers.UseCases.Common.Persistence.Mappings
{
	public class EmailConfirmProcessMapping : ClassMapping<EmailConfirmProcess>
	{
		public static readonly string TableName = "EmailConfirmProcess";

		public static readonly string ColumnId = "Id";
		public static readonly string ColumnAccount = "Account";
		public static readonly string ColumnConfirmKey = "ConfirmKey";


		public EmailConfirmProcessMapping()
		{
			Table(TableName);


			Id(x => x.Id, m =>
			{
				m.Column(ColumnId);
				m.Generator(Generators.Guid);
			});


			Property(x => x.ConfirmKey, m =>
			{
				m.Column(ColumnConfirmKey);
				m.NotNullable(true);
			});


			ManyToOne(x => x.Account, m =>
			{
				m.Column(ColumnAccount);
				m.NotNullable(true);
			});
		}
	}
}
