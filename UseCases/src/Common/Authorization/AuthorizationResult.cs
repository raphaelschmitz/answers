using Answers.Entities.Models;
using Answers.Entities.Values;


namespace Answers.UseCases.Common.Authorization
{
	public class AuthorizationResult
	{
		public AuthorizationResult(Account account)
		{
			Account = account;
		}


		public Account Account { get; }

		public bool IsAuthenticated => Account != null;


		public bool IsAuthorizedTo(Allowable allowable)
		{
			if (Account == null) return false;
			return Account.GetPermissions().Contains(allowable);
		}
	}
}
