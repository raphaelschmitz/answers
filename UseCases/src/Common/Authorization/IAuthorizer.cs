using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;


namespace Answers.UseCases.Common.Authorization
{
	public interface IAuthorizer
	{
		string CreateToken(Account account);
		Task<AuthorizationResult> Check(string token, CancellationToken cancellationToken);
	}
}
