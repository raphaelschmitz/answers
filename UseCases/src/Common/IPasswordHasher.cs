namespace Answers.UseCases.Common
{
	public interface IPasswordHasher
	{
		string CreateSaltedHash(string password);
		bool CheckIfPasswordMatches(string input, string saltedHash);
	}
}
