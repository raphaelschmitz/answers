namespace Answers.UseCases.Common.Validators
{
	public interface IValidator<in T>
	{
		ValidationResult Validate(T input);
	}
}
