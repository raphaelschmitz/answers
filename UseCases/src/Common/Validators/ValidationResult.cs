using System.Collections.Generic;
using System.Linq;


namespace Answers.UseCases.Common.Validators
{
	public class ValidationResult
	{
		public ValidationResult()
		{
			Errors = new List<string>();
		}


		public ValidationResult(IEnumerable<string> errors)
		{
			Errors = new List<string>(errors);
		}


		public IList<string> Errors { get; }

		public bool HasErrors => Errors.Any();
	}
}
