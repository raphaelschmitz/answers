namespace Answers.UseCases.Common
{
	public interface IMapper<TInput, TOutput>
	{
		TOutput Map(TInput input);
	}
}
