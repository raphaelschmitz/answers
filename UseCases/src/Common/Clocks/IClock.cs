using System;


namespace Answers.UseCases.Common.Clocks
{
	public interface IClock
	{
		DateTime GetNow();
	}
}
