namespace Answers.UseCases.Questions.SearchQuestions
{
	public class SearchQuestionsInput
	{
		public string SearchString { get; set; }


		public override string ToString()
		{
			return $"{GetType().Name}\n" +
			       $"{nameof(SearchString)}: {SearchString}\n";
		}
	}
}
