using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.UseCases.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NHibernate;
using NHibernate.Linq;


namespace Answers.UseCases.Questions.SearchQuestions
{
	public class SearchQuestionsUseCase
	{
		private readonly ISession _session;
		private readonly IMapper<IList<Question>, SearchQuestionsOutput> _mapper;
		private readonly ILogger<string> _logger;


		public SearchQuestionsUseCase(ISession session, IMapper<IList<Question>, SearchQuestionsOutput> mapper
			, ILogger<string> logger
		)
		{
			_session = session;
			_mapper = mapper;
			_logger = logger;
		}


		public async Task<ActionResult<SearchQuestionsOutput>> Execute(
			SearchQuestionsInput input,
			CancellationToken cancellationToken)
		{
			LogLevel logLevel = LogLevel.Warning;
			string inputSearchString = input.SearchString;
			_logger.Log(logLevel, inputSearchString);
			List<Question> questions = await _session.Query<Question>()
				.Where(x => x.CurrentTitle.Contains(inputSearchString))
				.ToListAsync(cancellationToken);

			return _mapper.Map(questions);
		}
	}
}
