using System;
using System.Collections.Generic;


namespace Answers.UseCases.Questions.SearchQuestions
{
	public class SearchQuestionsOutput
	{
		public IEnumerable<QuestionDto> Questions { get; set; }



		public class QuestionDto
		{
			public Guid Id { get; set; }

			public int Points { get; set; }
			public string Title { get; set; }
			public string Gist { get; set; }
			public DateTime CreationDate { get; set; }
			public UserDto User { get; set; }
		}



		public class UserDto
		{
			public Guid Id { get; set; }

			public string Email { get; set; }
			public string UserName { get; set; }
		}
	}
}
