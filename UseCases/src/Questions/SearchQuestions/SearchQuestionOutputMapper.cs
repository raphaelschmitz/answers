using System.Collections.Generic;
using System.Linq;
using Answers.Entities.Models;
using Answers.UseCases.Common;


namespace Answers.UseCases.Questions.SearchQuestions
{
	public class SearchQuestionOutputMapper : IMapper<IList<Question>, SearchQuestionsOutput>
	{
		public SearchQuestionsOutput Map(IList<Question> post)
		{
			return new SearchQuestionsOutput { Questions = post.Select(Map) };
		}


		private SearchQuestionsOutput.QuestionDto Map(Question question)
		{
			return new SearchQuestionsOutput.QuestionDto
			{
				Id = question.Id,
				Points = question.Post.Votes.Sum(x => x.Value),
				Title = question.CurrentTitle,
				Gist = question.Post.CurrentRevision.Content,
				CreationDate = question.Post.CurrentRevision.CreationDate,
				User = Map(question.Post.Revisions.OrderBy(x => x.CreationDate).First().Account)
			};
		}


		private SearchQuestionsOutput.UserDto Map(Account account)
		{
			return new SearchQuestionsOutput.UserDto
			{
				Id = account.Id,
				Email = account.EmailAddress,
				UserName = account.UserName,
			};
		}
	}
}
