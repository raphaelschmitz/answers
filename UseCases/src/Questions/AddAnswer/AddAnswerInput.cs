using System;


namespace Answers.UseCases.Questions.AddAnswer
{
	public class AddAnswerInput
	{
		public string JsonWebToken { get; set; }
		public Guid QuestionId { get; set; }
		public string Content { get; set; }


		public override string ToString()
		{
			return $"{GetType().Name}\n" +
			       $"{nameof(JsonWebToken)}: {JsonWebToken}\n" +
			       $"{nameof(QuestionId)}: {QuestionId}\n" +
			       $"{nameof(Content)}: {Content}\n";
		}
	}
}
