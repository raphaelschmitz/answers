using System;


namespace Answers.UseCases.Questions.AddAnswer
{
	public class AddAnswerOutput
	{
		public Guid Id { get; set; }
	}
}
