using System;
using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.Entities.Values;
using Answers.UseCases.Common.Authorization;
using Answers.UseCases.Common.Clocks;
using Microsoft.AspNetCore.Mvc;
using NHibernate;


namespace Answers.UseCases.Questions.AddAnswer
{
	public class AddAnswerUseCase
	{
		private readonly ISession _session;
		private readonly IAuthorizer _authorizer;
		private readonly IClock _clock;


		public AddAnswerUseCase(
			ISession session,
			IAuthorizer authorizer,
			IClock clock)
		{
			_session = session;
			_authorizer = authorizer;
			_clock = clock;
		}


		public async Task<ActionResult<AddAnswerOutput>> Execute(
			AddAnswerInput input,
			CancellationToken cancellationToken)
		{
			using (ITransaction transaction = _session.BeginTransaction())
			{
				string jsonWebToken = input.JsonWebToken;
				AuthorizationResult authorizationResult = await _authorizer.Check(jsonWebToken, cancellationToken);
				if (!authorizationResult.IsAuthorizedTo(Allowable.AddAnswers)) return new UnauthorizedResult();


				Guid questionId = input.QuestionId;
				Question question = await _session.GetAsync<Question>(questionId, cancellationToken);
				if (question == null) return new BadRequestResult();

				Account account = authorizationResult.Account;
				DateTime creationDate = _clock.GetNow();
				string content = input.Content;
				Post post = question.AddAnswer(account, creationDate, content);
				await _session.SaveAsync(post, cancellationToken);
				await transaction.CommitAsync(cancellationToken);
				return new AddAnswerOutput {Id = post.Id};
			}
		}
	}
}
