using System;


namespace Answers.UseCases.Questions.AddQuestion
{
	public class AddQuestionOutput
	{
        public Guid Id { get; set; }
    }
}
