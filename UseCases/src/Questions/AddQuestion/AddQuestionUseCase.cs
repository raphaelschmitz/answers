using System;
using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.Entities.Values;
using Answers.UseCases.Common.Authorization;
using Answers.UseCases.Common.Clocks;
using Answers.UseCases.Common.Validators;
using Microsoft.AspNetCore.Mvc;
using NHibernate;
using NHibernate.Linq;


namespace Answers.UseCases.Questions.AddQuestion
{
	public class AddQuestionUseCase
	{
		private readonly ISession _session;
		private readonly IValidator<AddQuestionInput> _inputValidator;
		private readonly IAuthorizer _authorizer;
		private readonly IClock _clock;


		public AddQuestionUseCase(
			ISession session,
			IValidator<AddQuestionInput> inputValidator,
			IAuthorizer authorizer,
			IClock clock)
		{
			_session = session;
			_inputValidator = inputValidator;
			_authorizer = authorizer;
			_clock = clock;
		}


		public async Task<ActionResult<AddQuestionOutput>> Execute(
			AddQuestionInput input,
			CancellationToken cancellationToken)
		{
			using (ITransaction transaction = _session.BeginTransaction())
			{
				ValidationResult validationResult = _inputValidator.Validate(input);
				if (validationResult.HasErrors) return new BadRequestObjectResult(validationResult);

				string jsonWebToken = input.JsonWebToken;
				AuthorizationResult authorizationResult = await _authorizer.Check(jsonWebToken, cancellationToken);
				if (!authorizationResult.IsAuthorizedTo(Allowable.AddQuestions)) return new UnauthorizedResult();

				Account account = authorizationResult.Account;
				DateTime dateTime = _clock.GetNow();
				string title = input.Title;
				string content = input.Content;
				Question question = Question.Create(account, dateTime, title, content);
				foreach (string tagTitle in input.Tags)
				{
					Tag tag = await _session.Query<Tag>()
						.FirstOrDefaultAsync(x => x.Title == tagTitle, cancellationToken);
					if (tag == null)
					{
						tag = Tag.Create(tagTitle, "");
						await _session.SaveAsync(tag, cancellationToken);
					}
					question.Tags.Add(tag);
				}

				await _session.SaveAsync(question, cancellationToken);
				await transaction.CommitAsync(cancellationToken);

				return new AddQuestionOutput { Id = question.Id };
			}
		}
	}
}
