using Answers.UseCases.Common.Validators;


namespace Answers.UseCases.Questions.AddQuestion
{
	public class AddQuestionInputValidator : IValidator<AddQuestionInput>
	{
		ValidationResult IValidator<AddQuestionInput>.Validate(AddQuestionInput input)
		{
			ValidationResult validationResult = new ValidationResult();
			if (input.Title.Length < 10) validationResult.Errors.Add("Title not long enough");
			if (input.Content.Length < 10) validationResult.Errors.Add("Content not long enough");
			return validationResult;
		}
	}
}
