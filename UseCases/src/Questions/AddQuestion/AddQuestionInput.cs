using System.Collections.Generic;


namespace Answers.UseCases.Questions.AddQuestion
{
	public class AddQuestionInput
	{
		public AddQuestionInput()
		{
			Tags = new List<string>();
		}


		public string JsonWebToken { get; set; }
		public string Title { get; set; }
		public string Content { get; set; }
		public IList<string> Tags { get; set; }


		public override string ToString()
		{
			return $"{GetType().Name}\n" +
				   $"{nameof(JsonWebToken)}: {JsonWebToken}\n" +
				   $"{nameof(Title)}: {Title}\n" +
				   $"{nameof(Tags)}: {Tags}\n" +
				   $"{nameof(Content)}: {Content}\n";
		}
	}
}
