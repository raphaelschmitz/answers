using System;


namespace Answers.UseCases.Questions.UpdateQuestion
{
	public class UpdateQuestionOutput
	{
		public Guid Id { get; set; }
	}
}
