using System;
using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.Entities.Values;
using Answers.UseCases.Common.Authorization;
using Answers.UseCases.Common.Clocks;
using Microsoft.AspNetCore.Mvc;
using NHibernate;


namespace Answers.UseCases.Questions.UpdateQuestion
{
	public class UpdateQuestionUseCase
	{
		private readonly ISession _session;
		private readonly IAuthorizer _authorizer;
		private readonly IClock _clock;


		public UpdateQuestionUseCase(ISession session, IAuthorizer authorizer, IClock clock)
		{
			_session = session;
			_authorizer = authorizer;
			_clock = clock;
		}


		public async Task<ActionResult<UpdateQuestionOutput>> Execute(
			UpdateQuestionInput input,
			CancellationToken cancellationToken)
		{
			using (ITransaction transaction = _session.BeginTransaction())
			{
				string jsonWebToken = input.JsonWebToken;
				AuthorizationResult authorizationResult = await _authorizer.Check(jsonWebToken, cancellationToken);
				if (!authorizationResult.IsAuthorizedTo(Allowable.UpdateQuestions)) return new UnauthorizedResult();

				Question question = await _session.GetAsync<Question>(input.QuestionId, cancellationToken);
				if (question == null) return new BadRequestResult();

				Account account = authorizationResult.Account;
				DateTime creationDate = _clock.GetNow();
				string title = input.Title;
				string content = input.Content;
				PostRevision questionRevision = PostRevision.Create(question.Post, account, creationDate, title, content);

				await _session.SaveAsync(question, cancellationToken);
				await transaction.CommitAsync(cancellationToken);
				return new UpdateQuestionOutput { Id = questionRevision.Id };
			}
		}
	}
}
