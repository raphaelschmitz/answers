using System;


namespace Answers.UseCases.Questions.UpdateQuestion
{
	public class UpdateQuestionInput
	{
		public string JsonWebToken { get; set; }
		public Guid QuestionId { get; set; }
		public string Title { get; set; }
		public string Content { get; set; }


		public override string ToString()
		{
			return $"{GetType().Name}\n" +
			       $"{nameof(JsonWebToken)}: {JsonWebToken}\n" +
			       $"{nameof(QuestionId)}: {QuestionId}\n" +
			       $"{nameof(Title)}: {Title}\n" +
			       $"{nameof(Content)}: {Content}\n";
		}
	}
}
