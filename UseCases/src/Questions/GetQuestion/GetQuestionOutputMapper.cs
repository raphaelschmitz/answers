using System.Linq;
using Answers.Entities.Models;
using Answers.Entities.Values;


namespace Answers.UseCases.Questions.GetQuestion
{
	public class GetQuestionOutputMapper : IGetQuestionOutputMapper
	{
		public GetQuestionOutput Map(Question post, Account ownAccount)
		{
			return new GetQuestionOutput
			{
				Id = post.Id,
				Title = post.CurrentTitle,
				Post = Map(post.Post, ownAccount),
				Tags = post.Tags.Select(Map),
				Answers = post.Posts.Where(x => x.PostType == PostType.Answer).Select(x => Map(x, ownAccount))
			};
		}


		private GetQuestionOutput.TagDto Map(Tag tag)
		{
			return new GetQuestionOutput.TagDto
			{
				Id = tag.Id,
				Title = tag.Title,
				Description = tag.Description
			};
		}


		private GetQuestionOutput.PostDto Map(Post post, Account ownAccount)
		{
			Vote vote = post.Votes.FirstOrDefault(x => x.Voter == ownAccount);

			return new GetQuestionOutput.PostDto
			{
				Id = post.Id,
				Points = post.Votes.Sum(x => x.Value),
				OwnVote = vote?.Value ?? 0,
				Content = post.CurrentRevision.Content,
				CreationDate = post.FirstRevision.CreationDate,
				Creator = Map(post.FirstRevision.Account),
				LastChange = post.CurrentRevision.CreationDate,
				LastChangeAuthor = Map(post.CurrentRevision.Account),
				Comments = post.Comments.Select(Map)
			};
		}


		private GetQuestionOutput.CommentDto Map(Comment comment)
		{
			return new GetQuestionOutput.CommentDto
			{
				Content = comment.Content,
				CreationDate = comment.CreationDate,
				User = Map(comment.Account)
			};
		}


		private GetQuestionOutput.UserDto Map(Account account)
		{
			return new GetQuestionOutput.UserDto
			{
				Id = account.Id,
				UserName = account.UserName,
				Points = account.ReceivedVotes.Sum(x => x.Value)
			};
		}
	}
}
