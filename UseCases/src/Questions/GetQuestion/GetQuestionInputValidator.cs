using Answers.UseCases.Common.Validators;


namespace Answers.UseCases.Questions.GetQuestion
{
	public class GetQuestionInputValidator : IValidator<GetQuestionInput>
	{
		public ValidationResult Validate(GetQuestionInput input)
		{
			ValidationResult validationResult = new ValidationResult();
			if (input.GistLength < 4) validationResult.Errors.Add("Gist not long enough");
			return validationResult;
		}
	}
}
