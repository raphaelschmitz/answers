using System;
using System.Threading;
using System.Threading.Tasks;
using Answers.Entities.Models;
using Answers.UseCases.Common.Authorization;
using Answers.UseCases.Common.Validators;
using Microsoft.AspNetCore.Mvc;
using NHibernate;


namespace Answers.UseCases.Questions.GetQuestion
{
	public class GetQuestionUseCase
	{
		private readonly ISession _session;
		private readonly IGetQuestionOutputMapper _mapper;
		private readonly IValidator<GetQuestionInput> _validator;
		private readonly IGistCreator _gistCreator;
		private readonly IAuthorizer _authorizer;


		public GetQuestionUseCase(
			ISession session,
			IGetQuestionOutputMapper mapper,
			IValidator<GetQuestionInput> validator,
			IGistCreator gistCreator, IAuthorizer authorizer)
		{
			_session = session;
			_gistCreator = gistCreator;
			_authorizer = authorizer;
			_mapper = mapper;
			_validator = validator;
		}


		public async Task<ActionResult<GetQuestionOutput>> Execute(
			GetQuestionInput input,
			CancellationToken cancellationToken)
		{
			Guid id = input.Id;
			Question question = await _session.GetAsync<Question>(id, cancellationToken);
			if (question == null) return new NotFoundResult();

			ValidationResult validationResult = _validator.Validate(input);
			if (validationResult.HasErrors) return new BadRequestObjectResult(validationResult);

			PostRevision currentRevision = question.Post.CurrentRevision;
			string currentContent = currentRevision.Content;
			int gistLength = input.GistLength;
			currentRevision.Content = _gistCreator.CreateGist(currentContent, gistLength);

			AuthorizationResult authorizationResult = await _authorizer.Check(input.JsonWebToken, cancellationToken);
			Account account = authorizationResult.Account;

			return _mapper.Map(question, account);
		}
	}
}
