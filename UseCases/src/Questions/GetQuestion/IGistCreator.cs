namespace Answers.UseCases.Questions.GetQuestion
{
	public interface IGistCreator
	{
		string CreateGist(string fullText, int maxLength);
	}
}
