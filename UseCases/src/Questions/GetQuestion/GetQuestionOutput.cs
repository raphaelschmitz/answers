using System;
using System.Collections.Generic;


namespace Answers.UseCases.Questions.GetQuestion
{
	public class GetQuestionOutput
	{
		public Guid Id { get; set; }

		public string Title { get; set; }
		public PostDto Post { get; set; }
		public IEnumerable<TagDto> Tags { get; set; }
		public IEnumerable<PostDto> Answers { get; set; }



		public class TagDto
		{
			public Guid Id { get; set; }

			public string Title { get; set; }
			public string Description { get; set; }
		}



		public class PostDto
		{
			public Guid Id { get; set; }

			public int Points { get; set; }
			public int OwnVote { get; set; }
			
			public string Content { get; set; }
			
			public DateTime CreationDate { get; set; }
			public UserDto Creator { get; set; }
			public DateTime LastChange { get; set; }
			public UserDto LastChangeAuthor { get; set; }
			
			public IEnumerable<CommentDto> Comments { get; set; }
		}



		public class UserDto
		{
			public Guid Id { get; set; }

			public string UserName { get; set; }
			public int Points { get; set; }
		}



		public class CommentDto
		{
			public string Content { get; set; }
			public DateTime CreationDate { get; set; }
			public UserDto User { get; set; }
		}
	}
}
