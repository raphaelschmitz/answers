using Answers.Entities.Models;


namespace Answers.UseCases.Questions.GetQuestion
{
	public interface IGetQuestionOutputMapper
	{
		GetQuestionOutput Map(Question post, Account account);
	}
}
