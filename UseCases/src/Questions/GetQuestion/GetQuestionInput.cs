using System;


namespace Answers.UseCases.Questions.GetQuestion
{
	public class GetQuestionInput
	{
		public string JsonWebToken { get; set; }
		public Guid Id { get; set; }
		public int GistLength { get; set; }


		public override string ToString()
		{
			return $"{GetType().Name}\n" +
			       $"{nameof(JsonWebToken)}: {JsonWebToken}\n" +
			       $"{nameof(Id)}: {Id}\n"+
			       $"{nameof(GistLength)}: {GistLength}\n";
		}
	}
}
