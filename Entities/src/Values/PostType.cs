namespace Answers.Entities.Values
{
	public enum PostType
	{
		Question = 1,
		Answer = 2
	}
}
