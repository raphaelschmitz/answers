using System.Collections.Generic;
using Answers.Entities.Models;


namespace Answers.Entities.Values
{
	public static class UserExtensions
	{
		public static IList<Allowable> GetPermissions(this Account account)
		{
			return Allowable.All;
		}
	}
}
