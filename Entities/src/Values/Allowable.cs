using System;
using System.Collections.Generic;
using System.Security.Claims;


namespace Answers.Entities.Values
{
	public class Allowable
	{
		public static readonly Allowable AddAnswers = new Allowable(1, "AddAnswers");
		public static readonly Allowable AddAnswerComments = new Allowable(2, "AddAnswerComments");
		public static readonly Allowable UpdateAnswers = new Allowable(3, "UpdateAnswers");
		public static readonly Allowable DeleteAnswers = new Allowable(4, "DeleteAnswers");
		public static readonly Allowable AddQuestions = new Allowable(5, "AddQuestions");
		public static readonly Allowable AddQuestionComments = new Allowable(6, "AddQuestionComments");
		public static readonly Allowable UpdateQuestions = new Allowable(7, "UpdateQuestions");
		public static readonly Allowable DeleteQuestions = new Allowable(8, "DeleteQuestions");
		public static readonly Allowable DeleteUsers = new Allowable(9, "DeleteUsers");
		public static readonly Allowable Vote = new Allowable(10, "Vote");
		public static readonly Allowable AccessAdminArea = new Allowable(11, "AccessAdminArea");


		public static readonly IList<Allowable> All = new List<Allowable>
		{
			AddAnswers,
			AddAnswerComments,
			UpdateAnswers,
			DeleteAnswers,
			AddQuestions,
			AddQuestionComments,
			UpdateQuestions,
			DeleteUsers,
			Vote,
			AccessAdminArea
		};


		private Allowable(int code, string claimId)
		{
			Code = code;
			ClaimId = claimId;
		}


		public int Code { get; }
		public string ClaimId { get; }


		public static Allowable Get(int code)
		{
			foreach (Allowable allowable in All)
			{
				if (allowable.Code == code) return allowable;
			}
			throw new ArgumentOutOfRangeException($"{code} does not represent an Allowable");
		}


		public Claim ToClaim()
		{
			return new Claim(ClaimId, "");
		}
	}
}
