using System;
using System.Collections.Generic;


namespace Answers.Entities.Models
{
	public class Account
	{
		private string _userName;
		private string _passwordHash;
		private string _emailAddress;


		protected Account()
		{
			Questions = new HashSet<Question>();
			Posts = new HashSet<Post>();
			Comments = new HashSet<Comment>();
			ReceivedVotes = new HashSet<Vote>();
		}


		public virtual Guid Id { get; set; }


		public virtual string EmailAddress
		{
			get => _emailAddress;
			set => _emailAddress = value ?? throw new ArgumentNullException(nameof(EmailAddress));
		}

		public virtual string PasswordHash
		{
			get => _passwordHash;
			set => _passwordHash = value ?? throw new ArgumentNullException(nameof(PasswordHash));
		}

		public virtual string UserName
		{
			get => _userName;
			set => _userName = value ?? throw new ArgumentNullException(nameof(UserName));
		}


		public virtual ISet<Question> Questions { get; set; }
		public virtual ISet<Post> Posts { get; set; }
		public virtual ISet<Comment> Comments { get; set; }
		public virtual ISet<Vote> ReceivedVotes { get; set; }


		public static Account Create(string emailAddress, string passwordHash, string userName)
		{
			return new Account
			{
				EmailAddress = emailAddress,
				PasswordHash = passwordHash,
				UserName = userName
			};
		}
	}
}
