using System;
using System.Collections.Generic;


namespace Answers.Entities.Models
{
	public class Tag
	{
		private string _title;
		private string _description;


		protected Tag()
		{
			Questions = new HashSet<Question>();
		}


		public virtual Guid Id { get; set; }


		public virtual string Title
		{
			get => _title;
			set => _title = value ?? throw new ArgumentNullException(nameof(Title));
		}

		public virtual string Description
		{
			get => _description;
			set => _description = value ?? throw new ArgumentNullException(nameof(Description));
		}


		public virtual ISet<Question> Questions { get; set; }


		public static Tag Create(string title, string description)
		{
			return new Tag
			{
				Title = title,
				Description = description
			};
		}
	}
}
