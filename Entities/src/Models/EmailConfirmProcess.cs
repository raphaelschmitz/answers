using System;


namespace Answers.Entities.Models
{
	public class EmailConfirmProcess
	{
		private Account _account;
		private string _confirmKey;


		protected EmailConfirmProcess()
		{
		}


		public virtual Guid Id { get; set; }

		public virtual Account Account
		{
			get => _account;
			set => _account = value ?? throw new ArgumentNullException(nameof(Account));
		}

		public virtual string ConfirmKey
		{
			get => _confirmKey;
			set => _confirmKey = value ?? throw new ArgumentNullException(nameof(ConfirmKey));
		}

		public virtual DateTime CreationDate { get; set; }


		public static EmailConfirmProcess Create(Account account, string confirmKey, DateTime creationDate)
		{
			return new EmailConfirmProcess
			{
				Account = account,
				ConfirmKey = confirmKey,
				CreationDate = creationDate
			};
		}
	}
}
