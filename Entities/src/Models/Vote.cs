using System;


namespace Answers.Entities.Models
{
	public class Vote
	{
		public static readonly int Up = 1;
		public static readonly int Down = -1;


		private int _value;
		private Post _post;
		private Account _voter;
		private Account _beneficiary;


		protected Vote()
		{

		}


		public virtual Guid Id { get; set; }

		public virtual Post Post
		{
			get => _post;
			set => _post = value?? throw new ArgumentNullException(nameof(Post));
		}

		public virtual Account Voter
		{
			get => _voter;
			set => _voter = value?? throw new ArgumentNullException(nameof(Voter));
		}

		public virtual Account Beneficiary
		{
			get => _beneficiary;
			set => _beneficiary = value?? throw new ArgumentNullException(nameof(Beneficiary));
		}

		public virtual int Value
		{
			get => _value;
			set => _value = value.CompareTo(0);
		}


		public static Vote Create(Post post, Account voter, int value)
		{
			Vote vote = new Vote
			{
				Post = post,
				Voter = voter,
				Beneficiary = post.Creator,
				Value = value
			};
			vote.Post.Votes.Add(vote);
			vote.Beneficiary.ReceivedVotes.Add(vote);
			return vote;
		}
	}
}
