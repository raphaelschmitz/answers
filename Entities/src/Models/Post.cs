using System;
using System.Collections.Generic;
using Answers.Entities.Values;


namespace Answers.Entities.Models
{
	public class Post
	{
		private Question _question;
		private Account _creator;
		private PostRevision _firstRevision;
		private PostRevision _currentRevision;


		protected Post()
		{
			Votes = new HashSet<Vote>();
			Revisions = new HashSet<PostRevision>();
			Comments = new HashSet<Comment>();
		}


		public virtual Guid Id { get; set; }

		public virtual Question Question
		{
			get => _question;
			set => _question = value ?? throw new ArgumentNullException(nameof(Question));
		}

		public virtual PostType PostType { get; set; }

		public virtual Account Creator
		{
			get => _creator;
			set => _creator = value ?? throw new ArgumentNullException(nameof(Creator));
		}

		public virtual PostRevision FirstRevision
		{
			get => _firstRevision;
			set => _firstRevision = value ?? throw new ArgumentNullException(nameof(FirstRevision));
		}

		public virtual PostRevision CurrentRevision
		{
			get => _currentRevision;
			set => _currentRevision = value ?? throw new ArgumentNullException(nameof(CurrentRevision));
		}

		public virtual ISet<Vote> Votes { get; set; }
		public virtual ISet<PostRevision> Revisions { get; set; }
		public virtual ISet<Comment> Comments { get; set; }


		public static Post Create(
			Question question,
			PostType postType,
			Account account,
			DateTime creationDate,
			string title,
			string content)
		{
			Post post = new Post
			{
				Question = question,
				PostType = postType,
				Creator = account,
			};
			question.Posts.Add(post);
			account.Posts.Add(post);

			post.FirstRevision = PostRevision.Create(post, account, creationDate, title, content);
			return post;
		}
	}
}
