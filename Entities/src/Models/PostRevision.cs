using System;


namespace Answers.Entities.Models
{
	public class PostRevision
	{
		private Account _account;
		private Post _post;
		private string _content;
		private string _title;


		protected PostRevision()
		{
		}


		public virtual Guid Id { get; set; }

		public virtual Post Post
		{
			get => _post;
			set => _post = value ?? throw new ArgumentNullException(nameof(Post));
		}

		public virtual string Title
		{
			get => _title;
			set => _title = value ?? throw new ArgumentNullException(nameof(Title));
		}

		public virtual string Content
		{
			get => _content;
			set => _content = value ?? throw new ArgumentNullException(nameof(Content));
		}


		public virtual Account Account
		{
			get => _account;
			set => _account = value ?? throw new ArgumentNullException(nameof(Account));
		}

		public virtual DateTime CreationDate { get; set; }


		public static PostRevision Create(
			Post post,
			Account account,
			DateTime creationDate,
			string title,
			string content)
		{
			PostRevision postRevision = new PostRevision
			{
				Post = post,
				Account = account,
				CreationDate = creationDate,
				Title = title,
				Content = content
			};
			post.Revisions.Add(postRevision);
			post.CurrentRevision = postRevision;
			return postRevision;
		}
	}
}
