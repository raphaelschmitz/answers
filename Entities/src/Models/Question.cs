using System;
using System.Collections.Generic;
using Answers.Entities.Values;


namespace Answers.Entities.Models
{
	public class Question
	{
		private Account _creator;
		private string _currentTitle;
    	private Post _post;
	

		protected Question()
		{
			Tags = new HashSet<Tag>();
			Posts = new HashSet<Post>();
		}


		public virtual Guid Id { get; set; }

		public virtual Account Creator
		{
			get => _creator;
			set => _creator = value ?? throw new ArgumentNullException(nameof(Creator));
		}

		public virtual string CurrentTitle
		{
			get => _currentTitle;
			set => _currentTitle = value ?? throw new ArgumentNullException(nameof(CurrentTitle));
		}

		public virtual Post Post
		{
			get => _post;
			set => _post = value ?? throw new ArgumentNullException(nameof(Post));
		}

		public virtual ISet<Tag> Tags { get; set; }
		public virtual ISet<Post> Posts { get; set; }


		public static Question Create(Account account, DateTime creationDate, string title, string content)
		{
			Question question = new Question
			{
				Creator = account,
				CurrentTitle = title
			};
			question.Post  = Post.Create(question, PostType.Question, account, creationDate, title, content);
			account.Questions.Add(question);
			return question;
		}


		public virtual Post AddAnswer(Account account, DateTime creationDate, string content)
		{
			Post post = Post.Create(this, PostType.Answer, account, creationDate, "", content);
			Posts.Add(post);
			return post;
		}
	}
}
