using System;


namespace Answers.Entities.Models
{
	public class WebsiteConfiguration
	{
		private string _emailFromAddress;
		private string _registrationEmailTemplate;


		protected WebsiteConfiguration()
		{
		}


		public virtual Guid Id { get; set; }

		public virtual string EmailSmtpAddress { get; set; }

		public virtual int EmailPort { get; set; }

		public virtual bool EmailUseSsl { get; set; }

		public virtual string EmailUserName { get; set; }

		public virtual string EmailPassword { get; set; }

		public virtual string EmailFromAddress
		{
			get => _emailFromAddress;
			set => _emailFromAddress = value ?? throw new ArgumentNullException(nameof(EmailFromAddress));
		}

		public virtual string RegistrationEmailTemplate
		{
			get => _registrationEmailTemplate;
			set => _registrationEmailTemplate =
				value ?? throw new ArgumentNullException(nameof(RegistrationEmailTemplate));
		}


		public static WebsiteConfiguration Create(
			string emailSmtpAddress,
			int emailPort,
			bool emailUseSsl,
			string emailUserName,
			string emailPassword,
			string emailFromAddress,
			string registrationEmailTemplate)
		{
			return new WebsiteConfiguration
			{
				EmailSmtpAddress = emailSmtpAddress,
				EmailPort = emailPort,
				EmailUseSsl = emailUseSsl,
				EmailUserName = emailUserName,
				EmailPassword = emailPassword,
				EmailFromAddress = emailFromAddress,
				RegistrationEmailTemplate = registrationEmailTemplate
			};
		}
	}
}
