using System;


namespace Answers.Entities.Models
{
	public class Comment
	{
		private Account _account;
		private Post _post;
		private string _content;


		protected Comment()
		{
		}


		public virtual Guid Id { get; set; }

		public virtual Account Account
		{
			get => _account;
			set => _account = value ?? throw new ArgumentNullException(nameof(Account));
		}

		public virtual Post Post
		{
			get => _post;
			set => _post = value ?? throw new ArgumentNullException(nameof(Post));
		}

		public virtual DateTime CreationDate { get; set; }

		public virtual string Content
		{
			get => _content;
			set => _content = value ?? throw new ArgumentNullException(nameof(Content));
		}


		public static Comment Create(Post post, Account account, DateTime creationDate, string content)
		{
			Comment comment = new Comment
			{
				Post = post,
				Account = account,
				CreationDate = creationDate,
				Content = content
			};
			post.Comments.Add(comment);
			return comment;
		}
	}
}
