using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;


namespace EntitiesIntegrationTests.TestHelpers
{
	public static class InMemoryDbConfigurationFactory
	{
		public static void ConfigureInMemorySqLiteConnection(this Configuration configuration)
		{
			configuration.DataBaseIntegration(db =>
			{
				db.ConnectionReleaseMode = ConnectionReleaseMode.OnClose;
				db.Dialect<SQLiteDialect>();
				db.Driver<SQLite20Driver>();
				db.ConnectionString = "data source=:memory:";
			});
		}
	}
}
