using System;
using System.Threading.Tasks;
using Answers.Entities.Models;
using NHibernate;


namespace EntitiesIntegrationTests.TestHelpers
{
    public static class TestQuestion
    {
        public static async Task<Question> Create(ISession session, Account account)
        {
            DateTime creationDate = DateTime.Now;
            Question question = Question.Create(account, creationDate, "", "");
            await session.SaveAsync(question);

            return question;
        }
    }
}
