using System.Threading.Tasks;
using Answers.Entities.Models;
using NHibernate;


namespace EntitiesIntegrationTests.TestHelpers
{
	public class TestUser
	{
		public static async Task<Account> Create(ISession session)
		{
			Account account = Account.Create("john@email.com", "acxr", "Johnster123");
			using (ITransaction transaction = session.BeginTransaction())
			{
				await session.SaveAsync(account);
				await transaction.CommitAsync();
			}
			return account;
		}
	}
}
