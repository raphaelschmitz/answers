using System;
using Answers.Entities;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;


namespace EntitiesIntegrationTests.TestHelpers
{
	public class InMemoryDatabaseTest : IDisposable
	{
		protected readonly ISession Session;


		public InMemoryDatabaseTest()
		{
			Configuration configuration = new Configuration();
			configuration.ConfigureInMemorySqLiteConnection();
			configuration.AddAnswersMappings();

			ISessionFactory sessionFactory = configuration.BuildSessionFactory();

			Session = sessionFactory.OpenSession();

			SchemaExport schemaExport = new SchemaExport(configuration);
			schemaExport.Execute(true, true, false, Session.Connection, Console.Out);
		}


		public void Dispose()
		{
			Session.Dispose();
		}
	}
}
