using Answers.InterfaceAdapters;
using Answers.InterfaceAdapters.Accounts.Authorizers;
using Answers.InterfaceAdapters.Accounts.Emails;
using Answers.InterfaceAdapters.Accounts.JsonWebTokens;
using Answers.InterfaceAdapters.Accounts.PasswordHashing;
using Answers.InterfaceAdapters.Common;
using Answers.InterfaceAdapters.Common.Emails;
using Answers.InterfaceAdapters.Posts;
using Answers.UseCases.Accounts.SignUp;
using Answers.UseCases.Common;
using Answers.UseCases.Common.Authorization;
using Answers.UseCases.Common.Clocks;
using Answers.UseCases.Questions.GetQuestion;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace Answers.Details.Installers
{
	public static class InterfaceImplementationsInstaller
	{
		public static void InstallInterfaceImplementations(this IServiceCollection services)
		{
			services.AddSingleton<IClock, Clock>();

			services.AddSingleton<IPasswordHasher, PasswordHasher>();

			services.AddSingleton<IEmailAddressValidator, EmailAddressValidator>();
			services.AddSingleton<IMimeMessageComposer, MimeMessageComposer>();
			services.AddSingleton<IHtmlStripper, HtmlStripper>();
			services.AddSingleton<IMimeMessageSender, MimeMessageSender>();

			services.AddSingleton<ITokenTranslator, JwtTokenTranslator>();
			services.AddSingleton<UserIdClaim>();
			services.AddScoped<IAuthorizer, Authorizer>();

			services.AddSingleton<IRegistrationTemplateFormatter, RegistrationTemplateFormatter>();
			services.AddSingleton<IRegistrationMailSender, RegistrationMailSender>();

			services.AddSingleton<IGistCreator, GistCreator>();

			services.AddSingleton(provider =>
			{
				IConfiguration configuration = provider.GetService<IConfiguration>();
				IConfigurationSection configurationSection = configuration.GetSection(JwtConfiguration.JsonElementName);
				JwtConfiguration jwtConfiguration = new JwtConfiguration();
				configurationSection.Bind(jwtConfiguration);
				return jwtConfiguration;
			});
		}
	}
}
