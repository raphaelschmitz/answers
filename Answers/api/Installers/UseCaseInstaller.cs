using System.Collections.Generic;
using Answers.Entities.Models;
using Answers.UseCases.Accounts.GetAccount;
using Answers.UseCases.Accounts.Login;
using Answers.UseCases.Accounts.SignUp;
using Answers.UseCases.Admin.GetDashboard;
using Answers.UseCases.Admin.UpdateEmailSettings;
using Answers.UseCases.Admin.UpdateSettings;
using Answers.UseCases.Common;
using Answers.UseCases.Common.Validators;
using Answers.UseCases.Posts.AddComment;
using Answers.UseCases.Posts.DeletePost;
using Answers.UseCases.Posts.GetPostRevisions;
using Answers.UseCases.Posts.PlaceVote;
using Answers.UseCases.Posts.UpdatePost;
using Answers.UseCases.Questions.AddAnswer;
using Answers.UseCases.Questions.AddQuestion;
using Answers.UseCases.Questions.GetQuestion;
using Answers.UseCases.Questions.SearchQuestions;
using Answers.UseCases.Questions.UpdateQuestion;
using Answers.UseCases.Tags.DeleteTag;
using Answers.UseCases.Tags.GetTag;
using Answers.UseCases.Tags.UpdateTag;
using Microsoft.Extensions.DependencyInjection;


namespace Answers.Details.Installers
{
	public static class UseCaseInstaller
	{
		public static void InstallQueries(this IServiceCollection services)
		{
			InstallAccountUseCases(services);
			InstallAdminUseCases(services);
			InstallPostUseCases(services);
			InstallQuestionUseCases(services);
			InstallTagQueries(services);
		}


		private static void InstallAccountUseCases(IServiceCollection services)
		{
			services.AddScoped<SignUpUseCase>();
			services.AddScoped<LoginUseCase>();

			services.AddSingleton<IMapper<Account, GetAccountOutput>, GetAccountOutputMapper>();
			services.AddScoped<GetAccountUseCase>();
		}


		private static void InstallAdminUseCases(IServiceCollection services)
		{
			services.AddScoped<UpdateSettingsUseCase>();
			services.AddScoped<UpdateEmailSettingsUseCase>();
			services.AddScoped<GetDashboardUseCase>();
		}


		private static void InstallPostUseCases(IServiceCollection services)
		{
			services.AddScoped<UpdatePostUseCase>();
			services.AddScoped<DeletePostUseCase>();
			services.AddScoped<PlaceVoteUseCase>();
			services.AddScoped<AddCommentUseCase>();

			services.AddSingleton<IMapper<Post, GetPostRevisionsOutput>, GetQuestionRevisionsOutputMapper>();
			services.AddScoped<GetPostRevisionsUseCase>();
		}


		private static void InstallQuestionUseCases(IServiceCollection services)
		{
			services.AddScoped<UpdateQuestionUseCase>();

			services.AddScoped<IValidator<AddQuestionInput>, AddQuestionInputValidator>();
			services.AddScoped<AddQuestionUseCase>();

			services.AddScoped<AddAnswerUseCase>();

			services.AddSingleton<IMapper<IList<Question>, SearchQuestionsOutput>, SearchQuestionOutputMapper>();
			services.AddScoped<SearchQuestionsUseCase>();

			services.AddScoped<IValidator<GetQuestionInput>, GetQuestionInputValidator>();
			services.AddSingleton<IGetQuestionOutputMapper, GetQuestionOutputMapper>();
			services.AddScoped<GetQuestionUseCase>();
		}


		private static void InstallTagQueries(IServiceCollection services)
		{
			services.AddScoped<UpdateTagUseCase>();
			services.AddScoped<DeleteTagUseCase>();

			services.AddSingleton<IMapper<Tag, GetTagOutput>, GetTagOutputMapper>();
			services.AddScoped<GetTagUseCase>();
		}
	}
}
