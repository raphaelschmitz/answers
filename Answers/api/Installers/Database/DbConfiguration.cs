namespace Answers.Details.Installers.Database
{
	public class DbConfiguration
	{
		public static readonly string JsonElementName = "DbConfiguration";


		public string ConnectionString { get; set; }

	}
}
