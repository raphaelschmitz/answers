using System;
using Answers.Entities.Models;
using Answers.UseCases.Common;
using Answers.UseCases.Common.Persistence;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Tool.hbm2ddl;


namespace Answers.Details.Installers.Database
{
	public static class NHibernateInstaller
	{
		public static void InstallEntities(this IServiceCollection services)
		{
			services.AddSingleton(provider =>
			{
				DbConfiguration dbConfiguration = new DbConfiguration();
				IConfiguration configuration = provider.GetService<IConfiguration>();
				IConfigurationSection configurationSection = configuration.GetSection(DbConfiguration.JsonElementName);
				configurationSection.Bind(dbConfiguration);
				return dbConfiguration;
			});


			services.AddSingleton(provider =>
			{
				Configuration configuration = new Configuration();

				DbConfiguration dbConfiguration = provider.GetService<DbConfiguration>();
				string connectionString = dbConfiguration.ConnectionString;
				configuration.DataBaseIntegration(db =>
				{
					db.ConnectionString = connectionString;
					db.Dialect<MsSql2012Dialect>();
				});
				configuration.AddAnswersMappings();

				return configuration;
			});


			services.AddSingleton(provider =>
			{
				Configuration configuration = provider.GetService<Configuration>();
				return configuration.BuildSessionFactory();
			});


			services.AddScoped(provider =>
			{
				ISessionFactory sessionFactory = provider.GetService<ISessionFactory>();
				return sessionFactory.OpenSession();
			});
		}


		public static void SetUpDatabaseWithNHibernate(this IApplicationBuilder app)
		{
			IServiceProvider serviceProvider = app.ApplicationServices;
			Configuration configuration = serviceProvider.GetService<Configuration>();
			new SchemaExport(configuration).Execute(true, true, false);
			CreateTestModels(serviceProvider);
		}


		private static void CreateTestModels(IServiceProvider serviceProvider)
		{
			ISessionFactory sessionFactory = serviceProvider.GetService<ISessionFactory>();
			using (ISession session = sessionFactory.OpenSession())
			{
				WebsiteConfiguration websiteConfiguration = WebsiteConfiguration.Create("email@smtp.com", 1234, false,
					"noreply answers", "password", "mailer@answers.com", "Hello. Yu are regsiter.");
				using (ITransaction transaction = session.BeginTransaction())
				{
					session.Save(websiteConfiguration);
					transaction.Commit();
				}

				IPasswordHasher passwordHasher = serviceProvider.GetService<IPasswordHasher>();
				string password = passwordHasher.CreateSaltedHash("password");
				Account account = Account.Create("admin@admin.com", password, "admin");
				Account voter = Account.Create("voter@vote.com", password, "voter");
				Account voteReceiver = Account.Create("voteReceiver@vote.com", password, "voteReceiver");
				using (ITransaction transaction = session.BeginTransaction())
				{
					session.Save(account);
					session.Save(voter);
					session.Save(voteReceiver);
					transaction.Commit();
				}

				Question question = Question.Create(account, DateTime.Now, "How to do the twist?",
					"I would like to know how to do the twist. How does it work?");
				Tag tag = Tag.Create("Tag a", "Tag a description");
				question.Tags.Add(tag);
				Tag tag2 = Tag.Create("Tag b", "Tag b description");
				question.Tags.Add(tag2);
				using (ITransaction transaction = session.BeginTransaction())
				{
					session.Save(question);
					transaction.Commit();
				}

				Vote.Create(question.Post, voter, Vote.Up);


				Post post = question.AddAnswer(account, DateTime.Now,
					"Pretend to rub a towel behind your back while stomping out cigarettes with your heels.");
				Comment.Create(post, account, DateTime.Now, "What a great answer! This helped me!");
				using (ITransaction transaction = session.BeginTransaction())
				{
					//session.Save(post);
					session.Save(question);
					transaction.Commit();
				}

				Question question2 = Question.Create(voteReceiver, DateTime.Now, "How to get upvotes?",
					"I would like to know how to get upvotes. How does it work?");
				Vote vote = Vote.Create(question2.Post, account, Vote.Up);
				using (ITransaction transaction = session.BeginTransaction())
				{
					session.Save(question2);
					transaction.Commit();
				}


				Post answer2 = question2.AddAnswer(account, DateTime.Now,
					"In order to do B, you first have to do A.");
				Comment.Create(answer2, account, DateTime.Now, "Some people can skip B and directly go to C!");
				using (ITransaction transaction = session.BeginTransaction())
				{
					session.Save(answer2);
					transaction.Commit();
				}
			}
		}
	}
}
