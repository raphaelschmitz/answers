using System.Threading;
using System.Threading.Tasks;
using Answers.UseCases.Accounts.GetAccount;
using Answers.UseCases.Accounts.Login;
using Answers.UseCases.Accounts.SignUp;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace Answers.Details.Controllers
{
	[Route("api/accounts")]
	[ApiController]
	public class AccountsController : ControllerBase
	{
		private readonly ILogger<string> _logger;
		private readonly SignUpUseCase _signUpUseCase;
		private readonly LoginUseCase _loginUseCase;
		private readonly GetAccountUseCase _getAccountUseCase;


		public AccountsController(
			ILogger<string> logger,
			SignUpUseCase signUpUseCase,
			LoginUseCase loginUseCase,
			GetAccountUseCase getAccountUseCase)
		{
			_logger = logger;
			_signUpUseCase = signUpUseCase;
			_loginUseCase = loginUseCase;
			_getAccountUseCase = getAccountUseCase;
		}


		[HttpPost("signup")]
		public async Task<ActionResult<SignUpOutput>> SignUp(
			SignUpInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(SignUp)}\n{input}");
			return await _signUpUseCase.Execute(input, cancellationToken);
		}


		[HttpPost("login")]
		public async Task<ActionResult<LoginOutput>> Login(
			LoginInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(Login)}\n{input}");
			return await _loginUseCase.Execute(input, cancellationToken);
		}


		[HttpGet("get")]
		public async Task<ActionResult<GetAccountOutput>> Get(
			[FromQuery] GetAccountInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(Get)}\n{input}");
			return await _getAccountUseCase.Execute(input, cancellationToken);
		}
	}
}
