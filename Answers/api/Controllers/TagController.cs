using System.Threading;
using System.Threading.Tasks;
using Answers.UseCases.Tags.DeleteTag;
using Answers.UseCases.Tags.GetTag;
using Answers.UseCases.Tags.UpdateTag;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace Answers.Details.Controllers
{
	[Route("api/tags")]
	[ApiController]
	public class TagController : ControllerBase
	{
		private readonly ILogger<string> _logger;
		private readonly UpdateTagUseCase _loginUseCase;
		private readonly DeleteTagUseCase _deleteTagUseCase;
		private readonly GetTagUseCase _signUpUseCase;


		public TagController(
			ILogger<string> logger,
			UpdateTagUseCase loginUseCase,
			DeleteTagUseCase deleteTagUseCase,
			GetTagUseCase signUpUseCase)
		{
			_logger = logger;
			_signUpUseCase = signUpUseCase;
			_deleteTagUseCase = deleteTagUseCase;
			_loginUseCase = loginUseCase;
		}


		[HttpPost("update")]
		public async Task<ActionResult<UpdateTagOutput>> Update(
			UpdateTagInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(Update)}\n{input}");
			return await _loginUseCase.Execute(input, cancellationToken);
		}


		[HttpPost("delete")]
		public async Task<ActionResult<DeleteTagOutput>> Delete(
			DeleteTagInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(Delete)}\n{input}");
			return await _deleteTagUseCase.Execute(input, cancellationToken);
		}


		[HttpGet("get")]
		public async Task<ActionResult<GetTagOutput>> Get(
			[FromQuery] GetTagInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(Get)}\n{input}");
			return await _signUpUseCase.Execute(input, cancellationToken);
		}
	}
}
