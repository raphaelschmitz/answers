using System.Threading;
using System.Threading.Tasks;
using Answers.UseCases.Questions.AddAnswer;
using Answers.UseCases.Questions.AddQuestion;
using Answers.UseCases.Questions.GetQuestion;
using Answers.UseCases.Questions.SearchQuestions;
using Answers.UseCases.Questions.UpdateQuestion;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace Answers.Details.Controllers
{
	[Route("api/questions")]
	[ApiController]
	public class QuestionController : ControllerBase
	{
		private readonly ILogger<string> _logger;
		private readonly AddQuestionUseCase _addQuestionUseCase;
		private readonly UpdateQuestionUseCase _updateQuestionUseCase;
		private readonly AddAnswerUseCase _addAnswerUseCase;
		private readonly GetQuestionUseCase _getQuestionUseCase;
		private readonly SearchQuestionsUseCase _searchQuestionsUseCase;


		public QuestionController(
			ILogger<string> logger,
			AddQuestionUseCase addQuestionUseCase,
			UpdateQuestionUseCase updateQuestionUseCase,
			AddAnswerUseCase addAnswerUseCase,
			GetQuestionUseCase getQuestionUseCase,
			SearchQuestionsUseCase searchQuestionsUseCase)
		{
			_logger = logger;
			_addQuestionUseCase = addQuestionUseCase;
			_updateQuestionUseCase = updateQuestionUseCase;
			_getQuestionUseCase = getQuestionUseCase;
			_searchQuestionsUseCase = searchQuestionsUseCase;
			_addAnswerUseCase = addAnswerUseCase;
		}


		[HttpPost("create")]
		public async Task<ActionResult<AddQuestionOutput>> AddQuestion(
			AddQuestionInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(AddQuestion)}\n{input}");
			return await _addQuestionUseCase.Execute(input, cancellationToken);
		}


		[HttpPost("update")]
		public async Task<ActionResult<UpdateQuestionOutput>> UpdateQuestion(
			UpdateQuestionInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(UpdateQuestion)}\n{input}");
			return await _updateQuestionUseCase.Execute(input, cancellationToken);
		}


		[HttpPost("add-answer")]
		public async Task<ActionResult<AddAnswerOutput>> AddAnswer(
			AddAnswerInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(AddAnswer)}\n{input}");
			return await _addAnswerUseCase.Execute(input, cancellationToken);
		}


		[HttpGet("view")]
		public async Task<ActionResult<GetQuestionOutput>> GetQuestion(
			[FromQuery] GetQuestionInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(GetQuestion)}\n{input}");
			return await _getQuestionUseCase.Execute(input, cancellationToken);
		}


		[HttpGet("search")]
		public async Task<ActionResult<SearchQuestionsOutput>> SearchQuestions(
			[FromQuery] SearchQuestionsInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(SearchQuestions)}\n{input}");
			return await _searchQuestionsUseCase.Execute(input, cancellationToken);
		}
	}
}
