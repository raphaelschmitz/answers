using System.Threading;
using System.Threading.Tasks;
using Answers.UseCases.Admin.GetDashboard;
using Answers.UseCases.Admin.UpdateEmailSettings;
using Answers.UseCases.Admin.UpdateSettings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace Answers.Details.Controllers
{
	[Route("api/admin")]
	[ApiController]
	public class AdminController : ControllerBase
	{
		private readonly ILogger<string> _logger;
		private readonly GetDashboardUseCase _getAccountUseCase;
		private readonly UpdateSettingsUseCase _updateSettingsUseCase;
		private readonly UpdateEmailSettingsUseCase _updateEmailSettingsUseCase;


		public AdminController(
			ILogger<string> logger,
			GetDashboardUseCase getAccountUseCase,
			UpdateSettingsUseCase updateSettingsUseCase,
			UpdateEmailSettingsUseCase updateEmailSettingsUseCase)
		{
			_logger = logger;
			_getAccountUseCase = getAccountUseCase;
			_updateSettingsUseCase = updateSettingsUseCase;
			_updateEmailSettingsUseCase = updateEmailSettingsUseCase;
		}


		[HttpPost("update")]
		public async Task<ActionResult<UpdateSettingsOutput>> Update(
			UpdateSettingsInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(Update)}\n{input}");
			return await _updateSettingsUseCase.Execute(input, cancellationToken);
		}


		[HttpPost("update-email-settings")]
		public async Task<ActionResult<UpdateEmailSettingsOutput>> UpdateEmailSettings(
			UpdateEmailSettingsInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(UpdateEmailSettings)}\n{input}");
			return await _updateEmailSettingsUseCase.Execute(input, cancellationToken);
		}


		[HttpGet("get")]
		public async Task<ActionResult<GetDashboardOutput>> Get(
			[FromQuery] GetDashboardInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(Get)}\n{input}");
			return await _getAccountUseCase.Execute(input, cancellationToken);
		}
	}
}
