using System.Threading;
using System.Threading.Tasks;
using Answers.UseCases.Posts.AddComment;
using Answers.UseCases.Posts.DeletePost;
using Answers.UseCases.Posts.GetPostRevisions;
using Answers.UseCases.Posts.PlaceVote;
using Answers.UseCases.Posts.UpdatePost;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace Answers.Details.Controllers
{
	[Route("api/posts")]
	[ApiController]
	public class PostsController : ControllerBase
	{
		private readonly ILogger<string> _logger;
		private readonly UpdatePostUseCase _updatePostUseCase;
		private readonly GetPostRevisionsUseCase _getPostRevisionsUseCase;
		private readonly PlaceVoteUseCase _placeVoteUseCase;
		private readonly DeletePostUseCase _deletePostUseCase;
		private readonly AddCommentUseCase _addCommentUseCase;


		public PostsController(
			ILogger<string> logger,
			UpdatePostUseCase updatePostUseCase,
			GetPostRevisionsUseCase getPostRevisionsUseCase,
			PlaceVoteUseCase placeVoteUseCase,
			DeletePostUseCase deletePostUseCase,
			AddCommentUseCase addCommentUseCase)
		{
			_logger = logger;
			_updatePostUseCase = updatePostUseCase;
			_getPostRevisionsUseCase = getPostRevisionsUseCase;
			_placeVoteUseCase = placeVoteUseCase;
			_deletePostUseCase = deletePostUseCase;
			_addCommentUseCase = addCommentUseCase;
		}


		[HttpPost("delete")]
		public async Task<ActionResult<DeletePostOutput>> DeleteAnswer(
			DeletePostInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(DeleteAnswer)}\n{input}");
			return await _deletePostUseCase.Execute(input, cancellationToken);
		}


		[HttpPost("update")]
		public async Task<ActionResult<UpdatePostOutput>> UpdateAnswer(
			UpdatePostInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(UpdateAnswer)}\n{input}");
			return await _updatePostUseCase.Execute(input, cancellationToken);
		}


		[HttpPost("create-comment")]
		public async Task<ActionResult<AddCommentOutput>> AddAnswerComment(
			AddCommentInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(AddAnswerComment)}\n{input}");
			return await _addCommentUseCase.Execute(input, cancellationToken);
		}


		[HttpPost("vote")]
		public async Task<ActionResult<PlaceVoteOutput>> Vote(
			PlaceVoteInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(Vote)}\n{input}");
			return await _placeVoteUseCase.Execute(input, cancellationToken);
		}


		[HttpGet("get-revisions")]
		public async Task<ActionResult<GetPostRevisionsOutput>> GetQuestionRevisions(
			[FromQuery] GetPostRevisionsInput input,
			CancellationToken cancellationToken)
		{
			_logger.LogDebug($">>> {GetType()}::{nameof(GetQuestionRevisions)}\n{input}");
			return await _getPostRevisionsUseCase.Execute(input, cancellationToken);
		}
	}
}
