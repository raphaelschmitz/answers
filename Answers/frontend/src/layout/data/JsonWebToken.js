export default class JsonWebToken {
	static CreateFromString(tokenString) {
		if (tokenString == null) return new JsonWebToken(null, null, null);

		let raw = tokenString;
		const [headerString, payloadString] = tokenString.split(".");
		let header = JSON.parse(window.atob(headerString));
		let payload = JSON.parse(window.atob(payloadString));
		return new JsonWebToken(raw, header, payload);
	}

	constructor(raw, header, payload) {
		this.raw = raw;
		this.header = header;
		this.payload = payload;
	}

	isLoggedIn() {
		if (this.raw == null) return false;
		if (this.isExpired()) return false;
		return true;
	}

	isExpired() {
		let expireTime = this.payload.exp * 1000;
		return expireTime > Date.now;
	}

	getUserName() {
		return this.payload.UserName;
	}

	isAdmin() {
		if (this.raw == null) return false;
		return this.payload.AccessAdminArea != null;
	}
}