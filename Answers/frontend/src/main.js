import Vue from "vue";
import VueMaterial from "vue-material";

import App from "./App.vue";
import axios from "./plugins/AxiosInstaller";
import router from "./plugins/router";
import store from "./plugins/VuexInstaller";

Vue.config.productionTip = false;

Vue.prototype.$http = axios;

Vue.use(VueMaterial);

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount("#app");
