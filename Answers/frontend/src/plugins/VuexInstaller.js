import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		accessToken: null
	},
	mutations: {
		updateAccessToken: (state, accessToken) => {
			state.accessToken = accessToken;
		},
		setAccessToken: (state, accessToken) => {
			state.accessToken = accessToken;
		},
		logout: state => {
			state.accessToken = null;
		}
	},
	actions: {
		fetchAccessToken({ commit }) {
			commit("updateAccessToken", localStorage.getItem("accessToken"));
		},
		setAccessToken({ commit }, accessToken) {
			commit("setAccessToken", accessToken);
			localStorage.setItem("accessToken", accessToken);
		},
		deleteAccessToken({ commit }) {
			localStorage.removeItem("accessToken");
			commit("logout");
		}
	}
});
