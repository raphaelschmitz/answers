import Vue from "vue";
import Router from "vue-router";

import NotFound from "../errors/pages/NotFound";
import Home from "../homepage/pages/Home";
import Question from "../questions/pages/Question";
import SignUp from "../accounts/pages/SignUp";
import Login from "../accounts/pages/Login";
import AccountCreated from "../accounts/pages/AccountCreated";
import Account from "../accounts/pages/Account";
import Tag from "../tags/pages/Tag";
import AdminDashboard from "../admin/pages/AdminDashboard";

Vue.use(Router);

export default new Router({
	mode: "history",
	base: process.env.BASE_URL,
	routes: [
		{
			path: "/",
			name: "home",
			component: Home
		},
		{
			path: "/admin",
			name: "AdminDashboard",
			component: AdminDashboard
		},
		{
			path: "/questions/get/:id",
			name: "question",
			props: true,
			component: Question
		},
		{
			path: "/accounts/signup",
			name: "accountRegister",
			component: SignUp
		},
		{
			path: "/accounts/account-created",
			name: "accountCreated",
			component: AccountCreated
		},
		{
			path: "/accounts/login/:origin?",
			name: "accountLogin",
			props: true,
			component: Login
		},
		{
			path: "/accounts/get/:id",
			name: "account",
			props: true,
			component: Account
		},
		{
			path: "/tags/get/:id",
			name: "tag",
			props: true,
			component: Tag
		},
		{
			path: "*",
			name: "NotFound",
			component: NotFound
		}
	]
});
