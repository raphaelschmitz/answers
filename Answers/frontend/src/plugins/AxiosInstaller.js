import axios from "axios";

export default axios.create({
	baseURL: "/api/",
	//timeout: 5000,
	headers: {
		//Authorization: `Bearer: ${this.jwt}`,
		"Content-Type": "application/json"
	}
});
