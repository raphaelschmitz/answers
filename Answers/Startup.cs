using System;
using Answers.Details.Installers;
using Answers.Details.Installers.Database;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using VueCliMiddleware;


namespace Answers.Details
{
	public class Startup
	{
		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddTransient<ILogger<string>, Logger<string>>();

			services.InstallEntities();
			services.InstallQueries();
			services.InstallInterfaceImplementations();


			services.AddMvcCore()
				.SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
				.AddJsonFormatters();

			services.AddSingleton<IApiDescriptionGroupCollectionProvider, ApiDescriptionGroupCollectionProvider>();
			services.TryAddEnumerable(ServiceDescriptor
				.Transient<IApiDescriptionProvider, DefaultApiDescriptionProvider>());
			services.AddOpenApiDocument();


			services.AddSpaStaticFiles(configuration =>
			{
				configuration.RootPath = "frontend/src";
			});
		}


		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.SetUpDatabaseWithNHibernate();
			}
			else
			{
				app.UseExceptionHandler("/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseHttpsRedirection();
			app.UseStaticFiles();
			app.UseSpaStaticFiles();


			app.UseMvc();

			app.UseOpenApi();
			app.UseSwaggerUi3();


			app.UseSpa(spa =>
			{
				spa.Options.SourcePath = "frontend";
				spa.Options.StartupTimeout = TimeSpan.FromSeconds(120);
				if (env.IsDevelopment())
				{
					spa.UseVueCli(npmScript: "serve");
				}
			});
		}
	}
}
