using System;
using System.Collections.Generic;
using Answers.Entities.Models;
using Answers.InterfaceAdapters.Accounts.Emails;
using Answers.InterfaceAdapters.Common.Emails;
using MimeKit;
using Moq;
using Xunit;


namespace InterfaceAdaptersUnitTests.Accounts.Emails
{
	public class RegistrationMailSenderTests
	{
		[Fact]
		public void FormatsTemplateWithData()
		{
			// Arrange
			Mock<IRegistrationTemplateFormatter> formatterMock = new Mock<IRegistrationTemplateFormatter>();
			IRegistrationTemplateFormatter registrationTemplateFormatter = formatterMock.Object;

			Mock<IMimeMessageComposer> messageComposerMock = new Mock<IMimeMessageComposer>();
			IMimeMessageComposer mimeMessageComposer = messageComposerMock.Object;

			Mock<IMimeMessageSender> messageSenderMock = new Mock<IMimeMessageSender>();
			IMimeMessageSender mimeMessageSender = messageSenderMock.Object;

			RegistrationMailSender registrationMailSender = new RegistrationMailSender(
				registrationTemplateFormatter, mimeMessageComposer, mimeMessageSender);

			string template = "uorvhculsnl";
			Account account = Account.Create("", "", "");
			EmailConfirmProcess emailConfirmProcess = EmailConfirmProcess.Create(account, "", DateTime.Now);
			WebsiteConfiguration websiteConfiguration = WebsiteConfiguration.Create(
				"", 0, false, "", "", "", template);


			// Act
			registrationMailSender.Send(websiteConfiguration, emailConfirmProcess);


			// Assert
			formatterMock.Verify(x => x.Format(template, account, emailConfirmProcess), Times.Once);
		}


		[Fact]
		public void ComposesMessageWithTheGivenData()
		{
			// Arrange
			string from = "ornsicef";
			string subject = "Registration";
			string body = "alecnascjnrjnsvrfl";
			Account account = Account.Create("", "", "");
			Account[] to = { account };

			Mock<IRegistrationTemplateFormatter> formatterMock = new Mock<IRegistrationTemplateFormatter>();
			formatterMock
				.Setup(x => x.Format(
					It.IsAny<string>(),
					It.IsAny<Account>(),
					It.IsAny<EmailConfirmProcess>()))
				.Returns(body);
			IRegistrationTemplateFormatter registrationTemplateFormatter = formatterMock.Object;

			Mock<IMimeMessageComposer> messageComposerMock = new Mock<IMimeMessageComposer>();
			IMimeMessageComposer mimeMessageComposer = messageComposerMock.Object;

			Mock<IMimeMessageSender> messageSenderMock = new Mock<IMimeMessageSender>();
			IMimeMessageSender mimeMessageSender = messageSenderMock.Object;

			RegistrationMailSender registrationMailSender = new RegistrationMailSender(
				registrationTemplateFormatter, mimeMessageComposer, mimeMessageSender);

			EmailConfirmProcess emailConfirmProcess = EmailConfirmProcess.Create(account, "", DateTime.Now);
			WebsiteConfiguration websiteConfiguration = WebsiteConfiguration.Create(
				"", 0, false, "", "", from, "");


			// Act
			registrationMailSender.Send(websiteConfiguration, emailConfirmProcess);


			// Assert
			messageComposerMock.Verify(x => x.CreateMimeMessage(from, to, subject, body), Times.Once);
		}


		[Fact]
		public void SendsMessage()
		{
			// Arrange
			Mock<IRegistrationTemplateFormatter> formatterMock = new Mock<IRegistrationTemplateFormatter>();
			IRegistrationTemplateFormatter registrationTemplateFormatter = formatterMock.Object;

			Mock<IMimeMessageComposer> messageComposerMock = new Mock<IMimeMessageComposer>();
			MimeMessage mimeMessage = new MimeMessage();
			messageComposerMock
				.Setup(x => x.CreateMimeMessage(
					It.IsAny<string>(),
					It.IsAny<IEnumerable<Account>>(),
					It.IsAny<string>(),
					It.IsAny<string>()))
				.Returns(mimeMessage);
			IMimeMessageComposer mimeMessageComposer = messageComposerMock.Object;

			Mock<IMimeMessageSender> messageSenderMock = new Mock<IMimeMessageSender>();
			IMimeMessageSender mimeMessageSender = messageSenderMock.Object;

			RegistrationMailSender registrationMailSender = new RegistrationMailSender(
				registrationTemplateFormatter, mimeMessageComposer, mimeMessageSender);

			Account account = Account.Create("", "", "");
			EmailConfirmProcess emailConfirmProcess = EmailConfirmProcess.Create(account, "", DateTime.Now);
			WebsiteConfiguration websiteConfiguration = WebsiteConfiguration.Create(
				"", 0, false, "", "", "", "");


			// Act
			registrationMailSender.Send(websiteConfiguration, emailConfirmProcess);


			// Assert
			messageSenderMock.Verify(x => x.SendMimeMessage(websiteConfiguration, mimeMessage), Times.Once);
		}
	}
}
