using System;
using System.Collections.Generic;
using System.Security.Claims;
using Answers.InterfaceAdapters.Accounts.Authorizers;
using Xunit;


namespace InterfaceAdaptersUnitTests.Accounts.Authorizers
{
	public class UserIdClaimTests
	{
		[Fact]
		public void ReturnsEmptyGuidIfClaimNotFound()
		{
			// Arrange
			UserIdClaim userIdClaim = new UserIdClaim();
			Guid guid = Guid.NewGuid();
			List<Claim> claims = new List<Claim>();


			// Act
			Guid result = userIdClaim.Find(claims);


			// Assert
			Assert.Equal(Guid.Empty, result);
		}


		[Fact]
		public void CanFindClaim()
		{
			// Arrange
			UserIdClaim userIdClaim = new UserIdClaim();
			Guid guid = Guid.NewGuid();
			List<Claim> claims = new List<Claim> { userIdClaim.Create(guid) };


			// Act
			Guid result = userIdClaim.Find(claims);


			// Assert
			Assert.Equal(guid, result);
		}
	}
}
