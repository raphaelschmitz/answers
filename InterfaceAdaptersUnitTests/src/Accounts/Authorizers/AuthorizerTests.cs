using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using Answers.Entities.Models;
using Answers.Entities.Values;
using Answers.InterfaceAdapters.Accounts.Authorizers;
using Answers.UseCases.Common.Authorization;
using Moq;
using NHibernate;
using Xunit;


namespace InterfaceAdaptersUnitTests.Accounts.Authorizers
{
	public class AuthorizerTests
	{
		[Fact]
		public void CreatesTokenFromTokenTranslator()
		{
			// Arrange
			Mock<ISession> sessionMock = new Mock<ISession>();
			ISession session = sessionMock.Object;

			Mock<ITokenTranslator> tokenTranslatorMock = new Mock<ITokenTranslator>();
			string tokenString = "sfrsvsrcs";
			tokenTranslatorMock
				.Setup(x => x.CreateToken(It.IsAny<IEnumerable<Claim>>()))
				.Returns(tokenString);
			ITokenTranslator tokenTranslator = tokenTranslatorMock.Object;

			UserIdClaim userIdClaim = new UserIdClaim();

			IAuthorizer authorizer = new Authorizer(session, tokenTranslator, userIdClaim);
			Account account = Account.Create("", "", "");


			// Act
			string result = authorizer.CreateToken(account);


			// Assert
			Assert.Equal(tokenString, result);
		}


		[Fact]
		public async void FindsAllowableInAccount()
		{
			// Arrange
			Allowable allowable = Allowable.AddQuestions;

			Account account = Account.Create("", "", "");
			Mock<ISession> sessionMock = new Mock<ISession>();
			sessionMock
				.Setup(x => x.GetAsync<Account>(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
				.ReturnsAsync(account);
			ISession session = sessionMock.Object;

			Mock<ITokenTranslator> tokenTranslatorMock = new Mock<ITokenTranslator>();
			tokenTranslatorMock
				.Setup(x => x.ReadToken(It.IsAny<string>()))
				.Returns(new ClaimsPrincipal());
			ITokenTranslator tokenTranslator = tokenTranslatorMock.Object;

			UserIdClaim userIdClaim = new UserIdClaim();

			IAuthorizer authorizer = new Authorizer(session, tokenTranslator, userIdClaim);
			string token = authorizer.CreateToken(account);


			// Act
			AuthorizationResult authorizationResult = await authorizer.Check(token, CancellationToken.None);


			// Assert
			Assert.NotNull(authorizationResult);
			Assert.True(authorizationResult.IsAuthorizedTo(allowable));
		}

		
		
		[Fact]
		public async void RejectsExpiredToken()
		{
			// Arrange
			Account account = Account.Create("", "", "");
			Mock<ISession> sessionMock = new Mock<ISession>();
			sessionMock
				.Setup(x => x.GetAsync<Account>(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
				.ReturnsAsync(account);
			ISession session = sessionMock.Object;

			Mock<ITokenTranslator> tokenTranslatorMock = new Mock<ITokenTranslator>();
			tokenTranslatorMock
				.Setup(x => x.ReadToken(It.IsAny<string>()))
				.Returns((ClaimsPrincipal)null);
			ITokenTranslator tokenTranslator = tokenTranslatorMock.Object;

			UserIdClaim userIdClaim = new UserIdClaim();

			IAuthorizer authorizer = new Authorizer(session, tokenTranslator, userIdClaim);
			string token = authorizer.CreateToken(account);


			// Act
			AuthorizationResult authorizationResult = await authorizer.Check(token, CancellationToken.None);


			// Assert
			Assert.False(authorizationResult.IsAuthenticated);
		}
		
		
	}
}
