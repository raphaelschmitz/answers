using System;
using System.Collections.Generic;
using System.Security.Claims;
using Answers.Entities.Values;
using Answers.InterfaceAdapters.Accounts.Authorizers;
using Answers.InterfaceAdapters.Accounts.JsonWebTokens;
using Answers.InterfaceAdapters.Common;
using Answers.UseCases.Common.Clocks;
using Microsoft.IdentityModel.JsonWebTokens;
using Moq;
using Xunit;


namespace InterfaceAdaptersUnitTests.Accounts.JsonWebTokens
{
	public class JwtTokenTranslatorTests
	{
		private static string Secret =
			"33333333333333333333333333333333333333333333333333333333333333333333333333333333";


		[Fact]
		public void CanCreateToken()
		{
			// Arrange
			JwtConfiguration jwtConfiguration = new JwtConfiguration
			{
				Secret = Secret,
				Issuer = "ab",
				Audience = "ab",
				ExpirationTime = "1:0"
			};
			IClock clock = new Clock();
			ITokenTranslator tokenTranslator = new JwtTokenTranslator(jwtConfiguration, clock);

			Allowable allowable = Allowable.AddAnswers;
			Claim claim = new Claim(allowable.ClaimId, "");
			List<Claim> claims = new List<Claim> {claim};


			// Act
			tokenTranslator.CreateToken(claims);
		}


		[Fact]
		public void CanConsumeCreatedToken()
		{
			// Arrange
			JwtConfiguration jwtConfiguration = new JwtConfiguration
			{
				Secret = Secret,
				Issuer = "ab",
				Audience = "ab",
				ExpirationTime = "1:0"
			};
			IClock clock = new Clock();
			JwtTokenTranslator jwtJwtTokenAuthorizer = new JwtTokenTranslator(jwtConfiguration, clock);

			Allowable allowable = Allowable.AddAnswers;
			Claim claim = allowable.ToClaim();
			List<Claim> claims = new List<Claim> {claim};
			string token = jwtJwtTokenAuthorizer.CreateToken(claims);


			// Act
			ClaimsPrincipal claimsPrincipal = jwtJwtTokenAuthorizer.ReadToken(token);


			// Assert
			Assert.True(claimsPrincipal.HasClaim(claim.Type, claim.Value));
		}



		[Fact]
		public void UsesTimespanFromConfiguration()
		{
			// Arrange
			string twoDaysAndFourHoursString = "2.4:0:0";

			JwtConfiguration jwtConfiguration = new JwtConfiguration
			{
				Secret = Secret,
				Issuer = "ab",
				Audience = "ab",
				ExpirationTime = twoDaysAndFourHoursString
			};

			DateTime now = DateTime.UtcNow;
			now = now.AddTicks(-(now.Ticks % TimeSpan.TicksPerSecond)); // truncate milliseconds
			TimeSpan expirationTimeSpan = TimeSpan.Parse(twoDaysAndFourHoursString);
			DateTime expirationTime = now + expirationTimeSpan;
			Mock<IClock> clockMock = new Mock<IClock>();
			clockMock
				.Setup(x => x.GetNow())
				.Returns(now);
			IClock clock = clockMock.Object;

			JwtTokenTranslator jwtTokenTranslator = new JwtTokenTranslator(jwtConfiguration, clock);
			
			Mock<ITokenTranslator> tokenTranslatorMock = new Mock<ITokenTranslator>();


			ITokenTranslator tokenTranslator = tokenTranslatorMock.Object;

			Allowable allowable = Allowable.AddAnswers;
			Claim claim = allowable.ToClaim();
			List<Claim> claims = new List<Claim> {claim};
			string token = jwtTokenTranslator.CreateToken(claims);


			// Act
			ClaimsPrincipal claimsPrincipal = jwtTokenTranslator.ReadToken(token);


			// Assert
			Claim expirationClaim = claimsPrincipal.FindFirst(JwtRegisteredClaimNames.Exp);
			long expAsNumber = long.Parse(expirationClaim.Value);
			DateTime resultExpirationTime = DateTime.UnixEpoch.AddSeconds(expAsNumber);
			Assert.Equal(expirationTime, resultExpirationTime);
		}


		
	}
}
