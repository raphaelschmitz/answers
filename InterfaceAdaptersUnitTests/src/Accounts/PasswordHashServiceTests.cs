using Answers.InterfaceAdapters.Accounts.PasswordHashing;
using Answers.UseCases.Common;
using Xunit;


namespace InterfaceAdaptersUnitTests.Accounts
{
	public class PasswordHashServiceTests
	{
		[Fact]
		public void CreateSaltedHash_SameInput_YieldsDifferentResultsEachTime()
		{
			// Arrange
			IPasswordHasher passwordHashService = new PasswordHasher();
			var password = "MyCrazyPassw0rd321%$";


			// Act
			var saltedHash1 = passwordHashService.CreateSaltedHash(password);
			var saltedHash2 = passwordHashService.CreateSaltedHash(password);
			var saltedHash3 = passwordHashService.CreateSaltedHash(password);


			// Assert
			Assert.NotEqual(saltedHash1, saltedHash2);
			Assert.NotEqual(saltedHash2, saltedHash3);
		}


		[Fact]
		public void CheckIfPasswordMatches_CorrectPassword_IsAccepted()
		{
			// Arrange
			IPasswordHasher passwordHashService = new PasswordHasher();
			var password = "MyCrazyPassw0rd321%$";
			var saltedHash = passwordHashService.CreateSaltedHash(password);


			// Act
			var passwordIsCorrect = passwordHashService.CheckIfPasswordMatches(password, saltedHash);


			// Assert
			Assert.True(passwordIsCorrect);
		}


		[Fact]
		public void CheckIfPasswordMatches_WrongPassword_IsRejected()
		{
			// Arrange
			IPasswordHasher passwordHashService = new PasswordHasher();
			var password = "MyCrazyPassw0rd321%$";
			var wrongPassword = "guest";
			var saltedHash = passwordHashService.CreateSaltedHash(password);


			// Act
			var passwordIsCorrect = passwordHashService.CheckIfPasswordMatches(wrongPassword, saltedHash);


			// Assert
			Assert.False(passwordIsCorrect);
		}


		[Fact]
		public void CheckIfPasswordMatches_OnlySlightlyDifferentPassword_IsRejected()
		{
			// Arrange
			IPasswordHasher passwordHashService = new PasswordHasher();
			var password = "MyCrazyPassw0rd321%$";
			var slightlyDifferentPassword = password + "a";
			var saltedHash = passwordHashService.CreateSaltedHash(password);


			// Act
			var passwordIsCorrect = passwordHashService.CheckIfPasswordMatches(slightlyDifferentPassword, saltedHash);


			// Assert
			Assert.False(passwordIsCorrect);
		}


		[Fact]
		public void CheckIfPasswordMatches_nullForPasssword_IsRejectedWithoutException()
		{
			// Arrange
			IPasswordHasher passwordHashService = new PasswordHasher();
			var password = "MyCrazyPassw0rd321%$";


			// Act
			var passwordIsCorrect = passwordHashService.CheckIfPasswordMatches(password, null);


			// Assert
			Assert.False(passwordIsCorrect);
		}
	}
}
