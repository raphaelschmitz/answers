using Answers.InterfaceAdapters.Common.Emails;
using Xunit;


namespace InterfaceAdaptersUnitTests.Common.Emails
{
	public class EmailAddressValidatorTests
	{
		[Fact]
		public void RecognizesValidEmail()
		{
			EmailAddressValidator emailAddressValidator = new EmailAddressValidator();
			string emailAddress = "user@domain.com";

			bool isValid = emailAddressValidator.Validate(emailAddress);

			Assert.True(isValid);
		}


		[Fact]
		public void RejectsIfTheresNoAtSign()
		{
			EmailAddressValidator emailAddressValidator = new EmailAddressValidator();
			string emailAddress = "userdomain.com";

			bool isValid = emailAddressValidator.Validate(emailAddress);

			Assert.False(isValid);
		}


		[Fact]
		public void RejectsIfTheresNoDot()
		{
			EmailAddressValidator emailAddressValidator = new EmailAddressValidator();
			string emailAddress = "user@domaincom";

			bool isValid = emailAddressValidator.Validate(emailAddress);

			Assert.False(isValid);
		}
	}
}
