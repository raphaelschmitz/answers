using Answers.InterfaceAdapters.Common.Emails;
using Xunit;


namespace InterfaceAdaptersUnitTests.Common.Emails
{
	public class HtmlStripperTests
	{
		[Fact]
		public void StripsHtml()
		{
			// Arrange
			string input = "<b>que?</b>";
			string expected = "que?";
			IHtmlStripper htmlStripper = new HtmlStripper();


			// Act
			string result = htmlStripper.Strip(input);


			// Assert
			Assert.Equal(expected, result);
		}
	}
}
