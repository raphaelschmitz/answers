using System;
using System.Collections.Generic;
using System.Linq;
using Answers.Entities.Models;
using Answers.InterfaceAdapters.Common.Emails;
using MimeKit;
using Moq;
using Xunit;


namespace InterfaceAdaptersUnitTests.Common.Emails
{
	public class MimeMessageComposerTests
	{
		[Fact]
		public void ThrowsForNullParameters()
		{
			// Arrange
			Mock<IHtmlStripper> htmlStripperMock = new Mock<IHtmlStripper>();
			IHtmlStripper htmlStripper = htmlStripperMock.Object;

			IMimeMessageComposer mimeMessageComposer = new MimeMessageComposer(htmlStripper);


			// Assert
			Assert.Throws<ArgumentNullException>(() =>
				mimeMessageComposer.CreateMimeMessage(null, new Account[0], "", ""));
			Assert.Throws<ArgumentNullException>(() =>
				mimeMessageComposer.CreateMimeMessage("", null, "", ""));
			Assert.Throws<ArgumentNullException>(() =>
				mimeMessageComposer.CreateMimeMessage("", new Account[0], null, ""));
			Assert.Throws<ArgumentNullException>(() =>
				mimeMessageComposer.CreateMimeMessage("", new Account[0], "", null));
		}


		[Fact]
		public void ValuesAreBeingSet()
		{
			// Arrange
			Mock<IHtmlStripper> htmlStripperMock = new Mock<IHtmlStripper>();
			IHtmlStripper htmlStripper = htmlStripperMock.Object;

			IMimeMessageComposer mimeMessageComposer = new MimeMessageComposer(htmlStripper);

			string from = "from@page.com";

			string toEmailAddress = "to@page.com";
			string toUserName = "toer";
			Account toAccount = Account.Create(toEmailAddress, "", toUserName);
			List<Account> to = new List<Account> { toAccount };

			string subject = "subject";
			string htmlBody = "htmlBody";


			// Act
			MimeMessage mimeMessage = mimeMessageComposer.CreateMimeMessage(from, to, subject, htmlBody);


			// Assert
			Assert.Equal(from, (mimeMessage.From.First() as MailboxAddress).Address);

			Assert.Equal(to.Count, mimeMessage.To.Count);
			Assert.Equal(toEmailAddress, (mimeMessage.To.First() as MailboxAddress).Address);

			Assert.Equal(subject, mimeMessage.Subject);
			Assert.Equal(htmlBody, mimeMessage.HtmlBody);
		}


		[Fact]
		public void TextBodyIsHtmlStripped()
		{
			// Arrange
			Mock<IHtmlStripper> htmlStripperMock = new Mock<IHtmlStripper>();
			string strippedReturn = "oiawenlcunlsr.ukc";
			htmlStripperMock
				.Setup(x => x.Strip(It.IsAny<string>()))
				.Returns(strippedReturn);
			IHtmlStripper htmlStripper = htmlStripperMock.Object;

			IMimeMessageComposer mimeMessageComposer = new MimeMessageComposer(htmlStripper);

			string from = "from@page.com";
			Account[] to = new Account[0];
			string subject = "subject";
			string htmlBody = "htmlBody";


			// Act
			MimeMessage mimeMessage = mimeMessageComposer.CreateMimeMessage(from, to, subject, htmlBody);


			// Assert
			htmlStripperMock.Verify(x => x.Strip(htmlBody), Times.Once);
			Assert.Equal(strippedReturn, mimeMessage.TextBody);
		}
	}
}
