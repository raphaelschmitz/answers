using System;
using Answers.InterfaceAdapters.Posts;
using Xunit;


namespace InterfaceAdaptersUnitTests.Posts
{
	public class GistCreatorTests
	{
		[Fact]
		public void ReturnedStringDoesNotExceedMaxLength()
		{
			GistCreator gistCreator = new GistCreator();
			string input = "apifioianrvioanvraoicnasecainc;na;onvrnaseioanveivons";
			int maxLength = 20;

			string gist = gistCreator.CreateGist(input, maxLength);

			Assert.True(gist.Length <= maxLength, "actual length: " + gist.Length);
		}


		[Fact]
		public void DoesNotCutOffTooMuch()
		{
			GistCreator gistCreator = new GistCreator();
			string partToKeep = "apif ioia rvio";
			int lengthOfPartToKeep = partToKeep.Length;
			string partThatShouldBeCut = " anvr";
			string input = partToKeep + partThatShouldBeCut;
			int maxLength = lengthOfPartToKeep + 3;

			string gist = gistCreator.CreateGist(input, maxLength);

			Assert.True(gist.Length == maxLength, "actual length: " + gist.Length);
		}


		[Fact]
		public void RemovesReallyLongWords()
		{
			GistCreator gistCreator = new GistCreator();
			string input = "apif ioiarrvioerervsfefsegsgbdrg";
			int maxLength = input.Length - 2;

			string gist = gistCreator.CreateGist(input, maxLength);

			Assert.True(gist.Length < 10, "actual length: " + gist.Length);
		}


		[Fact]
		public void EndsInEllipseIfLongerInput()
		{
			GistCreator gistCreator = new GistCreator();
			string input = "apifioianrvioanvraoicnasecainc;na;onvrnaseioanveivons";
			int maxLength = 20;

			string gist = gistCreator.CreateGist(input, maxLength);

			Assert.True(gist.EndsWith("..."), "actual gist: " + gist);
		}


		[Fact]
		public void DoesNotAddAnEllipseForShorterOutput()
		{
			GistCreator gistCreator = new GistCreator();
			string input = "apifioianr1234567890";
			int maxLength = 20;

			string gist = gistCreator.CreateGist(input, maxLength);

			Assert.False(gist.EndsWith("..."), "actual gist: " + gist);
		}


		[Fact]
		public void DoesNotCutWords()
		{
			GistCreator gistCreator = new GistCreator();
			string input = "train window pancakehorse rhino";
			int maxLength = 20;

			string gist = gistCreator.CreateGist(input, maxLength);

			Assert.False(gist.Contains("pan"), "actual result: " + gist);
		}


		[Fact]
		public void RejectsNullString()
		{
			GistCreator gistCreator = new GistCreator();
			int maxLength = 23;

			Assert.Throws<ArgumentNullException>(() => gistCreator.CreateGist(null, maxLength));
		}


		[Fact]
		public void RejectsNegativeLength()
		{
			GistCreator gistCreator = new GistCreator();
			string input = "train window pancakehorse rhino";
			int maxLength = -1;

			Assert.Throws<ArgumentOutOfRangeException>(() => gistCreator.CreateGist(input, maxLength));
		}


		[Fact]
		public void RejectsTooShortInput()
		{
			GistCreator gistCreator = new GistCreator();
			string input = "train window pancakehorse rhino";
			int maxLength = 2;

			Assert.Throws<ArgumentOutOfRangeException>(() => gistCreator.CreateGist(input, maxLength));
		}
	}
}
