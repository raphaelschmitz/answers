using System;
using Answers.Entities.Models;
using Answers.Entities.Values;


namespace EntitiesUnitTests
{
	public static class DefaultFactory
	{
		public static Account CreateAccount(string emailAddress = "", string passwordHash = "", string userName = "")
		{
			return Account.Create(emailAddress, passwordHash, userName);
		}


		public static Question CreateQuestion(
			Account account = null,
			DateTime? creationDate = null,
			string title = "",
			string content = "")
		{
			return Question.Create(
				account ?? CreateAccount(),
				creationDate ?? DateTime.Now,
				title,
				content);
		}
	}
}
