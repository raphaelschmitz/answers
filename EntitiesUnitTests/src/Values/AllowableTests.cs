using System;
using Answers.Entities.Values;
using Xunit;


namespace EntitiesUnitTests.Values
{
	public class AllowableTests
	{
		[Fact]
		public void NoIdOrClaimReused()
		{
			foreach (Allowable allowable in Allowable.All)
			{
				foreach (Allowable other in Allowable.All)
				{
					if (other == allowable) continue;
					Assert.NotEqual(allowable.Code, other.Code);
					Assert.NotEqual(allowable.ClaimId, other.ClaimId);
				}
			}
		}


		[Fact]
		public void Get_CanFindAllowable()
		{
			// Arrange
			Allowable allowable = Allowable.AddAnswerComments;
			int code = allowable.Code;


			// Act
			Allowable result = Allowable.Get(code);


			// Assert
			Assert.Equal(allowable, result);
		}


		[Fact]
		public void Get_ThrowsForWrongNumber()
		{
			// Assert
			Assert.Throws<ArgumentOutOfRangeException>(() => Allowable.Get(-1));
		}
	}
}
