using System;
using Answers.Entities.Models;
using Xunit;


namespace EntitiesUnitTests.Models
{
	public class PostRevisionTests
	{
		[Fact]
		public void Create_CanBeCalled()
		{
			// Arrange
			Account account = DefaultFactory.CreateAccount();
			Question question = DefaultFactory.CreateQuestion(account);
			Post post = question.Post;


			// Act
			PostRevision.Create(post, account, DateTime.Now, "", "");
		}


		[Fact]
		public void Create_SetsValues()
		{
			// Arrange
			Account account = DefaultFactory.CreateAccount();
			Question question = DefaultFactory.CreateQuestion(account);
			Post post = question.Post;
			DateTime creationDate = DateTime.Now;
			string title = "trubstv";
			string content = "opaiupwgumca";


			// Act
			PostRevision postRevision = PostRevision.Create(post, account, creationDate, title, content);


			// Assert
			Assert.Equal(account, postRevision.Account);
			Assert.Equal(post, postRevision.Post);
			Assert.Equal(creationDate, postRevision.CreationDate);
			Assert.Equal(title, postRevision.Title);
			Assert.Equal(content, postRevision.Content);
		}


		[Fact]
		public void Create_AddsRevisionToPost()
		{
			// Arrange
			Account account = DefaultFactory.CreateAccount();
			Question question = DefaultFactory.CreateQuestion(account);
			Post post = question.Post;
			DateTime creationDate = DateTime.Now;
			string title = "trubstv";
			string content = "opaiupwgumca";


			// Act
			PostRevision postRevision = PostRevision.Create(post, account, creationDate, title, content);


			// Assert
			Assert.Equal(postRevision, post.CurrentRevision);
			Assert.Contains(post.Revisions, x => x == postRevision);
		}


		[Fact]
		public void NonNullValuesThrow()
		{
			// Arrange
			Account account = DefaultFactory.CreateAccount();
			Question question = DefaultFactory.CreateQuestion(account);
			Post post = question.Post;


			// Act
			PostRevision postRevision = PostRevision.Create(post, account, DateTime.Now, "", "");


			// Assert
			Assert.Throws<ArgumentNullException>(() => postRevision.Title = null);
			Assert.Throws<ArgumentNullException>(() => postRevision.Content = null);
			Assert.Throws<ArgumentNullException>(() => postRevision.Post = null);
			Assert.Throws<ArgumentNullException>(() => postRevision.Account = null);
		}
	}
}
