using System;
using Answers.Entities.Models;
using Xunit;


namespace EntitiesUnitTests.Models
{
	public class TagTests
	{
		[Fact]
		public void Create_CanBeCalled()
		{
			Tag.Create("", "");
		}


		[Fact]
		public void ValuesCanNotBeNull()
		{
			Assert.Throws<ArgumentNullException>(() => Tag.Create(null, ""));
			Assert.Throws<ArgumentNullException>(() => Tag.Create("", null));
		}


		[Fact]
		public void Create_InitializesCollections()
		{
			// Act
			Tag tag = Tag.Create("", "");


			// Assert
			Assert.NotNull(tag.Questions);
		}


		[Fact]
		public void Create_SetsValues()
		{
			// Arrange
			string title = "ndb5hg";
			string description = "k8t77h5d";


			// Act
			Tag tag = Tag.Create(title, description);


			// Assert
			Assert.Equal(title, tag.Title);
			Assert.Equal(description, tag.Description);
		}
	}
}
