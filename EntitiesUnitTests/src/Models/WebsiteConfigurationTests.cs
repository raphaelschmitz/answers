using System;
using Answers.Entities.Models;
using Xunit;


namespace EntitiesUnitTests.Models
{
	public class WebsiteConfigurationTests
	{
		[Fact]
		public void Create_CanBeCalled()
		{
			// Act
			WebsiteConfiguration.Create("", 0, false, "", "", "", "");
		}


		[Fact]
		public void NullValuesThrow()
		{
			// Act
			WebsiteConfiguration websiteConfiguration = WebsiteConfiguration.Create("", 0, false, "", "", "", "");


			// Assert
			Assert.Throws<ArgumentNullException>(() => websiteConfiguration.EmailFromAddress = null);
			Assert.Throws<ArgumentNullException>(() => websiteConfiguration.RegistrationEmailTemplate = null);
		}


		[Fact]
		public void ValuesAreAssigned()
		{
			// Arrange
			string emailSmtpAddress = "from@email.com";
			int emailPort = 1234;
			bool emailUseSsl = true;
			string emailUserName = "from";
			string emailPassword = "ialecnalcn";
			string emailFromAddress = "from@email.com";
			string registrationEmailTemplate = "ioha uwcrlhguseguirhmuvic rh {{ name }} ivorsrcj";


			// Act
			WebsiteConfiguration websiteConfiguration =
				WebsiteConfiguration.Create(emailSmtpAddress, emailPort, emailUseSsl, emailUserName, emailPassword,
					emailFromAddress, registrationEmailTemplate);


			// Assert
			Assert.Equal(emailSmtpAddress, websiteConfiguration.EmailSmtpAddress);
			Assert.Equal(emailPort, websiteConfiguration.EmailPort);
			Assert.Equal(emailUseSsl, websiteConfiguration.EmailUseSsl);
			Assert.Equal(emailUserName, websiteConfiguration.EmailUserName);
			Assert.Equal(emailPassword, websiteConfiguration.EmailPassword);
			Assert.Equal(emailFromAddress, websiteConfiguration.EmailFromAddress);
			Assert.Equal(registrationEmailTemplate, websiteConfiguration.RegistrationEmailTemplate);
		}
	}
}
