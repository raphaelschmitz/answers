using System;
using System.Linq;
using Answers.Entities.Models;
using Xunit;


namespace EntitiesUnitTests.Models
{
	public class CommentTests
	{
		[Fact]
		public void Create_CanBeRun()
		{
			// Arrange
			Account account = DefaultFactory.CreateAccount();
			Question question = DefaultFactory.CreateQuestion();
			Post post = question.Post;


			// Act
			Comment.Create(post, account, DateTime.Now, "");
		}


		[Fact]
		public void Create_SetsValues()
		{
			// Arrange
			Account account = DefaultFactory.CreateAccount();
			Question question = DefaultFactory.CreateQuestion();
			Post post = question.Post;
			DateTime creationDate = DateTime.Now;
			string content = "teystcf3cwascxdujhrv5";


			// Act
			Comment comment = Comment.Create(post, account, creationDate, content);


			// Assert
			Assert.Equal(creationDate, comment.CreationDate);
			Assert.Equal(content, comment.Content);
			Assert.Equal(post, comment.Post);
			Assert.Equal(account, comment.Account);
		}


		[Fact]
		public void Create_AddsToPost()
		{
			// Arrange
			Account account = DefaultFactory.CreateAccount();
			Question question = DefaultFactory.CreateQuestion();
			Post post = question.Post;
			DateTime creationDate = DateTime.Now;
			string content = "teystcf3cwascxdujhrv5";


			// Act
			Comment comment = Comment.Create(post, account, creationDate, content);


			// Assert
			Assert.NotNull(post.Comments.FirstOrDefault(x => x == comment));
		}


		[Fact]
		public void ThrowsForNullables()
		{
			// Arrange
			Account account = DefaultFactory.CreateAccount();
			Question question = DefaultFactory.CreateQuestion();
			Post post = question.Post;
			Comment comment = Comment.Create(post, account, DateTime.Now, "");


			// Assert
			Assert.Throws<ArgumentNullException>(() => comment.Content = null);
			Assert.Throws<ArgumentNullException>(() => comment.Post = null);
			Assert.Throws<ArgumentNullException>(() => comment.Account = null);
		}
	}
}
