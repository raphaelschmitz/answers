using System;
using Answers.Entities.Models;
using Xunit;


namespace EntitiesUnitTests.Models
{
	public class EmailConfirmProcessTests
	{
		[Fact]
		public void Create_CanBeRun()
		{
			// Arrange
			Account account = DefaultFactory.CreateAccount();


			// Act
			EmailConfirmProcess.Create(account, "", DateTime.Now);
		}


		[Fact]
		public void Create_SetsValues()
		{
			// Arrange
			Account account = DefaultFactory.CreateAccount();
			string confirmKey = "hsev;igmoshourvc";
			DateTime creationDate = DateTime.Now;


			// Act
			EmailConfirmProcess emailConfirmProcess = EmailConfirmProcess.Create(account, confirmKey, creationDate);


			// Assert
			Assert.Equal(account, emailConfirmProcess.Account);
			Assert.Equal(confirmKey, emailConfirmProcess.ConfirmKey);
			Assert.Equal(creationDate, emailConfirmProcess.CreationDate);
		}


		[Fact]
		public void ThrowsForNullables()
		{
			// Arrange
			Account account = DefaultFactory.CreateAccount();
			EmailConfirmProcess emailConfirmProcess = EmailConfirmProcess.Create(account, "", DateTime.Now);


			// Assert
			Assert.Throws<ArgumentNullException>(() => emailConfirmProcess.Account = null);
			Assert.Throws<ArgumentNullException>(() => emailConfirmProcess.ConfirmKey = null);
		}
	}
}
