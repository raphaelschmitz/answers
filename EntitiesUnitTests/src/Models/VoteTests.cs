using System;
using System.Linq;
using Answers.Entities.Models;
using Xunit;


namespace EntitiesUnitTests.Models
{
	public class VoteTests
	{
		private readonly Account _account;
		private readonly Question _question;
		private readonly Post _post;


		public VoteTests()
		{
			_account = DefaultFactory.CreateAccount();
			_question = DefaultFactory.CreateQuestion(_account);
			_post = _question.Post;
		}


		[Fact]
		public void Create_CanBeCalled()
		{
			// Act
			Vote.Create(_post, _account, Vote.Up);
		}


		[Fact]
		public void ThrowsForNonNullables()
		{
			// Arrange
			Vote vote = Vote.Create(_post, _account, Vote.Up);

			
			// Assert
			Assert.Throws<ArgumentNullException>(() => vote.Post = null);
			Assert.Throws<ArgumentNullException>(() => vote.Voter = null);
			Assert.Throws<ArgumentNullException>(() => vote.Beneficiary = null);
		}


		[Fact]
		public void Create_SetsVoteValues()
		{	
			// Arrange
			int value = Vote.Up;
			var voter = Account.Create("", "", "");

			
			// Act
			Vote vote = Vote.Create(_post, voter, value);
			
			
			// Assert
			Assert.Equal(_post, vote.Post);
			Assert.Equal(voter, vote.Voter);
			Assert.Equal(_account, vote.Beneficiary);
			Assert.Equal(value , vote.Value);
		}


		[Fact]
		public void Create_AddsVoteToPost()
		{
			// Act
			Vote vote = Vote.Create(_post, _account, Vote.Up);
			
			
			// Assert
			Assert.NotNull(_post.Votes.FirstOrDefault(x=>x == vote));
		}


		[Fact]
		public void Create_AddsVoteToBeneficiary()
		{
			// Arrange
			var voter = Account.Create("", "", "");

			
			// Act
			Vote vote = Vote.Create(_post, voter, Vote.Up);
			
			
			// Assert
			Assert.NotNull(_account.ReceivedVotes.FirstOrDefault(x=>x == vote));
		}
	}
}
