using System;
using System.Linq;
using Answers.Entities.Models;
using Answers.Entities.Values;
using Xunit;


namespace EntitiesUnitTests.Models
{
	public class PostTests
	{
		[Fact]
		public void Create_CanBeCalled()
		{
			// Arrange
			Account account = DefaultFactory.CreateAccount();
			Question question = DefaultFactory.CreateQuestion(account);


			// Act
			Post.Create(question, PostType.Answer, account, DateTime.Now, "", "");
		}


		[Fact]
		public void Create_SetsValues()
		{
			// Arrange
			Account account = DefaultFactory.CreateAccount();
			Question question = DefaultFactory.CreateQuestion(account);


			// Act
			PostType postType = PostType.Answer;
			DateTime creationDate = DateTime.Now;
			string title = "q34535";
			string content = "i8n6j7tybd";
			Post post = Post.Create(question, postType, account, creationDate, title, content);


			// Assert
			Assert.Equal(question, post.Question);
			Assert.Equal(postType, post.PostType);
			Assert.Equal(account, post.Creator);
			Assert.Equal(creationDate, post.CurrentRevision.CreationDate);
			Assert.Equal(title, post.CurrentRevision.Title);
			Assert.Equal(content, post.CurrentRevision.Content);
		}


		[Fact]
		public void Create_AddsToOtherCollections()
		{
			// Arrange
			Account account = DefaultFactory.CreateAccount();
			Question question = DefaultFactory.CreateQuestion(account);


			// Act
			Post post = Post.Create(question, PostType.Answer, account, DateTime.Now, "", "");


			// Assert
			Assert.NotNull(account.Posts.FirstOrDefault(x => x == post));
			Assert.NotNull(question.Posts.FirstOrDefault(x => x == post));
		}


		[Fact]
		public void Create_SetsFirstRevision()
		{
			// Arrange
			Account account = DefaultFactory.CreateAccount();
			Question question = DefaultFactory.CreateQuestion(account);


			// Act
			Post post = Post.Create(question, PostType.Answer, account, DateTime.Now, "", "");


			// Assert
			Assert.NotNull(post.FirstRevision);
		}


		[Fact]
		public void Create_CollectionsAreInitialized()
		{
			// Arrange
			Account account = DefaultFactory.CreateAccount();
			Question question = DefaultFactory.CreateQuestion(account);


			// Act
			Post post = Post.Create(question, PostType.Answer, account, DateTime.Now, "", "");


			// Assert
			Assert.NotNull(post.Votes);
			Assert.NotNull(post.Revisions);
			Assert.NotNull(post.Comments);
		}


		[Fact]
		public void ThrowsForNonNullables()
		{
			// Arrange
			Account account = DefaultFactory.CreateAccount();
			Question question = DefaultFactory.CreateQuestion(account);


			// Act
			Post post = Post.Create(question, PostType.Answer, account, DateTime.Now, "", "");


			// Assert
			Assert.Throws<ArgumentNullException>(() => post.Question = null);
			Assert.Throws<ArgumentNullException>(() => post.Creator = null);
			Assert.Throws<ArgumentNullException>(() => post.FirstRevision = null);
			Assert.Throws<ArgumentNullException>(() => post.CurrentRevision = null);
		}
	}
}
