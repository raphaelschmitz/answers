using System;
using Answers.Entities.Models;
using Xunit;


namespace EntitiesUnitTests.Models
{
	public class AccountTests
	{
		[Fact]
		public void Create_CanBeCalled()
		{
			// Act
			Account.Create("", "", "");
		}


		[Fact]
		public void NullValuesThrow()
		{
			// Arrange
			Account account = Account.Create("", "", "");


			// Assert
			Assert.Throws<ArgumentNullException>(() => account.EmailAddress = null);
			Assert.Throws<ArgumentNullException>(() => account.PasswordHash = null);
			Assert.Throws<ArgumentNullException>(() => account.UserName = null);
		}


		[Fact]
		public void CollectionsAreInitialized()
		{
			// Arrange
			Account account = Account.Create("", "", "");


			// Assert
			Assert.NotNull(account.Questions);
			Assert.NotNull(account.Posts);
			Assert.NotNull(account.Comments);
			Assert.NotNull(account.ReceivedVotes);
		}
	}
}
