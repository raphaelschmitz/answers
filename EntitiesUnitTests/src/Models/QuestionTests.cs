using System;
using System.Linq;
using Answers.Entities.Models;
using Xunit;


namespace EntitiesUnitTests.Models
{
	public class QuestionTests
	{
		private readonly Account _account;


		public QuestionTests()
		{
			_account = DefaultFactory.CreateAccount();
		}


		[Fact]
		public void Create_CanRun()
		{
			// Act
			Question.Create(_account, DateTime.Now, "", "");
		}


		[Fact]
		public void Create_CollectionsAreInitialized()
		{
			// Act
			Question question = Question.Create(_account, DateTime.Now, "", "");


			// Assert
			Assert.NotNull(question.Tags);
			Assert.NotNull(question.Posts);
		}


		[Fact]
		public void Create_AddsPost()
		{
			// Act
			Question question = Question.Create(_account, DateTime.Now, "Title", "Content");


			//Assert
			Assert.NotNull(question.Post);
		}


		[Fact]
		public void Create_SetsValues()
		{
			// Arrange
			string title = "Title";


			// Act
			Question question = Question.Create(_account, DateTime.Now, title, "Content");


			//Assert
			Assert.Equal(_account, question.Creator);
			Assert.Equal(title, question.CurrentTitle);
		}


		[Fact]
		public void Create_AddsQuestionToAccount()
		{
			// Act
			Question question = Question.Create(_account, DateTime.Now, "", "Content");


			//Assert
			Assert.Contains(_account.Questions, x => x.Id == question.Id);
		}


		[Fact]
		public void Create_InputDataIsSetInRevision()
		{
			// Arrange
			DateTime dateTime = DateTime.Now;
			string title = "Title";
			string content = "Content";


			// Act
			Question question = Question.Create(_account, dateTime, title, content);


			//Assert
			Post post = question.Post;
			Assert.Equal(dateTime, post.CurrentRevision.CreationDate);
			Assert.Equal(title, post.CurrentRevision.Title);
			Assert.Equal(content, post.CurrentRevision.Content);
		}


		[Fact]
		public void ThrowsForNonNullables()
		{
			// Arrange
			Question question = DefaultFactory.CreateQuestion();


			// Act
			Assert.Throws<ArgumentNullException>(() => question.Creator = null);
			Assert.Throws<ArgumentNullException>(() => question.CurrentTitle = null);
			Assert.Throws<ArgumentNullException>(() => question.Post = null);
		}


		[Fact]
		public void AddAnswer_CreatesPost()
		{
			// Arrange
			Question question = DefaultFactory.CreateQuestion();


			// Act
			Post post = question.AddAnswer(_account, DateTime.Now, "");


			//Assert
			Assert.NotNull(post);
		}


		[Fact]
		public void AddAnswer_AddsPost()
		{
			// Arrange
			Question question = DefaultFactory.CreateQuestion();


			// Act
			Post post = question.AddAnswer(_account, DateTime.Now, "");


			//Assert
			Assert.NotNull(question.Posts.FirstOrDefault(x => x == post));
		}
	}
}
